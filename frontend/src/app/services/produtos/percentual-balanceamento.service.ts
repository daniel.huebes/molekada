import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env} from '../../../environments/environment';
import { PercentualBalanceamentoLoja } from 'src/app/view/percentual-balanceamento/percentual-balanceamento-loja';
import { PercentualBalanceamentoProduto } from 'src/app/view/percentual-balanceamento/percentual-balanceamento-produto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PercentualBalanceamentoService {
    private readonly APIBalanceamento = env.backendAPI;

    constructor(private http: HttpClient) {}

    listBalanceamentoLojas(){
        return this.http.get<PercentualBalanceamentoLoja>(this.APIBalanceamento + '/balanceamento-loja-api/balanceamento-loja');
    }

    listBalanceamentoProdutos(page, filter?, orderBy?, orderBySense?){
        let p:any = {};
        p.page = page;
        if(filter) {
            p.filter = filter;
        }
        if(orderBy) {
            p.orderBy = orderBy;
            p.orderBySense = orderBySense == true ? 'ASC' : 'DESC';
        }

        return this.http.get<PercentualBalanceamentoProduto>(this.APIBalanceamento + '/balanceamento-product-no-repeat-view-api/balanceamento-product-no-repeat-view', {
            params: p
        });
    }

    balanceamentoByLoja(field:string):Observable<boolean> {
        console.log(field);
        
        return this.http.get<boolean>(this.APIBalanceamento + '/balanceamento-product-api/get-all-balanceamento-by-loja/' + field);
    }

    createBalanceamentoProduto(body) {
        return this.http.post<any>(this.APIBalanceamento + '/balanceamento-product-api/balanceamento-product', body);
    }

    editBalanceamentoProduto(id, body) {
        return this.http.put<any>(this.APIBalanceamento + '/balanceamento-product-api/balanceamento-product/' + id, body);
    }

    editBalanceamentoLoja(id, body) {
        return this.http.put<any>(this.APIBalanceamento + '/balanceamento-loja-api/balanceamento-loja/' + id, body);
    }
}
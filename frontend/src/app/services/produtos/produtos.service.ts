import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env} from '../../../environments/environment';
import { CategoryData, Product, ProductAgrupado, ProductUpdated } from 'src/app/view/produtos/produto';
import { take, tap } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class ProdutosService {
    private readonly APIProducts = env.backendAPI;

    constructor(private http: HttpClient) {}

    list(page, filter?, orderBy?, orderBySense?, url = 'product'){ //pega todos os produtos
        let p:any = {};
        p.page = page;
        if(filter) {
            p.filter = filter;
        }
        if(orderBy) {
            p.orderBy = orderBy;
            p.orderBySense = orderBySense == true ? 'ASC' : 'DESC';
        }

        return this.http.get<Product>(this.APIProducts + `/product-api/${url}`, {
            params: p
        });
    }

    listAgrupado(page, filter?, orderBy?, orderBySense?){ //pega todos os produtos agrupados por código
        let p:any = {};
        p.page = page;
        if(filter) {
            p.filter = filter;
        }
        if(orderBy) {
            p.orderBy = orderBy;
            p.orderBySense = orderBySense == true ? 'ASC' : 'DESC';
        }

        return this.http.get<ProductAgrupado>(this.APIProducts + '/product-view-api/product-view', {
            params: p
        });
    }

    listUpdated(){
        return this.http.get<ProductUpdated>(this.APIProducts + '/product-updated-view-api/product-updated-view', {});
    }

    listCategory() {
        let p: any = {};

        p.orderBy = 'categoria_nome';
        p.orderBySense = 'ASC';

        return this.http.get<CategoryData[]>(this.APIProducts + '/product-api/product-categories', {
            params: p
        });

    }

    listMaketplaces() {
      let p: any = {};

        p.orderBy = 'marketplace_nome';
        p.orderBySense = 'ASC';

        return this.http.get<CategoryData[]>(this.APIProducts + '/product-api/product-marketplaces', {
            params: p
        }).pipe(take(1));
    }

    export(filter, index) {
      const path = this.getPath(index);
      let p:any = {};
        p.page = 1;
        if(filter) {
            p.filter = filter;
        }

      return this.http.get(this.APIProducts + `/product-api/${path}`, {params: p})
        .pipe(
          tap((res:any) => window.open(res?.url)),
          take(1));
    }

    private getPath(index) {
      switch(index) {
        case 0: return 'product-export/cod';
        case 1: return 'product-export/group';
        case 2: return 'product-export-new/prod';
        case 3: return 'product-export-new/cod';
      }

      return '';
    }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env} from '../../../environments/environment';


@Injectable({
  providedIn: 'root',
})
export class SalesService {
    private readonly APISales = env.backendAPI;

    constructor(private http: HttpClient) {}

    listSales(filter){
      return this.http.post<any>(this.APISales + '/product-api/sales', filter);
    }

    listCollection(){
      return this.http.get<any>(this.APISales + '/product-api/collection', {});
    }

    getImages(filter){
      return this.http.post<any>(this.APISales + '/product-api/sales-images', filter);
    }

    getCategory(){
      return this.http.get<any>(this.APISales + '/product-api/sales-category');
    }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  user: { email: string; password: string } = { email: '', password: '' };

  constructor(private authServ: AuthService, private router: Router, private messageService: MessageService) { }

  ngOnInit(): void { }

  clean() {
    this.user = { email: '', password: '' };
  }

  login() {
    this.authServ.login(this.user).then(
      () => {
        this.router.navigate(['view']);
      },
      (error) => {
        console.log(error.code);
        if (error.code == 'auth/invalid-email') {
          this.messageService.add({ severity: 'error', summary: 'ERRO', detail: 'Este e-mail não é válido.' });
        } else if (error.code == 'auth/wrong-password') {
          this.messageService.add({ severity: 'error', summary: 'ERRO', detail: 'Senha incorreta, tente novamente.' });
        } else if (error.code == 'auth/too-many-requests') {
          this.messageService.add({ severity: 'error', summary: 'ERRO', detail: 'Muitas tentivas, aguarde um tempo e tente novamente.' });
        } else if (error.code == 'auth/user-not-found') {
          this.messageService.add({ severity: 'error', summary: 'ERRO', detail: 'E-mail não cadastrado.' });
        }
      }
    );
  }
}

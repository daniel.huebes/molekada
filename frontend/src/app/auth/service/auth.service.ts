import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment as env} from '../../../environments/environment';
import { Auth } from '../auth';
import { AppStorageService } from 'src/app/shared/app-storage/app-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public auth: AngularFireAuth, 
              private router: Router, 
              private appStorageService: AppStorageService,
              private http: HttpClient) { }

  async signOut(){
    await this.auth.signOut();
    this.appStorageService.clear(AppStorageService.KEY_STORAGE.apiKey);
    this.router.navigate(['/auth']);
  }

  async login(user){
    await this.auth.signInWithEmailAndPassword(user.email, user.password);
    await this.getToken(user);
  }

  async getToken(user) {
    const authBackend = this.http.post<Auth>(env.backendAPI + '/authentication-api/login', user).pipe();

    let token = await authBackend.toPromise() as any;
    this.appStorageService.set(AppStorageService.KEY_STORAGE.apiKey, token);
  }
}

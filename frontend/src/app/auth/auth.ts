export interface Auth {
    authorization: string;
    schema: string;
}
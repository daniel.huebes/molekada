import { ViewComponent } from './view.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProdutosComponent } from './produtos/produtos.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PercentualBalanceamentoComponent } from './percentual-balanceamento/percentual-balanceamento.component';
import { VendasComponent } from './vendas/vendas.component';

const routes: Routes = [
  {
    path: '',
    component: ViewComponent,
    children:[
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'vendas',
        component: VendasComponent
      },
      {
        path: 'produtos',
        component: ProdutosComponent
      },
      {
        path: 'percentual-balanceamento',
        component: PercentualBalanceamentoComponent
      }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewRoutingModule {}

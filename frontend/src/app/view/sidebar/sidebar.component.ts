import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { user } from 'rxfire/auth';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  user = globalThis.maclerUser;
  menuUser;
  visibleSidebar = false;

  constructor(private authServ: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.menuUser = [
      {
        items: [
          {
            label: 'Sair',
            icon: 'pi pi-sign-out',
            command: () => {
              this.logout();
            },
          },
        ],
      },
    ];
  }

  async logout() {
    await this.authServ.signOut();
    this.router.navigate(['auth']);
  }
}

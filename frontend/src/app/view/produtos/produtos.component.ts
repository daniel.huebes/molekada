import { Component, OnInit } from '@angular/core';
import { ProdutosService } from '../../services/produtos/produtos.service';
import { AuthService } from '../../auth/service/auth.service';
import { Observable, Subject, catchError, empty, map, tap } from 'rxjs';
import { CategoryData, Product, ProductAgrupado } from './produto';
import * as moment from 'moment';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss'],
})

export class ProdutosComponent implements OnInit {

  produtos$: Observable<Product> | undefined;
  error$ = new Subject<boolean>();
  tableRows = 10;
  totalElements = 50;
  totalElementsNew = 50;
  page = 1;
  filter;
  order;
  orderBy = 'codigo';

  produtosAgrupados$: Observable<ProductAgrupado> | undefined;
  errorAgrupados$ = new Subject<boolean>();
  tableRowsAgrupados = 10;
  totalElementsAgrupados;
  pageAgrupados = 1;
  filterAgrupados;
  orderAgrupados;
  produtosNew$: Observable<Product>
  public lojas;
  public selectedFilter: any;
  public rangeData: Date[] = [];

  public updated: any;
  public categories: CategoryData[];
  public selectedCategory: CategoryData;
  public exporting = false;
  public listFilter = '';
  public activeIndex = 0;
  public selectedMarketplace = null;
  public marketplaces: any[];
  public readonly showButtonFilter = true;

  constructor(private service: ProdutosService, private authServ: AuthService) { }

  ngOnInit() {
    this.buscarProdutos(this.page);
    this.buscarProdutosAgrupados(this.pageAgrupados);
    this.buscarUpdated();
    this.setDate();
    this.order = true;
    this.orderAgrupados = true;
  }

  colsCadCom = [
    { field: 'name', header: 'Produto' },
    { field: 'codigo', header: 'Código' },
    { field: 'preco_media', header: 'Preço médio' },
    { field: 'percentual_lucro', header: 'Percentual de lucro' },
    { field: 'markup', header: 'Markup' },
    { field: 'custo', header: 'Custo' },
    { field: 'categoria_nome', header: 'Categoria' },
  ];

  colsCadComAgrupados = [
    { field: 'name', header: 'Produto' },
    { field: 'codigo', header: 'Código' },
    { field: 'preco_media', header: 'Preço médio' },
    { field: 'percentual_lucro', header: 'Percentual de lucro' },
    { field: 'markup', header: 'Markup' },
    { field: 'custo', header: 'Custo' }
  ];

  buscarProdutos(page, filter?, orderBy?) {
    this.produtos$ = this.service.list(page, filter, orderBy, !this.order)
      .pipe(
        catchError(error => {
          this.error$.next(true);
          if (error.error.message == 'Your token is not valid!') {
            this.authServ.signOut();
          }
          return empty();
        })
      );

    this.produtos$.subscribe(data => {
      this.totalElements = data.totalElements;
      this.order = !this.order;
    });
  }

  buscarProdutosAgrupados(page, filter?, orderBy?) {
    this.produtosAgrupados$ = this.service.listAgrupado(page, filter, orderBy, !this.order)
      .pipe(
        catchError(error => {
          this.errorAgrupados$.next(true);
          if (error.error.message == 'Your token is not valid!') {
            this.authServ.signOut();
          }
          return empty();
        })
      );

    this.produtosAgrupados$.subscribe(data => {
      this.totalElementsAgrupados = data.totalElements;
      this.orderAgrupados = !this.order;
    });
  }

  buscarUpdated() {
    this.service.listUpdated().subscribe(data => {
      this.updated = moment(data.contents[0].updatedAt).format('DD/MM/YYYY HH:mm');
    });
  }

  resetData() {
    this.produtos$ = undefined;
    this.error$ = undefined;
  }

  resetDataAgrupados() {
    this.produtosAgrupados$ = undefined;
    this.errorAgrupados$ = undefined;
  }

  async paginate(event) {
    console.log(event, '1')
    this.resetData();
    this.tableRows = event.rows;
    let page = event.page + 1;

    if (this.filter) {
      let filterTerm = "substringof('" + this.filter + "', codigo)";
      this.buscarProdutos(page, filterTerm);
    }

    this.buscarProdutos(page);
  }

  async paginateNew(event) {
    this.resetData();
    this.tableRows = event.rows;
    let page = event.page + 1;
    this.filterListNew(page);
  }

  async paginateAgrupados(event) {
    this.resetDataAgrupados();
    this.tableRowsAgrupados = event.rows;
    let pageAgrupados = event.page + 1;

    if (this.filterAgrupados) {
      let filterTerm = "substringof('" + this.filterAgrupados + "', codigo)";
      this.buscarProdutosAgrupados(pageAgrupados, filterTerm);
    }

    this.buscarProdutosAgrupados(pageAgrupados);
  }

  async clearFilter(clerarFilterNew = false) {
    this.filter = '';
    this.rangeData = null;
    if(!clerarFilterNew) {
      this.selectedCategory = undefined;
      this.filterList();
    }

    else {
      this.selectedFilter = undefined;
      this.buscarProdutosNew();
    }
  }

  async filterList() {
    this.resetData();
    let filterTerm = '';
    let filterList = [];
    if (this.filter) {
      filterList.push("substringof('" + this.filter + "', codigo)");
    }
    if (this.selectedCategory) {
      filterList.push(`categoria_id eq ${this.selectedCategory.categoria_id}`)
    }
    filterTerm = filterList.join(' and ');
    this.listFilter = filterTerm;
    this.buscarProdutos(1, filterTerm);
  }


  async clearFilterAgrupados() {
    this.selectedCategory = undefined;
    this.filterAgrupados = '';

    this.filterListAgrupados();
  }

  async filterListAgrupados() {
    this.resetDataAgrupados();
    let filterTerm = '';
    let filterList = [];
    if (this.filterAgrupados) {
      filterList.push("substringof('" + this.filterAgrupados + "', codigo)");
    }
    if (this.selectedCategory) {
      filterList.push(`categoriaid eq ${this.selectedCategory.categoria_id}`)
    }
    filterTerm = filterList.join(' and ');
    this.listFilter = filterTerm;
    this.buscarProdutosAgrupados(1, filterTerm);
  }

  async sortList(event) {
    this.resetData();
    if (this.filter) {
      let filterTerm = "substringof('" + this.filter + "', codigo)";
      this.buscarProdutos(1, filterTerm, event.field)
    } else {
      this.buscarProdutos(1, null, event.field);
    }
  }

  async sortListAgrupados(event) {
    this.resetDataAgrupados();
    if (this.filterAgrupados) {
      let filterTerm = "substringof('" + this.filterAgrupados + "', codigo)";

      this.buscarProdutosAgrupados(1, filterTerm, event.field)
    } else {
      this.buscarProdutosAgrupados(1, null, event.field);
    }
  }

  sortListNew(event) {
    this.orderBy = event?.field;
    this.order = !this.order;
    this.filterListNew(1);
  }

  async listCategories() {
    this.service.listCategory().subscribe(data => {
      this.categories = data;
    });
  }
  export() {
    this.exporting = true;
    this.service.export(this.listFilter, this.activeIndex)
    .subscribe(() => this.exporting = false);
  }

  buscarProdutosNew(page?, filter?) {
    let path = this.getPath();

    if(this.activeIndex == 2 || this.activeIndex == 3) {
      this.produtosNew$ = this.service.list(page, filter, this.orderBy, this.order, path)
      .pipe(
        tap(data => {
          this.totalElementsNew = data.totalElements;
          this.order = !this.order;
        }),
        catchError(error => {
          this.error$.next(true);
          if (error.error.message == 'Your token is not valid!') {
            this.authServ.signOut();
          }
          return empty();
        })
      );

    }
  }

  async filterListNew(page=1) {
    this.lojas = [
    {nome: 'Molekada', id: '1'},
    {nome: 'Amalook', id: '2'}
    ];
    let filterTerm = '';
    let filterList = [];
    if (this.filter) {
      filterList.push("codigo like " + this.filter);
    }
    if (this.selectedFilter?.id) {
      filterList.push(`id_loja eq ${this.selectedFilter?.id}`);
    }
    if (this.selectedCategory) {
      filterList.push(`categoria_id eq ${this.selectedCategory.categoria_id}`);
    }
    if(this.rangeData.length == 2) {
      filterList.push(`startDate ge "${moment(this.rangeData[0]).toDate().toISOString()}" and endDate le "${moment(this.rangeData[1]).hours(23).toDate().toISOString()}"`)
    }
    if(this.selectedMarketplace?.marketplace_id) {
      filterList.push(`marketplace_id eq ${this.selectedMarketplace?.marketplace_id}`);
    }
    filterTerm = filterList.join(' and ');
    this.listFilter = filterTerm;
    this.buscarProdutosNew(page, filterTerm);
  }

  private getPath() {
    return this.activeIndex === 2 ? 'product-view'
    : this.activeIndex === 3 ? 'product-cod' : '';
  }

  public listMarketplaces() {
    this.service.listMaketplaces().subscribe(res => {
      this.marketplaces = res;
    });
  }

  private setDate() {
    let today = moment();
    let month = moment().subtract(2, "months");
    this.rangeData[1] = new Date(moment(today).toDate());
    this.rangeData[0] = new Date(moment(month).toDate());
  }

  public changeTab() {
    if(this.activeIndex === 2 || this.activeIndex === 3) this.filterListNew();
    else if(this.activeIndex === 0 || this.activeIndex === 1) {
      const count = this.activeIndex === 0 ? this.totalElements : this.totalElementsAgrupados;
      this.paginate({rows:10, page: 1, pageCount: count});
    } 
  }
}

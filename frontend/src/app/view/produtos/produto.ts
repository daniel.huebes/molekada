export interface Product {
    totalElements: number;
    totalPages: number;
    page: number;
    contents: ProductData[];
}

export interface ProductData {
    id: string;
    name: string;
    codigo: string;
    preco_media: number;
    percentual_lucro: number;
    markup: number;
    custo: number;
    categoria_id: number;
    categoria_nome: string;
    itens_vendidos: number;
    id_loja?:string;
    createdAt: string;
    updatedAt: string;
}

export interface ProductAgrupado {
    totalElements: number;
    totalPages: number;
    page: number;
    contents: ProductAgrupadoData[];
}

export interface ProductAgrupadoData {
    name: string;
    codigo: string;
    preco_media: number;
    percentual_lucro: number;
    markup: number;
    custo: number;
    itens_vendidos: number;
}

export interface ProductUpdated {
    totalElements: number;
    totalPages: number;
    page: number;
    contents: ProductUpdatedData[];
}

export interface ProductUpdatedData {
    id: number;
    createdAt: string;
    updatedAt: string;
}

export interface CategoryData {
    categoria_id: number;
    categoria_nome: string;
}

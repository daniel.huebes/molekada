import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit {
  user;

  constructor(private authServ: AuthService,
              private router: Router) {}

  ngOnInit(): void {
    this.authServ.auth.user.subscribe((user) => {
      if (!user) {
        this.router.navigate(['/', 'auth']);
      }
      this.user = user;
      globalThis.maclerUser = user;
    });
  }
}

import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { ViewRoutingModule } from './view-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewComponent } from './view.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { ToolbarModule } from 'primeng/toolbar';
import { MenuModule } from 'primeng/menu';
import { SidebarModule } from 'primeng/sidebar';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { TabViewModule } from 'primeng/tabview';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { PaginatorModule } from 'primeng/paginator';
import { AutoCompleteModule } from 'primeng/autocomplete';

import { ProdutosComponent } from './produtos/produtos.component';
import { PercentualBalanceamentoComponent } from './percentual-balanceamento/percentual-balanceamento.component';
import { VendasComponent } from './vendas/vendas.component';
import { ProgressBarModule } from 'primeng/progressbar';

@NgModule({
  declarations: [
    DashboardComponent,
    ViewComponent,
    VendasComponent,
    SidebarComponent,
    ProdutosComponent,
    PercentualBalanceamentoComponent
  ],
  imports: [
    SharedModule,
    ViewRoutingModule,
    ToolbarModule,
    MenuModule,
    SidebarModule,
    TableModule,
    DialogModule,
    ProgressBarModule,
    TabViewModule,
    ProgressSpinnerModule,
    PaginatorModule,
    AutoCompleteModule,

    MatSidenavModule,
    MatToolbarModule,
    MatTableModule,
  ],
})
export class ViewModule {}

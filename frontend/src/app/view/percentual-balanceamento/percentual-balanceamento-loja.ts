export interface PercentualBalanceamentoLoja {
    totalElements: number;
    totalPages: number;
    page: number;
    contents: PercentualBalanceamentoData[];
}

export interface PercentualBalanceamentoData {
    id: string;
    name: string;
    percentual_balanceamento: number;
    createdAt: string;
    updatedAt: string;
    checked?:boolean;
}
import { Component, OnInit } from '@angular/core';
import { PercentualBalanceamentoService } from 'src/app/services/produtos/percentual-balanceamento.service';
import { PercentualBalanceamentoProdutoData } from './percentual-balanceamento-produto';
import { PercentualBalanceamentoData } from './percentual-balanceamento-loja';
import { firstValueFrom } from 'rxjs';
import { MessageService } from 'primeng/api';

@Component({
    selector: 'app-percentual-balanceamento',
    templateUrl: './percentual-balanceamento.component.html',
    styleUrls: ['./percentual-balanceamento.component.scss'],
})

export class PercentualBalanceamentoComponent implements OnInit {
    lojaMolekada: any;
    lojaAmalook: any;
    lojaMiniMagia: any
    page: any = 1;
    produtos: any;
    tableRows = 10;
    totalElements;
    isLoadingLojas: boolean = false;
    isLoadingProdutos: boolean = false;
    filter;
    order = false;
    error: boolean = false;
    showModal:boolean = false;
    msgModal: string = '';
    itemClicked: PercentualBalanceamentoProdutoData | PercentualBalanceamentoData = null;
    callFunctionConfirm: boolean = true;

    colsCadCom = [
        { field: 'codigo', header: 'Código' },
        { field: 'name', header: 'Produto' },
        { field: 'percentual_molekada', header: 'Percentual Molekada' },
        { field: 'percentual_amalook', header: 'Percentual Amalook' }
    ];
    
    constructor(private service: PercentualBalanceamentoService,
                private messageService: MessageService) {}

    ngOnInit() {
        this.buscarBalanceamentoLojas();
        this.buscarBalanceamentoProdutos(this.page, null, 'codigo');
    }

    async onSaveProducts(){
        this.isLoadingProdutos = true;
        let promise = [];
        let countErr = 0;
        this.produtos.contents.forEach(async produto => {
            if(produto.percentual_molekada != null || produto.percentual_amalook != null || produto.percentual_mini_magia != null) {
                if(produto.id_balanceamento) {
                    
                    this.verifyShopChecked();
                    let somaPorcentagem = parseFloat(produto.percentual_molekada ?? 0) + parseFloat(produto.percentual_amalook ?? 0) + parseFloat(produto.percentual_mini_magia ?? 0);
                    if(somaPorcentagem == 100 || somaPorcentagem == 0) {
                        produto.erro = '';

                        let objProduct = {
                            percentual_molekada: produto.percentual_molekada,
                            percentual_amalook: produto.percentual_amalook,
                            percentual_mini_magia: produto.percentual_mini_magia,
                            codigo: produto.codigo
                        };
        
                        promise.push(this.service.editBalanceamentoProduto(produto.id_balanceamento, objProduct).subscribe(response => {
                            // this.buscarBalanceamentoProdutos(this.page, null, 'codigo');
                            // this.isLoadingProdutos = false;
                        }));
                    } else {
                        produto.erro = 'A soma das porcentagens precisa ser 100%';
                        countErr++;
                        this.isLoadingProdutos = false;
                    }
                } else {
                    let somaPorcentagem = parseFloat(produto.percentual_molekada) + parseFloat(produto.percentual_amalook) + parseFloat(produto.percentual_mini_magia);
                    
                    if(somaPorcentagem == 100 || somaPorcentagem == 0) {
                        produto.erro = '';

                        let objProduct = {
                            codigo: produto.codigo,
                            percentual_molekada: produto.percentual_molekada,
                            percentual_amalook: produto.percentual_amalook,
                            percentual_mini_magia: produto.percentual_mini_magia
                        };
        
                        promise.push(this.service.createBalanceamentoProduto(objProduct).subscribe(response => {
                            // this.buscarBalanceamentoProdutos(this.page, null, 'codigo');
                            // this.isLoadingProdutos = false;
                        }));
                    } else {
                        produto.erro = 'A soma das porcentagens precisa ser 100%';
                        countErr++;
                        this.isLoadingProdutos = false;
                    }
                }
            }
        });
        if (countErr == 0) {
            let errF = false;
            await Promise.all(promise).catch((err) => {
                console.log(err);
                errF = true;
                this.messageService.add({ severity: 'error', summary: 'ERRO', detail: 'Este e-mail não é válido.' });
            });
            if (!errF) {
                this.messageService.add({ severity: 'success', summary: 'Salvo', detail: 'Alterado com sucesso.' });
                setTimeout(() => {
                    this.buscarBalanceamentoProdutos(this.page, null, 'codigo');
                    this.isLoadingProdutos = false;    
                }, 1900);

            }
        }

    }

    async onSaveLojas(){
        const lojas = [this.lojaAmalook, this.lojaMiniMagia, this.lojaMolekada];
        let totalPercent = 0;
        lojas.forEach(loja =>  {
            if(loja.checked)
                totalPercent += Number(loja.percentual_balanceamento);
        });
        if(totalPercent === 100) {
            this.error = false;
            this.isLoadingLojas = true;
    
            await this.service.editBalanceamentoLoja(this.lojaMolekada.id, this.lojaMolekada).subscribe(response => {});
    
            await this.service.editBalanceamentoLoja(this.lojaAmalook.id, this.lojaAmalook).subscribe(response => {
                this.isLoadingLojas = false;
            });
    
            await this.service.editBalanceamentoLoja(this.lojaMiniMagia.id, this.lojaMiniMagia).subscribe(response => {
                this.isLoadingLojas = false;
            });
        } else this.error = true;
        
    }

    async buscarBalanceamentoLojas() {
        await this.service.listBalanceamentoLojas().subscribe(balanceamentoLojas => {
            balanceamentoLojas.contents.forEach(loja => {
                if(loja.name == 'Molekada') {
                    this.lojaMolekada = {
                        percentual_balanceamento: loja.percentual_balanceamento,
                        id: loja.id,
                        name: loja.name,
                        checked: loja.percentual_balanceamento === 0  ? false : true,
                    };
                } else if(loja.name == 'Amalook') {
                    this.lojaAmalook = {
                        percentual_balanceamento: loja.percentual_balanceamento,
                        id: loja.id,
                        name: loja.name,
                        checked: loja.percentual_balanceamento === 0  ? false : true,
                    };
                } else {
                    this.lojaMiniMagia = {
                        percentual_balanceamento: loja.percentual_balanceamento,
                        id: loja.id,
                        name: loja.name,
                        checked: loja.percentual_balanceamento === 0  ? false : true,
                    }
                }
            });
        });
    }

    async buscarBalanceamentoProdutos(page, filter?, orderBy?){
        await this.service.listBalanceamentoProdutos(page, filter, orderBy, this.order).subscribe(balanceamentoProdutos => {
            balanceamentoProdutos.contents.forEach(produto => {
                let codigoSplitted = produto.codigo.split('-');
                let nomeSplitted = produto.name.split('-');
                produto.codigo = codigoSplitted[0] + '-' + codigoSplitted[1];
                produto.name = nomeSplitted[0];
            });
            this.produtos = balanceamentoProdutos;
            this.totalElements = balanceamentoProdutos.totalElements;
        });
    }

    async paginate(event) {
        this.tableRows = event.rows;
        let page = event.page + 1;
        this.page = page;

        if(this.filter) {
            let filterTerm = "substringof('"+this.filter+"', codigo)";
            this.buscarBalanceamentoProdutos(page, filterTerm);
        } else {
            this.buscarBalanceamentoProdutos(page);
        }
    }

    async resetData() {
        this.produtos = undefined;
    }

    async filterList() {
        setTimeout(() => {
            this.resetData();
            let filterTerm = "substringof('"+this.filter+"', codigo)";
            this.buscarBalanceamentoProdutos(1, filterTerm);
        }, 1000);
    }

    async sortList(event) {
        this.resetData();
        this.order = !this.order;
        this.buscarBalanceamentoProdutos(1, null, event.field)
        if(this.filter) {
            let filterTerm = "substringof('"+this.filter+"', codigo)";
            this.buscarBalanceamentoProdutos(1, filterTerm, event.field);
        } else {
            this.buscarBalanceamentoProdutos(1, null, event.field);
        }
    }

    private verifyShopChecked() {
    const lojas = [this.lojaAmalook, this.lojaMiniMagia, this.lojaMolekada];
        lojas.forEach(loja => {
            if(!loja.checked) loja.percentual_balanceamento = 0;
        });
    }

    public async changePercent(item: PercentualBalanceamentoProdutoData | PercentualBalanceamentoData, field?:string, showModal = false, check?: string) {
        //Modal confirmação de mudança no percentual de estoque da loja
        if(showModal)  {
            let msg = '';
            item = item as PercentualBalanceamentoData;
            item.checked = !item.checked;
            this.itemClicked = item;

            const key = this.getKey(this.itemClicked.name);
            const existsBalanceamento = await  firstValueFrom(this.service.balanceamentoByLoja(key));
            if(!existsBalanceamento || !item.checked) {
                msg = `Deseja realmente ${item.checked ? 'desabilitar' : 'habilitar'} a loja ${item?.name}?`;
                this.callFunctionConfirm = true;
                this.showModalConfirm(msg);
            }  else if(item.checked) {
                msg = `A loja ${item?.name} possui produtos com balanceamento personalizado.<br> <b>Desabilite</b> a loja nesses produtos para continuar.`;
                this.callFunctionConfirm = false;
                this.showModalConfirm(msg);
            }
            
        };

        if(field) {
            item[check] = !item[check];
            const lojas = [this.lojaAmalook, this.lojaMiniMagia, this.lojaMolekada];
            for(let loja of lojas) {
                //Modal informação loja desabilitada
                if(loja.name === field && !item[check] && !loja.checked) {
                    this.callFunctionConfirm = false;
                    const msg = `O estoque da loja ${loja.name} está desabilitado!`
                    this.showModalConfirm(msg);
                    item[check] = 0;
                    return;
                } 
            };
            item[check] = !item[check];
            item = item as PercentualBalanceamentoProdutoData;
            item.percentual_molekada = item.percentual_molekada === null ? 0 : item.percentual_molekada;
            item.percentual_amalook = item.percentual_amalook === null ? 0 : item.percentual_amalook;
            item.percentual_mini_magia = item.percentual_mini_magia === null ? 0 : item.percentual_mini_magia;

            //Seta pra zero quando campo é desabilitado
            const key = this.getKey(field);
            if(key) {
                item[key] = 0;
            }
        }
    }

    private showModalConfirm(msg:string) {
        this.msgModal = msg
        this.showModal = true;
        
    }

    public async confirm() {
        this.itemClicked = this.itemClicked as PercentualBalanceamentoData;
        this.itemClicked.checked = !this.itemClicked.checked;
        this.showModal = !this.showModal;
        if(!this.itemClicked.checked) this.itemClicked.percentual_balanceamento = 0;
    }

    private getKey(field: string): string {
        if(field === 'Molekada') {
            return 'percentual_molekada';
        }        
        if(field === 'Amalook') {
            return 'percentual_amalook';
        }
        if(field === 'Mini Magia') {
            return 'percentual_mini_magia';
        }
        return null;
    }
}
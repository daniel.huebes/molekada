export interface PercentualBalanceamentoProduto {
    totalElements: number;
    totalPages: number;
    page: number;
    contents: PercentualBalanceamentoProdutoData[];
}

export interface PercentualBalanceamentoProdutoData {
    id: string;
    id_balanceamento: string;
    name: string;
    codigo: string;
    itens_vendidos: number;
    percentual_molekada: number;
    percentual_amalook: number;
    percentual_mini_magia:number;
    createdAt: string;
    updatedAt: string;
    molekadaCheck?:boolean;
    amalookCheck?:boolean;
    miniMagiaCheck?:boolean;
}
import { Component, OnInit } from '@angular/core';
import { ProdutosService } from '../../services/produtos/produtos.service';
import { AuthService } from '../../auth/service/auth.service';
import { Observable, Subject, catchError, empty, map, tap } from 'rxjs';
import { Venda } from './vendas';
import * as moment from 'moment';
import { SalesService } from 'src/app/services/produtos/sales.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-vendas',
  templateUrl: './vendas.component.html',
  styleUrls: ['./vendas.component.scss'],
})

export class VendasComponent implements OnInit {

  arrString = [
    'Oganizando registros...       ',
    'Oganizando registros...       ',
    'Verificando datas de vendas...',
    'Verificando datas de vendas...',
    'Contando registros...',
    'Contando registros...',
    'Separando por coleção...',
    'Separando por coleção...',
    'Carregando informações dos produtos...',
    'Carregando informações dos produtos...',
    'Agrupando número de vendas...',
    'Agrupando número de vendas...',
    'Oganizando registros...',
    'Oganizando registros...',
    'Contando unidades...',
    'Contando unidades...',
    'Verificando datas de vendas...',
    'Verificando datas de vendas...'
  ];
  labelLoading: string = '';
  showModal: boolean = false;
  vendas: any[] = [];
  vendasAux: any[] = [];
  loading: boolean = false;
  first = 0;
  rows = 10;
  filter = {
    inicio: new Date(),
    fim: new Date(),
    colecao: '',
    categoria: '',
    first: 0
  };
  colecoes = [];
  resetFlag: any;
  loadingPdf: boolean;
  categorias: any;

  constructor(private service: SalesService, 
              private messageService: MessageService) { }

  async ngOnInit() {
    //this.list();
    this.resetFlag = true;
    this.service.listCollection().subscribe((resp) => {
      this.colecoes = resp;
    });
    this.getCategory();
  }

  async list(htmlCall?) {
    this.showModal = true;
    if (htmlCall && this.resetFlag) {
      this.first = 0;
    }
    for (let i = 0; i < 20; i++) {
      setTimeout(() => {
        this.labelLoading = this.arrString[i];
      }, 1200 * i);
      if (i == 19) {
        this.labelLoading = 'Conexão lenta...';
      }
    };
    this.filter.first = this.first;
    console.log(this.filter);
    
    this.service.listSales(this.filter).subscribe((resp) => {
      console.log(resp);
      if (resp.length == 0) {
        if (htmlCall && this.resetFlag) {
            this.vendas = [];
            this.createPage();
            this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Busca não retornou resultados.' });
          } else {
            this.prev();
            this.messageService.add({ severity: 'warn', summary: 'Atenção', detail: 'Ultima página.' });
          }
      } else {
        if (htmlCall && this.resetFlag) {
          this.vendas = resp;
          this.resetFlag = false;
        } else {
          this.vendas = this.vendas.concat(resp);
        }
        this.getImage(resp);
        this.createPage();
      }
      this.showModal = false;
    });
  }

  getImage(data:any) {
    let arrayProd = [];

    for (let d = 0; d < data.length; d++) {
      const sale = data[d];
      
      arrayProd.push("'" + sale.parte1+'-'+sale.parte2 + "'");
    }
    this.service.getImages(arrayProd).subscribe((resp) => {
      console.log(resp);
      for (let p = 0; p < this.vendas.length; p++) {
        const sale = this.vendas[p];
        
        if (!sale.img) {
          let filter = resp.filter((tm) => tm.codigo_produto == sale.parte1+'-'+sale.parte2);

          if (filter[0]) {
            sale.img = filter[0].link;
          }
        }
      }
    });
  }

  getCategory() {
    this.service.getCategory().subscribe((resp) => {
      this.categorias = resp;
    });
  }

  next() {
    this.first = this.first + this.rows;
    if (this.first >= this.vendas.length) {
      this.list();
    } else {
      this.createPage();
    }
  }

  resetFilter() {
    this.resetFlag = true;
  }

  prev() {
    if (this.first > 0) {
      this.first = this.first - this.rows;
      this.createPage();
    }
  }

  reset() {
      this.first = 0;
      this.createPage();
  }

  createPage() {
    this.vendasAux = this.vendas.slice(this.first, (this.first + 10));
  }

  async exportarPDFOut() {
    
  }

  // async exportarPDFOut() {
  //   this.loadingPdf = true;
  //   let html = `
  //   <html>
  //     <header>
  //         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  //     </header>
  //     <style>
  //         .title {
  //             text-align: center;
  //             font-size: 16px;
  //             font-weight: bold;
  //         }

  //         .sub-titles {
  //             text-align: center;
  //             font-size: 12px;
  //             font-weight: bold;
  //             border-bottom: 1px solid gray;
  //             margin-bottom: 25px;
  //         }

  //         .field-name {
  //             font-weight: bold;
  //         }

  //         .session {
  //             margin-bottom: 30px;
  //         }
  //         table {
  //           border-collapse: collapse;
  //           width: 100%;
  //           font-size: 12px !important;
  //         }
  //         table, th, td {
  //           border: 1px solid black;
  //         }
  //         td {
  //           padding: 5px;
  //         }
  //         th {
  //           padding: 5px;
  //         }
  //     </style>

  //     <body>`;

  //   html += `<div class="session">
  //                 <div class="row sub-titles">
  //                     <div class="col-12">Produtos</div>
  //                 </div>
  //                 <table class="text-center" style="font-size: 10px">
  //                   <tr>
  //                       <th>Coleção</th>
  //                       <th>Produto</th>
  //                       <th>Descrição</th>
  //                       <th>Qtd.</th>
  //                   </tr>
  //                   `;

  //   this.vendas.forEach(element => {
  //     html += `
  //                       <tr>
  //                           <td>${element.descricao}</td>
  //                           <td>${element.parte1}-${element.parte2}</td>
  //                           <td>${element.descricao_prod}</td>
  //                           <td>${element.qtd}</td>
  //                       </tr>
  //                       `;
  //   });
  //   html += `
  //                 </table>
  //         </div>
  //     </body>
  //   </html>`;


  //   let headerHTML = `
  //     <style>
  //       .page-header{
  //         -webkit-print-color-adjust: exact;
  //         color:#000 !important;
  //         font-size:8px !important;
  //         width: 100%;
  //         text-align: right;
  //         margin-right: 30px;
  //       }
  //       .page-header-center{
  //           -webkit-print-color-adjust: exact;
  //           color:#000 !important;
  //           font-size:12px !important;
  //           width: 100%;
  //           margin-left: 30%;
  //         }
  //       .page-header-left {
  //         -webkit-print-color-adjust: exact;
  //         color:#000 !important;
  //         font-size:8px !important;
  //         text-align: left;
  //         margin-left: 30px;
  //       }
  //     </style>
  //     <span style='-webkit-print-color-adjust: exact;color:#000 !important;' class='page-header-left'>
  //       Molekada
  //     </span>
  //     <span style='-webkit-print-color-adjust: exact;color:#000 !important;' class='page-header'>
  //       <b> Saída </b> - ${moment().format('DD/MM/YYYY')}
  //     </span>`;

  //   const headers = new HttpHeaders().set('Content-Type', 'application/json');
  //   let body = {
  //     name: `Vendas-relatorio-data-${moment().format('DD-MM-YYYY')}`,
  //     html: btoa(encodeURIComponent(html)),
  //     footerHTML: btoa(encodeURIComponent(`
  //           <style>
  //             .page-footer{
  //               -webkit-print-color-adjust: exact;
  //               color:#000 !important;
  //               font-size:8px !important;
  //               width: 100%;
  //               text-align: center
  //             }
  //           </style>`)),
  //     headerHTML: btoa(encodeURIComponent(headerHTML))
  //   };

  //   this.http.post(
  //     `https://punnekmvqtxqiscaem3nbev5s40jmhqg.lambda-url.us-east-1.on.aws/dev/getPDF?isMobile=true`, body, {
  //     headers: headers
  //   }
  //   ).subscribe((data:any) => {
  //     window.open(data.url);
  //   }) as any;
    
  //   this.loadingPdf = false;
  // }
}

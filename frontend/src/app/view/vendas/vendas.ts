export interface Venda {
    id: string;
    name: string;
    codigo: string;
    preco_media: number;
    percentual_lucro: number;
    markup: number;
    custo: number;
    categoria_id: number;
    categoria_nome: string;
    itens_vendidos: number;
    id_loja?:string;
    createdAt: string;
    updatedAt: string;
}
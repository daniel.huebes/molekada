import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";

import { Router } from "@angular/router";
import { AppStorageService } from "./app-storage/app-storage.service";

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(
    private storageService: AppStorageService,
    private router: Router
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return new Observable(observer => {
      const token = this.storageService.get(AppStorageService.KEY_STORAGE.apiKey);
      if (token) {
        req = req.clone({
          setHeaders: {
            'x-api-key': token.authorization as any
          }
        });
      }
      const subscription = next.handle(req).subscribe(
        event => {
          if (event instanceof HttpResponse) {
            observer.next(event);
          }
        },
        err => {
          if (err.status === 403) {
            localStorage.clear();
            this.router.navigate(['/', 'auth']);
          }
          observer.error(err);
        },
        () => {
          observer.complete();
        }
      );
      return () => {
        subscription.unsubscribe();
      };
    });
  }
}

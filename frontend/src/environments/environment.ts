// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'portal-molekada',
    appId: '1:830344915664:web:e84a2b6d3ba90592bcccd5',
    storageBucket: 'portal-molekada.appspot.com',
    apiKey: 'AIzaSyBBaqtELNXZxov5ahrRyQRq5wdZJXN7GNM',
    authDomain: 'portal-molekada.firebaseapp.com',
    messagingSenderId: '830344915664',
    measurementId: 'G-V8T3B0NF4D',
  },
  production: false,
  backendAPI: "http://localhost:3000/dev"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

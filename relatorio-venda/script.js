//-------------------------------IMPORTS------------------------------------------

const moment = require('moment');
const axios = require('axios');
require('dotenv').config();
const FirebirdPool = require('./src/common/database/firebirdPool');
const firebirdClient = FirebirdPool.getInstance();
const { Pool } = require('pg');
var optionsPostgres = {};
optionsPostgres.host = 'db.dgsys.com.br';
optionsPostgres.port = 5432;
optionsPostgres.database = 'molekada2';
optionsPostgres.user = 'molekada';
optionsPostgres.password = 'DgsYs200_Molek@d@';
optionsPostgres.lowercase_keys = false; // set to true to lowercase keys
optionsPostgres.role = null;            // default
optionsPostgres.pageSize = 4096;
const postgres = new Pool({ ...optionsPostgres });

//-------------------------------IMPORTS------------------------------------------

//-------------------------------UTILS------------------------------------------

let colecoes = [];
let produtosColecoes = [];
let codigosProdutos = [];
let produtosVendas = [];
let produtosFiltrados = [];
let finalHTML = '';

//-------------------------------UTILS------------------------------------------



//-------------------------------CORE------------------------------------------

const script = async () => {
  console.log('Iniciando...');

  //Get ultimas coleções
  console.log('Buscando ultimas coleções...');
  await getColecao();
  console.log('Busca de coleções concluida.');

  //Get lista de vendas
  console.log('Buscando vendas...');
  await getVendas();
  console.log('Busca de vendas concluida.');

  //Get referencias de produtos das coleções
  console.log('Buscando referencia das coleções...');
  await getRefColecao();
  console.log('Busca de referencia das coleções concluida.');

  //Get Imagens
  console.log('Buscando imagens...');
  //await getImagens();
  console.log('Busca de imagens concluida.');

  //Monta organiza infos
  console.log('Organizando infos...');
  await organizaInfos();
  console.log('Infos organizadas.');

  //Monta PDF de envio
  console.log('Criando PDF...');
  await createPDF();
  console.log('Criação de PDF concluida.');

  //Envia arquivo gerado
  console.log('Enviando PDF...');
  await sendEmail();
  console.log('Envio de PDF concluida.');

  console.log('Script finalizado com sucesso.');
}

//-------------------------------CORE------------------------------------------



//-------------------------------FUNÇÕES------------------------------------------

const getColecao = async () => {
  try {
    let sql = `SELECT DESCRICAO, CODIGO, DT_INICIO FROM COLECAO_001 ORDER BY CODIGO DESC`;
    colecoes = await firebirdClient.executeQuery(sql);
    console.log(colecoes);
  } catch (error) {
    console.log(error);
  }
}

const getRefColecao = async () => {
  try {
    let sql = `SELECT COLECAO, CODIGO FROM PRODUTO_001 WHERE CODIGO IN (${codigosProdutos})`;
    produtosColecoes = await firebirdClient.executeQuery(sql);
  } catch (error) {
    console.log(error);
  }
}

const getVendas = async () => {
  try {
    let sql = `SELECT id, "name", codigo, categoria_id, categoria_nome, quantidade, valor_item, id_loja, data_venda, custo, "createdAt", "updatedAt", marketplace_id, marketplace_nome
                FROM public.products_vendas`;
    let resp = await postgres.query(sql);
    //console.log(produtosVendas.rows);
    produtosVendas = resp.rows;
    codigosInsert = {};

    produtosVendas.forEach((item) => {
      if (!item.codigo.includes("KIT")) {
        let cod = item.codigo.split('-');
        if (!codigosInsert[cod['0']]) {
          codigosProdutos.push(("'" + cod['0'] + "'"));
          codigosInsert[cod['0']] = 1;
        }
      }
    });
  } catch (error) {
    console.log(error);
  }
}

const getImagens = async () => {

}

const createPDF = async () => {
  let produtosHTML = '';

  for (let i = 0; i < colecoes.length; i++) {
    const colecao = colecoes[i];
    
    let produtos = produtosFiltrados.filter((tm) => tm.colecao == colecao.CODIGO);

    produtos = produtos.sort((a,b) => {
        if (a.qtd < b.qtd) {
          return 1;
        }
        if (a.qtd > b.qtd) {
          return -1;
        }
        return 0;
      })

    for (let p = 0; p < produtos.length; p++) {
      const produto = produtos[p];

      if (p == 0) {
        produtosHTML+= `<div class="col-12">
                            <h2>${colecao.DESCRICAO}</h2>
                            <hr>
                        </div>`;

        produtosHTML+= '<div class="row">';
        produtosHTML+= '<br>';
  
        produtosHTML+= `<div class="col-2">
                            <b>Imagem</b>
                        </div>`;
        produtosHTML+= `<div class="col-2">
                            <b>Cod.</b>
                        </div>`;
        produtosHTML+= `<div class="col-6">
                            <b>Descrição</b>
                        </div>`;
        produtosHTML+= `<div class="col-2">
                            <b>Qtd.</b>
                        </div>`;
        produtosHTML+= '<br>';
        
        produtosHTML+= '</div>';
        produtosHTML+= '<hr>';

      }
      produtosHTML+= '<div class="row">';
      produtosHTML+= '<br>';

      //<img src="./test.jpg" style="border-radius: 50%; width: 30px; height: 30px;">
      //<div style="border-radius: 50%; width: 50px; height: 50px; background-color: #666463"></div>

      produtosHTML+= `<div class="col-2">
                          Sem Info. 
                      </div>`;
      produtosHTML+= `<div class="col-2">
                          <b>${produto.codigo}</b>
                      </div>`;
      produtosHTML+= `<div class="col-6">
                          <b>${produto.desc}</b>
                      </div>`;
      produtosHTML+= `<div class="col-2">
                          <b>${produto.qtd}</b>
                      </div>`;
      produtosHTML+= '<br>';

      produtosHTML+= '</div>';
      produtosHTML+= '<hr>';

    }
  }

  finalHTML = `<html>
    <head>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <title>Estoque</title>
      <style>
        //........Customized style.......
      </style>
    </head>
    <body onload="window.print();">
      ${produtosHTML}
    </body>
  </html>`
}

const organizaInfos = async () => {
  let produtosAgrupados = [];
  let produtosAgrupadosInseridos = {};
  let referenciasFiltradas = [];
  let produtosFiltradosInseridos = {};

  for (let r = 0; r < produtosColecoes.length; r++) {
    const ref = produtosColecoes[r];
    
    let resp = colecoes.filter((tm) => tm.CODIGO == ref.COLECAO);
    if (resp[0]) {
      referenciasFiltradas.push(ref);
    }
  }

  for (let i = 0; i < produtosVendas.length; i++) {
    const venda = produtosVendas[i];
    
    let cod = venda.codigo.split('-');
    let resp = referenciasFiltradas.filter((tm) => tm.CODIGO == cod[0]);

    if (resp[0]) {
      venda.COLECAO = resp[0].COLECAO;
      if (produtosAgrupadosInseridos[resp[0].COLECAO] > -1) {
        produtosAgrupados[produtosAgrupadosInseridos[resp[0].COLECAO]].push(venda);
      } else {
        produtosAgrupadosInseridos[resp[0].COLECAO] = produtosAgrupados.length;
        produtosAgrupados[produtosAgrupados.length] = [venda];
      }
    }
  }

  for (let e = 0; e < produtosAgrupados.length; e++) {
    const porColecao = produtosAgrupados[e];
    
    for (let p = 0; p < porColecao.length; p++) {
      const venda = porColecao[p];
      let cod = venda.codigo.split('-');
      let codComplete = cod[0] + '-' + cod[1];
  
      if (produtosFiltradosInseridos[codComplete] > -1) {
        produtosFiltrados[produtosFiltradosInseridos[codComplete]].qtd += venda.quantidade;
      } else {  
        let desc = venda.name;
        if (venda.name.includes(" - ")) {
          desc = venda.name.split('-')[0];
        }
        produtosFiltradosInseridos[codComplete] = produtosFiltrados.length;
        produtosFiltrados[produtosFiltrados.length] = {desc: desc, qtd: venda.quantidade, colecao: venda.COLECAO, codigo: codComplete};
      }
    }
  }
  //console.log(produtosFiltrados);
}

const sendEmail = async (data, error) => {
  if (!error) {
    let header = `<style>
    .page-header{
      -webkit-print-color-adjust: exact;
      color: rgb(0, 0, 0) !important;
      font-size:8px !important;
      width: 100vw;
      text-align: right;
      margin-right: 30px;
    }
    .page-header-center{
        -webkit-print-color-adjust: exact;
        color: rgb(0, 0, 0) !important;
        font-size:12px !important;
        width: 100vw;
        margin-left: 30vw;
      }
    .page-header-left {
      -webkit-print-color-adjust: exact;
      color:    !important;
      font-size:8px !important;
      text-align: left;
      margin-left: 30px;
    }
  </style>
  <span style='-webkit-print-color-adjust: exact;color: rgb(0, 0, 0) !important; width: 100vw' class='page-header-left'>
    MOLEKADA - RELATORIO DE VENDA
  </span>
  <span style='-webkit-print-color-adjust: exact;color:rgb(0, 0, 0) !important;' class='page-header'>
    <b> ${moment().format('DD/MM/yyy HH:MM:ss')} </b>
  </span>`

    let foot = `<style>
        .page-footer{
            -webkit-print-color-adjust: exact;
            color:rgb(0, 0, 0) !important;
            font-size:8px !important;
            width: 100vw;
            text-align: center
        }
        </style>
        <span style='-webkit-print-color-adjust: exact;color:rgb(0, 0, 0) !important;' class='page-footer'>
        <b> Desenvolvido por DG Sistemas </b>
        </span>`
    let body = {
      // bucketName: 'cicampo-report',
      name: `Relatorio de venda`,
      html: Buffer.from(finalHTML),
      footerHTML: Buffer.from(foot),
      headerHTML: Buffer.from(header),
      from: null,
      emailDest: null,
      subject: null,
      bodyText: null,
    }

    //let email = 'gerente@molekada.com.br;felipe.pj.dev@gmail.com;thiago@molekada.com.br;dev@dgsys.com.br';
    let email = 'felipe.pj.dev@gmail.com';

    body.from = 'no.reply.dgsys@gmail.com';
    body.emailDest = email;
    body.subject = `Relatorio venda`;
    body.bodyText = `Em Anexo esta o PDF com as vendas de produtos.`;
    console.log(body);

    axios.post(`https://punnekmvqtxqiscaem3nbev5s40jmhqg.lambda-url.us-east-1.on.aws/?isMobile=true`, body, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).catch((err) => {
      console.log(err);
    });
  }
};

//-------------------------------FUNÇÕES------------------------------------------

script();

module.exports = {
  script: script
};
/** scripts */
const getImage = require('./script-image');
const getImageRef = require('./script-image-ref');
const getCollection = require('./script-collection');


const getImageScript = async () => {
  await getImage.script()
  .then(
      () => {
      },
      (error) => {
      }
  );
}

const getImageRefScript = async () => {
  await getImageRef.script()
  .then(
      () => {
      },
      (error) => {
      }
  );
}

const getCollectionScript = async () => {
  await getCollection.script()
  .then(
      () => {
      },
      (error) => {
      }
  );
}

module.exports = {
  getImageScript: getImageScript,
  getImageRefScript: getImageRefScript,
  getCollectionScript: getCollectionScript
};
//-------------------------------IMPORTS------------------------------------------

const moment = require('moment');
const { uuid } = require('uuidv4');
const axios = require('axios');
require('dotenv').config();
const FirebirdPool = require('./src/common/database/firebirdPool');
const firebirdClient = FirebirdPool.getInstance();
const { Pool } = require('pg');
var optionsPostgres = {};
optionsPostgres.host = 'db.dgsys.com.br';
optionsPostgres.port = 5432;
optionsPostgres.database = 'molekada2';
optionsPostgres.user = 'molekada';
optionsPostgres.password = 'DgsYs200_Molek@d@';
optionsPostgres.lowercase_keys = false; // set to true to lowercase keys
optionsPostgres.role = null;            // default
optionsPostgres.pageSize = 4096;
const postgres = new Pool({ ...optionsPostgres });

//-------------------------------IMPORTS------------------------------------------

//-------------------------------UTILS------------------------------------------

let images = [];

//-------------------------------UTILS------------------------------------------



//-------------------------------CORE------------------------------------------

const script = async () => {
  console.log('Iniciando...');

  //Get imagens
  console.log('Buscando imagens...');
  await getImagens();
  console.log('Busca de imagens concluida.');

  //Salva imagens na nuvem
  console.log('Enviando imagens...');
  await saveImagens();
  console.log('Envio de imagens concluida.');

  console.log('Script finalizado com sucesso.');
}

//-------------------------------CORE------------------------------------------



//-------------------------------FUNÇÕES------------------------------------------

const getImagens = async () => {
  try {
    
    let filterImg = {
      page: 1,
      limit: 100,
      allResults: false,
    };
    while (!filterImg.allResults) {
      let result = await axios.get(
        `https://molekada.painel.magazord.com.br/api/v2/site/frontend/produto/1?page=${filterImg.page}&limit=${filterImg.limit}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Basic YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=`,
          },
        }
      );
      //console.log(result);
      if (result.data.data.items.length === 0) {
        filterImg.allResults = true;
      } else if (result.data.data.items.length > 0) {
        filterImg.page += 1;
      }

      for (let i = 0; i < result.data.data.items.length; i++) {
        const image = result.data.data.items[i];
        
        if (image.midias[0] && image.midias[0].arquivo_nome) {
          console.log(image.codigo);
          let produto = image.codigo.split('-');
          images.push({
            nome_arquivo: image.midias[0].arquivo_nome,
            codigo_produto: produto[0] + '-' + produto[1]
          });
          console.log('midia: ', images[images.length - 1]);
        } else {
          console.log('não achou midia: ', image);
        }
      }
      // console.log(images[0]);
      console.log(images.length + '/' + result.data.data.total);
      //console.log('images: ', images.length);
      // console.log(images[images.length - 1]);
    }


  } catch (error) {
    console.log(error);
  }
}

const saveImagens = async () => {
  try {
    let sql = `SELECT id, "createdAt", "updatedAt", nome_arquivo, link, tipo_arquivo, codigo_produto
                FROM public.produtos_imagens`;

    let resp = await postgres.query(sql);
    //console.log(produtosVendas.rows);
    let imagesPostgres = resp.rows;
    let salvos = 0;
    let ignorados = 0;

    for (let i = 0; i < images.length; i++) {
      const image = images[i];

      let filter = imagesPostgres.filter((tm) => tm.codigo_produto == image.codigo_produto);

      if (filter[0]) {
        console.log('Achou');
        if (filter[0].nome_arquivo != image.nome_arquivo) {
          console.log('Update: ', image.nome_arquivo);

          let sqlUpdate = `UPDATE public.produtos_imagens
          SET nome_arquivo='${image.nome_arquivo}'
                          WHERE id = '${filter[0].id}'`;
          await postgres.query(sqlUpdate);
          salvos++;

        } else {
          console.log('Sem alteração: ', image.nome_arquivo);
          ignorados++;
        }
      } else {
        console.log('Criou: ', image.nome_arquivo);

        let sqlInsert = `UPDATE public.produtos_imagens
                          SET codigo_produto='${image.codigo_produto}'
                                          WHERE nome_arquivo='${image.nome_arquivo}'`;
        await postgres.query(sqlInsert);
        salvos++;
      }
      console.log('Restante: ', i + '/' + images.length);
      console.log('ig: ', ignorados);
      console.log('sv: ', salvos);
    }

  
  } catch (error) {
    console.log(error);
  }
}


//-------------------------------FUNÇÕES------------------------------------------

//script();

module.exports = {
  script: script
};
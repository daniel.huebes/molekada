const { resolve } = require('path');
const Service = require('node-windows').Service;

const scriptPath = resolve(__dirname, 'schedule.js');

// Create a new service object
var svc = new Service({
  name: 'DG Sys Scripts Coleções 1.1',
  description: 'DG Sys Coleções 1.1',
  script: scriptPath,
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install', () => {
  svc.start();
});

svc.install();

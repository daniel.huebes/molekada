const cron = require('node-cron');
const index = require('./index');

cron.schedule('0 16 * * *', () => {    
  index.getCollectionScript().then();
});

cron.schedule('0 21 * * *', () => {    
  index.getImageScript().then();
});

cron.schedule('0 20 * * *', () => {    
  index.getImageRefScript().then();
});



index.getCollectionScript().then();
//-------------------------------IMPORTS------------------------------------------

const moment = require('moment');
const { uuid } = require('uuidv4');
const axios = require('axios');
require('dotenv').config();
const FirebirdPool = require('./src/common/database/firebirdPool');
const firebirdClient = FirebirdPool.getInstance();
const { Pool } = require('pg');
var optionsPostgres = {};
optionsPostgres.host = 'db.dgsys.com.br';
optionsPostgres.port = 5432;
optionsPostgres.database = 'molekada2';
optionsPostgres.user = 'molekada';
optionsPostgres.password = 'DgsYs200_Molek@d@';
optionsPostgres.lowercase_keys = false; // set to true to lowercase keys
optionsPostgres.role = null;            // default
optionsPostgres.pageSize = 4096;
const postgres = new Pool({ ...optionsPostgres });

//-------------------------------IMPORTS------------------------------------------

//-------------------------------UTILS------------------------------------------

let colecoes = [];

//-------------------------------UTILS------------------------------------------



//-------------------------------CORE------------------------------------------

const script = async () => {
  console.log('Iniciando...');

  //Get coleções
  console.log('Buscando coleções...');
  await getColecao();
  console.log('Busca de coleções concluida.');

  //Salva coleções na nuvem
  console.log('Enviando Coleções...');
  await saveCollections();
  console.log('Envio de coleções concluida.');

  console.log('Script finalizado com sucesso.');
}

//-------------------------------CORE------------------------------------------



//-------------------------------FUNÇÕES------------------------------------------

const getColecao = async () => {
  try {
    let sql = `SELECT C.DESCRICAO, C.CODIGO, C.DT_INICIO, P.CODIGO AS COD_PRODUTO
                    FROM COLECAO_001 C, PRODUTO_001 P
                    WHERE P.COLECAO = C.CODIGO 
                    ORDER BY CODIGO DESC`;
    colecoes = await firebirdClient.executeQuery(sql);
    console.log(colecoes);
  } catch (error) {
    console.log(error);
  }
}

const saveCollections = async () => {
  try {
    let sql = `SELECT id, "createdAt", "updatedAt", descricao, codigo_colecao, data_inicio, codigo_produto
                    FROM public.produtos_colecoes`;
    let resp = await postgres.query(sql);
    //console.log(produtosVendas.rows);
    let colecoesPostgres = resp.rows;
    
    for (let i = 0; i < colecoes.length; i++) {
      const colecao = colecoes[i];

      let filter = colecoesPostgres.filter((tm) => tm.codigo_produto == colecao.COD_PRODUTO);

      if (filter[0]) {
        console.log('Achou');
        if (filter[0].codigo_colecao != colecao.CODIGO) {
          console.log('Update: ', colecao.COD_PRODUTO);

          let sqlUpdate = `UPDATE public.produtos_colecoes
          SET descricao='${colecao.DESCRICAO}', codigo_colecao='${colecao.CODIGO}', data_inicio='${new Date(colecao.DT_INICIO).toLocaleString()}'
                          WHERE id = '${filter[0].id}'`;
          await postgres.query(sqlUpdate);
        } else {
          console.log('Sem alteração: ', colecao.COD_PRODUTO);
        }
      } else {
        console.log('Criou: ', colecao.COD_PRODUTO);

        let sqlInsert = `INSERT INTO public.produtos_colecoes
                          (id, "createdAt", "updatedAt", descricao, codigo_colecao, data_inicio, codigo_produto)
                          VALUES('${uuid()}', now(), now(), '${colecao.DESCRICAO}', '${colecao.CODIGO}', '${new Date(colecao.DT_INICIO).toLocaleString()}', '${colecao.COD_PRODUTO}');`;
        await postgres.query(sqlInsert);
      }
      console.log('Restante: ', i + '/' + colecoes.length);
    }

  
  } catch (error) {
    console.log(error);
  }
}


//-------------------------------FUNÇÕES------------------------------------------

//script();

module.exports = {
  script: script
};
const axios = require('axios');

const token1 = 'Basic YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=';
const token2 = 'Basic YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=';

const getRecursive = async (url, token) => {
    let items = [];
    let hastItem = false;
    let page = 0;
    do {
        const object = await axios.get(`${url}?page=${page}&limit=100`, {
            headers: { Authorization: token }
        });
        console.log('Obtendo a pagina ' + page, url);
        if (object.data.data && object.data.data.items && object.data.data.items.length) {
            hastItem = true;
            items = items.concat(object.data.data.items);
        } else {
            hastItem = false;
        }
        page++;
    } while (hastItem);
    return items;
}

const diffProducts = (array1, array2) => {
    const array3 = [];
    for (let item of array1) {
        if (!array2.some(item2 => item2.codigo === item.codigo)) {
            array3.push(item);
        }
    }
    return array3;
}

const createCategory = async (category, url, token) => {
    const response = await axios.post(url, category, {
        headers: { Authorization: token }
    });
    return response.data.data;
}

const createProduction = async (product, categories, url, token) => {
    console.log(product);
    await axios.post(url, {
        codigo: product.codigo,
        nome: product.name,
        marca: product.marca,
        peso: product.peso,
        altura: product.altura,
        largura: product.largura,
        comprimento: product.comprimento,
        unidadeMedida: product.unidadeMedida,
        origemFiscal: product.origemFiscal,
        ativo: product.ativo,
        derivacoes: [],
        categorias: categories,
        ncm: product.ncm,
        cest: product.cest,
        dataLancamento: product.dataLancamento
    }, {
        headers: { Authorization: token }
    });
}

const createProductDerevation = async (url, token, otherUrl, otherToken) => {
    const response = await axios.get(otherUrl, {
        headers: { Authorization: otherToken }
    });
    if (response.data.data) {
        await axios.post(url, {
            codigo: response.data.data.codigo,
            nome: response.data.data.nome,
            peso: response.data.data.peso,
            altura: response.data.data.altura,
            largura: response.data.data.largura,
            comprimento: response.data.data.comprimento,
            ativo: response.data.data.ativo,
            ean: response.data.data.ean,
            derivacoes: response.data.data.derivacoes
        }, {
            headers: { Authorization: token }
        });
    }
}

const onProcess = async () => {
    const itensStore1 = await getRecursive('https://molekada.painel.magazord.com.br/api/v2/site/produto', token1);
    const categoriesStore1 = await getRecursive('https://molekada.painel.magazord.com.br/api/v2/site/categoria', token1);
    const itensStore2 = await getRecursive('https://amalook.painel.magazord.com.br/api/v2/site/produto', token2);
    const categoriesStore2 = await getRecursive('https://amalook.painel.magazord.com.br/api/v2/site/categoria', token2);
    const diffStore1 = diffProducts(itensStore1, itensStore2);
    const diffStore2 = diffProducts(itensStore2, itensStore1);

    for (let item of diffStore1) {
        const categories = [];
        for (let category of item.categorias) {
            const originalCategory = categoriesStore2.find(c => c.id === category);
            let categoryFinded = categoriesStore1.find(c => c.nome === originalCategory.nome);
            if (!categoryFinded) {
                console.log('Criando uma categoria');
                categoryFinded = await createCategory(originalCategory, 'https://molekada.painel.magazord.com.br/api/v2/site/categoria', token1);
                categoriesStore1.push(categoryFinded);
            }
            categories.push(categoryFinded.id);
        };
        await createProduction(
            item,
            categories,
            'https://molekada.painel.magazord.com.br/api/v2/site/produto',
            token1);
        for (let derivation of derivations) {
            await createProductDerevation(
                `https://molekada.painel.magazord.com.br/api/v2/site/produto/${item.codigo}/derivacao/`, token,
                `https://amalook.painel.magazord.com.br/api/v2/site/produto/${item.codigo}/derivacao/${derivation.codigo}`, otherToken);
        }
    }
}

onProcess();
"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("products", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      codigo: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      preco_media: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      percentual_lucro: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      markup: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      custo: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      categoria_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      categoria_nome: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      itens_vendidos: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("products");
  },
};

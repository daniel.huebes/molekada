"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("products_vendas", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      codigo: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      categoria_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      categoria_nome: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      quantidade: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      valor_item: {
        type: Sequelize.REAL,
        allowNull: false,
      },
      id_loja: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      data_venda: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      custo: {
      type: Sequelize.REAL,
      allowNull: true,
    },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("products_vendas");
  },
};

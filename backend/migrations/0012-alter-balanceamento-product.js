"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn("balanceamento_products", "percentual_mini_magia", {
      type: Sequelize.REAL,
      allowNull: true
    });
  },
};

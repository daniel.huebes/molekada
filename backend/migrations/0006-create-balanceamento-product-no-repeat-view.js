"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize
      .query(`create or replace view balanceamento_product_no_repeat_view as 
        select
            bpv2.*
        from
            (
            select
                (
                select
                    id
                from
                    balanceamento_product_view bpv
                where
                    bpv.codigo like concat(b.codigo, '%')
                limit 1) codigo
            from
                (
                select
                    *
                from
                    (
                    select
                        left(codigo, char_length(codigo)-2) codigo
                    from
                        products) a
                where
                    codigo not like '%-'
                group by
                    codigo
                order by
                    codigo) b) c
        inner join balanceamento_product_view bpv2 on
            (bpv2.id = c.codigo)`);
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("balanceamento_product_no_repeat_view");
  },
};

"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize
      .query(`create or replace view balanceamento_product_view as 
    select 
    p.id, 
    bp.id as id_balanceamento,
    p.name, 
    p.codigo, 
    p.itens_vendidos,
    bp.percentual_molekada, 
    bp.percentual_amalook,
    p."createdAt",
    p."updatedAt"
    from products p 
    left join balanceamento_products bp on (p.id = bp.product_id)`);
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("balanceamento_product_view");
  },
};

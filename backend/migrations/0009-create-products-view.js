"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize
      .query(`create or replace view products_view as 
      select 
			row_number () OVER () AS id,
            codigo2 codigo,
            "name",
            categoriaid, 
            categorianome,
            avg(preco_media) preco_media, 
            avg(percentual_lucro) percentual_lucro,
            avg(markup) markup,
            avg(custo) custo,
            sum(itens_vendidos) itens_vendidos,
            max("createdAt") "createdAt",
            max("updatedAt") "updatedAt"
        from 
            (
                SELECT 
                    id, 
                    split_part("name", '-', 1) "name", 
                    concat(split_part(codigo, '-', 1), '-', split_part(codigo, '-', 2)) codigo2, 
                    categoria_id categoriaid, 
                    categoria_nome categorianome,
                    preco_media, 
                    percentual_lucro, 
                    markup, custo, 
                    categoria_id, 
                    categoria_nome, 
                    itens_vendidos, 
                    "createdAt", 
                    "updatedAt"
                FROM 
                    public.products
            ) products 
        group by 
            "name", 
            codigo2,
            categoriaid, 
            categorianome`);
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("products_view");
  },
};

"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize
      .query(`create or replace view products_updated_view as 
        select 
            row_number () OVER () AS id,
            max("updatedAt") "updatedAt",
            max("createdAt") "createdAt" 
        from 
            products`);
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("products_updated_view");
  },
};

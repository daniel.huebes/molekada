"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn("products_vendas", "marketplace_id", {
      type: Sequelize.INTEGER,
      allowNull: true,
    });
    await queryInterface.addColumn("products_vendas", "marketplace_nome", {
      type: Sequelize.STRING(250),
      allowNull: true,
    });
  },
  down: async (queryInterface, Sequelize) => {},
};

import type { AWS } from '@serverless/typescript';

import authentication from '@lambdas/authentication';
import product from '@lambdas/product';
import productView from '@lambdas/product-view';
import productUpdatedView from '@lambdas/product-updated-view';
import balanceamentoProduct from '@lambdas/balanceamento-product';
import balanceamentoProductView from '@lambdas/balanceamento-product-view';
import balanceamentoProductNoRepeatView from '@lambdas/balanceamento-product-no-repeat-view';
import balanceamentoLoja from '@lambdas/balanceamento-loja';

const serverlessConfiguration: AWS = {
  service: 'molekada-v2',
  frameworkVersion: '3',
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['pg-native'],
      target: 'node18',
      define: { 'require.resolve': undefined },
      platform: 'node',
    },
  },
  plugins: ['serverless-esbuild', 'serverless-offline'],
  package: {
    individually: true
  },
  provider: {
    name: 'aws',
    region: 'sa-east-1',
    runtime: 'nodejs18.x',
    memorySize: 450,
    timeout: 30,
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true
    },
    environment: {
      PRODUCTION: 'prod',
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      JWT_SECURITY: 'GVAUIjI&pM5qJ@u$QqjqFJdT',
      AMAZON_KEY: 'AKIAUVZN2UWAYYGCVRQN',
      AMAZON_SECRET_KEY: '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',
      AWS_BUCKET: 'molekada'
    },
    lambdaHashingVersion: '20201221',
  },
  functions: { authentication, product, productView, productUpdatedView, balanceamentoProduct, balanceamentoProductView, balanceamentoProductNoRepeatView, balanceamentoLoja }
};

module.exports = serverlessConfiguration;

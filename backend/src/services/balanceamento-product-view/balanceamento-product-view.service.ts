import { Payload } from '@libs/handler.decorators';
import { CrudService } from '@libs/service.crud';
import { BalanceamentoProductView, BalanceamentoProductViewFactory } from '@models/balanceamento-products/balanceamento-product-view.model';

export class BalanceamentoProductViewService extends CrudService<BalanceamentoProductView> {

  constructor(payload: Payload) {
    super(payload, BalanceamentoProductViewFactory());
  }

}
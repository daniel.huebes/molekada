import { Payload } from '@libs/handler.decorators';
import { CrudService } from '@libs/service.crud';
import { ProductUpdatedView, ProductUpdatedViewFactory } from '@models/products/product-updated-view.model';

export class ProductUpdatedViewService extends CrudService<ProductUpdatedView> {

  constructor(payload: Payload) {
    super(payload, ProductUpdatedViewFactory());
  }

}
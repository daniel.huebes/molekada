import { ForbiddenException } from '@exceptions/forbidden.exception';
import { Product, ProductFactory } from '@models/products/product.model';
import { CrudService, iFilter } from '@libs/service.crud';
import { Payload } from '@libs/handler.decorators';
import { Database } from '@libs/database';
import axios from 'axios';
import { QueryTypes } from 'sequelize';

var iconv = require("iconv-lite");

export class ProductService extends CrudService<Product> {

  constructor(payload: Payload) {
    super(payload, ProductFactory());
  }

  public async productMetrics(body) {
    console.log('Body recebido:' + JSON.stringify(body));
    try {
      let retorno = {
        success: true
      };

      body.arrayPedidoRastreio.forEach(pedidoRastreio => {
        pedidoRastreio.pedidoItem.forEach(async item => {

          let productDb = await this.getAll("codigo", item.produtoDerivacaoCodigo);

          console.log('Buscando produto ' + item.produtoDerivacaoCodigo + ' no banco de dados...');

          if (productDb.length > 0) {
            console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi encontrado no banco de dados. Realizando cálculos');

            let custo = productDb[0].custo;
            let precoVenda = item.valorItem;
            let mediaVenda = (productDb[0].preco_media + precoVenda) / 2;
            let lucro = precoVenda - custo;
            let percentualLucro = (lucro / precoVenda) * 100;
            let markup = ((precoVenda - custo) / 100) * 100;

            let produto = productDb[0];

            produto.preco_media = mediaVenda;
            produto.percentual_lucro = percentualLucro;
            produto.markup = markup;
            produto.itens_vendidos = productDb[0].itens_vendidos + item.quantidade;

            await this.update(produto.id, produto).then(res => {
              console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi atualizado com sucesso. Response: ' + res);
            }).catch(error => {
              console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi atualizado, um erro ocorreu: ' + error);
            });
          } else {
            console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi encontrado no banco de dados. Buscando estoque no Magazord da Molekada');

            let uri = 'https://molekada.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
            let produtoEstoque = await axios.get(uri, {
              headers: {
                'Authorization': 'Basic YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk='
              }
            });

            if (produtoEstoque.data.length > 0) {
              console.log('Produto ' + item.produtoDerivacaoCodigo + ' teve seu estoque encontrado no Magazord da Molekada. Realizando cálculos');

              let custo = produtoEstoque.data.data[0].custoMedio;
              let precoVenda = item.valorItem;
              let mediaVenda = precoVenda;
              let lucro = precoVenda - custo;
              let percentualLucro = (lucro / precoVenda) * 100;
              let markup = ((precoVenda - custo) / 100) * 100;

              let produto: Product = <Product>{
                name: produtoEstoque.data.data[0].descricaoProduto,
                codigo: produtoEstoque.data.data[0].produto,
                preco_media: mediaVenda,
                percentual_lucro: percentualLucro,
                markup: markup,
                custo: custo,
                categoria_id: item.categoria_id,
                categoria_nome: item.categoria,
                itens_vendidos: item.quantidade
              };

              await this.create(produto).then(res => {
                console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi cadastrado com sucesso. Response: ' + res);
              }).catch(error => {
                console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi cadastrado, um erro ocorreu: ' + error);
              });
            } else {
              console.log('Produto ' + item.produtoDerivacaoCodigo + ' não teve seu estoque encontrado no Magazord da Molekada. Buscando no Magazord da Amalook');

              let uri = 'https://amalook.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
              produtoEstoque = await axios.get(uri, {
                headers: {
                  'Authorization': 'Basic YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY='
                }
              });

              if (produtoEstoque.data.length > 0) {
                console.log('Produto ' + item.produtoDerivacaoCodigo + ' teve seu estoque encontrado no Magazord da Amalook. Realizando cálculos');

                let custo = produtoEstoque.data.data[0].custoMedio;
                let precoVenda = item.valorItem;
                let mediaVenda = precoVenda;
                let lucro = precoVenda - custo;
                let percentualLucro = (lucro / precoVenda) * 100;
                let markup = ((precoVenda - custo) / 100) * 100;

                let produto: Product = <Product>{
                  name: produtoEstoque.data.data[0].descricaoProduto,
                  codigo: produtoEstoque.data.data[0].produto,
                  preco_media: mediaVenda,
                  percentual_lucro: percentualLucro,
                  markup: markup,
                  custo: custo,
                  categoria_id: item.categoria_id,
                  categoria_nome: item.categoria,
                  itens_vendidos: item.quantidade
                };

                await this.create(produto).then(res => {
                  console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi cadastrado com sucesso. Response: ' + res);
                }).catch(error => {
                  console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi cadastrado, um erro ocorreu: ' + error);
                });
              } else {
                console.log('Produto ' + item.produtoDerivacaoCodigo + ' não teve seu estoque encontrado no Magazord da Molekada e Amalook. Ignorando produto');
              }
            }
          }
        });
      });

      return retorno;
    } catch (err) {
      console.log('erro no try catch: ' + err);
      if (err?.response?.data?.error?.message) {
        throw new ForbiddenException(err?.response?.data?.error?.message);
      } else {
        throw err;
      }
    }
  }

  public async cargaDeDados(filter) {
    let uri = '';
    let token = '';

    if (filter.loja == 'molekada') {
      uri = 'https://molekada.painel.magazord.com.br/api/v2/site/pedido';
      token = 'YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=';
    } else if (filter.loja == 'amalook') {
      uri = 'https://amalook.painel.magazord.com.br/api/v2/site/pedido';
      token = 'YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=';
    } else {
      throw new ForbiddenException('Apenas as lojas "molekada" e "amalook" estão disponíveis.');
    }

    let page = 1;
    let limit = 100;
    let startDate = filter.startDate;
    let situacoes = "1,3,4,5,6,7,8";

    let execute = true;
    let pedidosTotais = [];

    while (execute) {
      let pedidosAPI = await axios.get(uri, {
        headers: {
          'Authorization': 'Basic ' + token
        },
        params: {
          "dataHora[gte]": startDate,
          "situacao": situacoes,
          "page": page,
          "limit": limit
        }
      });

      if (pedidosAPI.data.data.items.length > 0) {
        pedidosAPI.data.data.items.forEach(pedido => {
          pedidosTotais.push(pedido);
        });
      }

      if (pedidosAPI.data.data.has_more) {
        page = page + 1;
      } else {
        execute = false;
      }
    }

    let finalizado = false;

    const intervalObject = setInterval(async () => {
      let requisicoes = 0;
      let indexPedido = 0;
      console.log('Iniciando carga de dados com limite de 1000 requisições');

      while (requisicoes <= 1000) {
        if (typeof pedidosTotais[indexPedido] !== 'undefined') {

          let pedidoAPI = await axios.get(uri + '/' + pedidosTotais[indexPedido].codigo, {
            headers: {
              'Authorization': 'Basic ' + token
            }
          });

          pedidoAPI.data.data.arrayPedidoRastreio.forEach(pedidoRastreio => {
            pedidoRastreio.pedidoItem.forEach(async item => {

              let productDb = await this.getAll("codigo", item.produtoDerivacaoCodigo);
              console.log('Buscando produto ' + item.produtoDerivacaoCodigo + ' no banco de dados...');

              if (productDb.length > 0) {
                console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi encontrado no banco de dados. Realizando cálculos');

                let custo = productDb[0].custo;
                let precoVenda = item.valorItem;
                let mediaVenda = (productDb[0].preco_media + precoVenda) / 2;
                let lucro = precoVenda - custo;
                let percentualLucro = (lucro / precoVenda) * 100;
                let markup = ((precoVenda - custo) / 100) * 100;

                let produto = productDb[0];

                produto.preco_media = mediaVenda;
                produto.percentual_lucro = percentualLucro;
                produto.markup = markup;
                produto.itens_vendidos = productDb[0].itens_vendidos + item.quantidade;

                await this.update(produto.id, produto).then(res => {
                  console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi atualizado com sucesso. Response: ' + JSON.stringify(res));
                }).catch(error => {
                  console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi atualizado, um erro ocorreu: ' + JSON.stringify(error));
                });
              } else {
                console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi encontrado no banco de dados. Buscando estoque no Magazord');

                let uriEstoque = '';
                let tokenEstoque = '';

                if (filter.loja == 'molekada') {
                  uriEstoque = 'https://molekada.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
                  tokenEstoque = 'YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=';
                } else if (filter.loja == 'amalook') {
                  uriEstoque = 'https://amalook.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
                  tokenEstoque = 'YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=';
                } else {
                  throw new ForbiddenException('Apenas as lojas "molekada" e "amalook" estão disponíveis.');
                }

                let produtoEstoque = await axios.get(uriEstoque, {
                  headers: {
                    'Authorization': 'Basic ' + tokenEstoque
                  }
                });
                requisicoes = requisicoes + 1;

                if (produtoEstoque.data) {
                  console.log('Produto ' + item.produtoDerivacaoCodigo + ' teve seu estoque encontrado no Magazord. Realizando cálculos');

                  let custo = produtoEstoque.data.data[0].custoMedio;
                  let precoVenda = item.valorItem;
                  let mediaVenda = precoVenda;
                  let lucro = precoVenda - custo;
                  let percentualLucro = (lucro / precoVenda) * 100;
                  let markup = ((precoVenda - custo) / 100) * 100;

                  let produto: Product = <Product>{
                    name: produtoEstoque.data.data[0].descricaoProduto,
                    codigo: produtoEstoque.data.data[0].produto,
                    preco_media: mediaVenda,
                    percentual_lucro: percentualLucro,
                    markup: markup,
                    custo: custo,
                    categoria_id: item.categoria_id,
                    categoria_nome: item.categoria,
                    itens_vendidos: item.quantidade
                  };

                  await this.create(produto).then(res => {
                    console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi cadastrado com sucesso. Response: ' + JSON.stringify(res));
                  }).catch(error => {
                    console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi cadastrado, um erro ocorreu: ' + JSON.stringify(error));
                  });
                } else {
                  console.log('Produto ' + item.produtoDerivacaoCodigo + ' não teve seu estoque encontrado no Magazord. Ignorando produto');
                }
              }
            });
          });

          requisicoes = requisicoes + 1;
          indexPedido = indexPedido + 1;
        } else {
          requisicoes = 1001;
          finalizado = true;
        }

        if (requisicoes == 1001) {
          if (!finalizado) {
            pedidosTotais.splice(0, 1001);
          }
        }
      }

      console.log('Carga finalizada');

      if (finalizado) {
        console.log('Finalizando Requisições');
        clearInterval(intervalObject);
        console.log('Requisições finalizadas');
      } else {
        console.log('Proxima carga será enviada no próximo minuto');
      }
    }, 60000);

    return {
      success: true
    }
  }


  public async atualizaCusto(filter) {
    let dados = (await this.list({
      size: 4000
    })).contents;

    for (let i = 0; i < dados.length; i++) {
      const produto = dados[i];

      await (new Promise(resolve => setTimeout(resolve, 100)));

      let uri = 'https://amalook.painel.magazord.com.br/api/v1/listEstoque?produto=' + produto.codigo;
      let produtoEstoque = await axios.get(uri, {
        headers: {
          'Authorization': 'Basic YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY='
        }
      });

      if (produtoEstoque.data && produtoEstoque.data.data[0]) {
        if (produtoEstoque.data.data[0].custoMedio > 0) {
          //console.log(produtoEstoque.data.data[0]);
          produto.custo = produtoEstoque.data.data[0].custoMedio;
          let precoVenda = produto.preco_media;
          let lucro = precoVenda - produto.custo;
          produto.percentual_lucro = (lucro / precoVenda) * 100;
          produto.markup = ((precoVenda - produto.custo) / 100) * 100;

          await this.update(produto.id, produto);
        } else {
          console.log('Sem custo: ', produto.codigo);
        }
      } else {
        console.log('Não encontrado: ', produto.codigo);
      }
    }
  }

  public async listCategories() {
    let query = `SELECT distinct categoria_id, categoria_nome FROM public.products;`;
    const conciliated = await (Database.get(this.payload.schema)).query(query,
      {
        type: QueryTypes.SELECT
      }
    );

    return conciliated;
  }

}
import { ForbiddenException } from '@exceptions/forbidden.exception';
import { CrudService, iFilter, } from '@libs/service.crud';
import { Payload } from '@libs/handler.decorators';
import axios from 'axios';
import { ProductItem, ProductItemFactory } from '@models/products/product-item.model';
import { Database } from '@libs/database';
import { QueryTypes } from 'sequelize';
import { S3Service } from '@services/s3/s3.service';
//const htmlToPdf = require('html-pdf-node');

const pdf = require('html-pdf');
const path = require('path');

//const puppeteer = require('puppeteer'); 
var iconv = require("iconv-lite");

export class ProductItemService extends CrudService<ProductItem> {

  constructor(payload: Payload, onExporting = false) {
    super(payload, ProductItemFactory());
  }

  public async productMetrics(body) {
    console.log('Body recebido:' + JSON.stringify(body));
    try {
      let retorno = {
        success: true,
        msg: ''
      };
      let produtoEstoque;
      let product;
      for (let pedidoRastreio of body.arrayPedidoRastreio) {
        for (let item of pedidoRastreio.pedidoItem) {

          let produtoDb = await this.getAll("codigo", item.produtoDerivacaoCodigo);
          console.log('Buscando produto ' + item.produtoDerivacaoCodigo + ' no banco de dados...');

          if (produtoDb.length > 0) {
            let produto: ProductItem = <ProductItem>{
                name: produtoDb[0].name,
                codigo: produtoDb[0].codigo,
                custo: produtoDb[0].custo,
                categoria_id: item.categoria_id,
                categoria_nome: item.categoria,
                quantidade: item.quantidade,
                valor_item: item.valorItem,
                data_venda: body.dataHora,
                id_loja: body.id_loja,
                marketplace_id: body?.lojaDoMarketplaceId,
                marketplace_nome: body?.lojaDoMarketplaceNome
              };
              product = produto;
          } else {
            /*##### Buscando dados no estoque Molekada ######*/
            console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi encontrado no banco de dados, buscando no estoque Magazord da Molekada');

            let uri = 'https://molekada.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
            produtoEstoque = await axios.get(uri, {
              headers: {
                'Authorization': 'Basic YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk='
              }
            });

            if (produtoEstoque.data.length < 1) {
              /*##### Buscando dados no estoque Molekada ######*/
              console.log('Produto ' + item.produtoDerivacaoCodigo + ' não teve seu estoque encontrado no Magazord da Molekada. Buscando no Magazord da Amalook');

              let uri = 'https://amalook.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
              produtoEstoque = await axios.get(uri, {
                headers: {
                  'Authorization': 'Basic YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY='
                }
              });
            }
            if (produtoEstoque.data.length < 1) {
              console.log('Produto ' + item.produtoDerivacaoCodigo + ' não teve seu estoque encontrado no Magazord da Molekada e Amalook. Ignorando produto');
              return retorno = {
                success: false,
                msg: 'Produdo não foi encontrado no Magazord da Molekada e da Amalook. Produto ignorado'
              };
            }

            if (produtoEstoque.data.data.length > 0) {
              let produto: ProductItem = <ProductItem>{
                name: produtoEstoque.data.data[0].descricaoProduto,
                codigo: produtoEstoque.data.data[0].produto,
                custo: produtoEstoque.data.data[0].custoMedio,
                categoria_id: item.categoria_id,
                categoria_nome: item.categoria,
                quantidade: item.quantidade,
                valor_item: item.valorItem,
                data_venda: body.dataHora,
                id_loja: body.id_loja, 
                marketplace_id: body?.lojaDoMarketplaceId ?? 999,
                marketplace_nome: body?.lojaDoMarketplaceNome ?? 'Site',
              };
              product = produto;
            }
          }

          await this.create(product).then(res => {
            console.log('Produto ' + item.produtoDerivacaoCodigo + ' foi cadastrado com sucesso. Response: ' + JSON.stringify(res));
          }).catch(error => {
            console.log('Produto ' + item.produtoDerivacaoCodigo + ' não foi cadastrado, um erro ocorreu: ' + JSON.stringify(error));
          })
        }
        return retorno;
      }
    } catch (err) {
      console.log('erro no try catch: ' + err);
      if (err?.response?.data?.error?.message) {
        throw new ForbiddenException(err?.response?.data?.error?.message);
      } else {
        throw err;
      }
    }
  }


  public async custom(filter: iFilter, onExport = false) {
    let view = `
    SELECT p.name, 
      p.codigo, 
      AVG(p.valor_item) as preco_media, 
      AVG(p.custo) as custo,
      ((AVG(p.valor_item) - AVG(p.custo)) / AVG(p.valor_item)) * 100 as percentual_lucro,
      ((AVG(p.valor_item) - AVG(p.custo)) / 100) *100 as markup,
      p.categoria_id,
      p.categoria_nome,
      SUM(p.quantidade) as itens_vendidos,
      COUNT(*) OVER () AS totalElements
      FROM 
      products_vendas p
      `;



    const group = `
      GROUP BY 
      p.codigo, 
      p.name, 
      p.categoria_id, 
      p.categoria_nome
      ORDER BY "${filter.orderBy}" ${filter.orderBySense}`;
    const list = await this.customList(filter, view, group, onExport);

    return list;
  }

  public async customByCod(filter: iFilter, onExport = false) {
    let view = `
      SELECT
        name,
        codigo2 AS codigo,
        AVG(preco_media) AS preco_media,
        AVG(custo) AS custo,
        ((AVG(preco_media) - AVG(custo)) / AVG(preco_media)) * 100 AS percentual_lucro,
        ((AVG(preco_media) - AVG(custo)) / 100) * 100 AS markup,
        categoriaid AS categoria_id,
        categorianome AS categoria_nome,
        SUM(itens_vendidos) AS itens_vendidos,
        COUNT(*) OVER () AS totalElements
      FROM (
        SELECT
          split_part("name", '-', 1) AS name,
          concat(split_part(codigo, '-', 1), '-', split_part(codigo, '-', 2)) AS codigo2,
          categoria_id AS categoriaid,
          categoria_nome AS categorianome,
          AVG(valor_item) AS preco_media,
          AVG(custo) AS custo,
          SUM(quantidade) AS itens_vendidos
        FROM
          public.products_vendas
    `;

    const group = `
        GROUP BY
          split_part("name", '-', 1),
          concat(split_part(codigo, '-', 1), '-', split_part(codigo, '-', 2)),
          categoria_id,
          categoria_nome
      ) AS subquery
      GROUP BY
        name,
        codigo2,
        categoriaid,
        categorianome
        ORDER BY "${filter.orderBy}" ${filter.orderBySense}
    `;

    const list = await this.customList(filter, view, group, onExport);

    return list;
  }

  public async exportCustomList(filter: iFilter) {
    const entities = (await this.custom(filter, true)).contents;
    let csv = "Código; Produto; Preço médio; Percentual lucro; Markup; Custo; Categoria; Itens vendidos\n";

    for (let i = 0; i < entities.length; i++) {
      const content = entities[i] as any;
      csv += `${content?.codigo ?? ''};${content?.name ?? ''};${String(content?.preco_media.toFixed(2)).replace('.', ',') ?? ''};${content?.percentual_lucro.toFixed(2)}%;${Number(content?.markup).toFixed(2) ?? ''};${String(content?.custo.toFixed(2)).replace('.', ',') ?? ''};${content.categoria_nome ?? ''};${content.itens_vendidos}\n`;
    }
    var output = iconv.encode(csv, "ISO-8859-1");

    return output as any;
  }

  public async exportCustListByCod(filter: iFilter) {
    const entities = (await this.customByCod(filter, true)).contents;
    let csv =
      "Código; Produto; Preço médio; Percentual lucro; Markup; Custo; Itens vendidos\n";

    for (let i = 0; i < entities.length; i++) {
      const content = entities[i];
      csv += `${content?.codigo ?? ""};${content?.name ?? ""};${Number(content?.preco_media).toFixed(2).replace(".", ",") ?? ""};${Number(content?.percentual_lucro).toFixed(2)}%;${Number(content?.markup).toFixed(2) ?? ""};${String(content?.custo.toFixed(2)).replace(".", ",") ?? ""};${content.itens_vendidos}\n`;
    }

    var output = iconv.encode(csv, "ISO-8859-1");

    return output as any;
  }

  public async listMarketplaces() {
    let query = `SELECT distinct marketplace_id, marketplace_nome FROM public.products_vendas;`;
    const marketplaces = await (Database.get(this.payload.schema)).query(query, {
      type: QueryTypes.SELECT
    });
    return marketplaces;
  }

  public async getSalesHisotry(filter:any) {
    let sales = [];
    console.log('--------------------------------------------------------------------------------------------------');
    console.log(filter);
    // console.log(filter.filter);
    // console.log(filter.filter.filter);
    // console.log(filter.filter.filter);
    // console.log(filter.filter.filter[0]);
    // console.log(JSON.parse(filter.filter.filter[0]))
    console.log('--------------------------------------------------------------------------------------------------');

    let sql = `SELECT 
                  sum(pv.quantidade) as qtd,
                  SUBSTRING(pv.name FROM 1 FOR POSITION('-' IN pv.name) - 1) AS descricao_prod,
                  pc.codigo_colecao,
                  pc.descricao,
                  pc.data_inicio,
                  SUBSTRING(pv.codigo FROM 1 FOR POSITION('-' IN pv.codigo) - 1) AS parte1,
                  SUBSTRING(pv.codigo FROM POSITION('-' IN pv.codigo) + 1 FOR POSITION('-' IN SUBSTRING(pv.codigo FROM POSITION('-' IN pv.codigo) + 1)) - 1) AS parte2,
                  COUNT(*) AS repeticoes
              FROM 
                  public.products_vendas pv
              inner JOIN 
                  produtos_colecoes pc ON pc.codigo_produto = SUBSTRING(pv.codigo FROM 1 FOR POSITION('-' IN pv.codigo) - 1)
              where pv.data_venda > '${filter.inicio}' and pv.data_venda < '${filter.fim}' 
                    ${filter.colecao ? `AND pc.codigo_colecao = '${filter.colecao.codigo_colecao}'` : ''}
                    ${filter.categoria ? `AND pv.categoria_id = ${filter.categoria.categoria_id}` : ''}
              GROUP BY
                  descricao_prod,
                  pc.codigo_colecao,
                  pc.data_inicio,
                  pc.descricao,
                  parte1,
                  parte2
              ORDER by pc.codigo_colecao desc,
                  qtd desc
              LIMIT 100 
              OFFSET ${filter.first};`

    sales = await (Database.get(this.payload.schema)).query(sql,
      {
        type: QueryTypes.SELECT
      }
    );
    //console.log(sales);
    return sales;
  }
  
  public async getCategory() {
    let category = [];
    
    let sql = `SELECT categoria_nome, categoria_id
                FROM public.products_vendas
                group by categoria_nome, categoria_id;`

    category = await (Database.get(this.payload.schema)).query(sql,
      {
        type: QueryTypes.SELECT
      }
    );
    return category;
  }

  public async getImage(array :any) {
    let images = [];
    
    let sql = `SELECT *
                FROM public.produtos_imagens
                WHERE codigo_produto IN (${array.join(',')});`

    images = await (Database.get(this.payload.schema)).query(sql,
      {
        type: QueryTypes.SELECT
      }
    );
    console.log(images);
    return images;
  }

  public async getCollection() {
    let collection = [];
    
    let sql = `select pc.codigo_colecao, pc.descricao from produtos_colecoes pc 
                  group by pc.codigo_colecao, pc.descricao 
                  order by pc.codigo_colecao desc`

    collection = await (Database.get(this.payload.schema)).query(sql,
      {
        type: QueryTypes.SELECT
      }
    );
    console.log(collection);
    return collection;
  }


  public async atualizaDados(filter) {
    let uri = '';
    let token = '';

    if(filter.loja == 'molekada') {
      uri = 'https://molekada.painel.magazord.com.br/api/v2/site/pedido';
      token = 'YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=';
    } else if(filter.loja == 'amalook') {
      uri = 'https://amalook.painel.magazord.com.br/api/v2/site/pedido';
      token = 'YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=';
    } else {
      throw new ForbiddenException('Apenas as lojas "molekada" e "amalook" estão disponíveis.');
    }

    let page = 1;
    let limit = 100;
    let startDate = filter.startDate;
    let endDate = filter.endDate;
    let situacoes = "1,3,4,5,6,7,8";

    let execute = true;
    let pedidosTotais = [];

    while(execute) {
      console.log('Execuntando requisição ' + page + '...')
      let pedidosAPI = await axios.get(uri, {
        headers: {
          'Authorization': 'Basic ' + token
        },
        params: {
          "dataHora[gte]": startDate,
          "dataHora[lte]": endDate,
          "situacao": situacoes, 
          "page": page,
          "limit": limit
        }
      });

      if(pedidosAPI.data.data.items.length > 0) {
        pedidosAPI.data.data.items.forEach(pedido => {
          pedidosTotais.push(pedido);
        });
      }

      console.log(pedidosAPI.data.data.has_more)
      if(pedidosAPI.data.data.has_more) {
        page = page + 1;
      } else {
        execute = false;
      }
    }

    let finalizado = false;

    const intervalObject = setInterval(async() => {
      let requisicoes = 0;
      let indexPedido = 0;
      console.log('Iniciando carga de dados com limite de 1000 requisições');

      while(requisicoes <= 1000) {
        if(typeof pedidosTotais[indexPedido] !== 'undefined') {

          let pedidoAPI = await axios.get(uri + '/' + pedidosTotais[indexPedido].codigo, {
            headers: {
              'Authorization': 'Basic ' + token
            }
          });

          for(let pedidoRastreio of pedidoAPI.data.data.arrayPedidoRastreio) {
            for(let item of pedidoRastreio.pedidoItem) {
                  
                  console.log('Buscando produto '+item.produtoDerivacaoCodigo+' no estoque no Magazord');
  
                  let uriEstoque = '';
                  let tokenEstoque = '';
                  
                  if(filter.loja == 'molekada') {
                    uriEstoque = 'https://molekada.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
                    tokenEstoque = 'YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=';
                  } else if(filter.loja == 'amalook') {
                    uriEstoque = 'https://amalook.painel.magazord.com.br/api/v1/listEstoque?produto=' + item.produtoDerivacaoCodigo;
                    tokenEstoque = 'YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=';
                  } else {
                    throw new ForbiddenException('Apenas as lojas "molekada" e "amalook" estão disponíveis.');
                  }
  
                  let produtoEstoque = await axios.get(uriEstoque, {
                    headers: {
                      'Authorization': 'Basic ' + tokenEstoque
                    }
                  });
                  requisicoes = requisicoes + 1;
  
  
                  if(produtoEstoque.data) {
                    console.log('Produto '+item.produtoDerivacaoCodigo+' teve seu estoque encontrado no Magazord');
  
  
                    let produto: ProductItem = <ProductItem>{
                      name: produtoEstoque.data.data[0].descricaoProduto,
                      codigo: produtoEstoque.data.data[0].produto,
                      custo: produtoEstoque.data.data[0].custoMedio,
                      categoria_id: item.categoria_id,
                      categoria_nome: item.categoria,
                      quantidade: item.quantidade,
                      valor_item: item.valorItem,
                      data_venda: pedidoAPI.data.data.dataHora,
                      id_loja: pedidoAPI.data.data.id_loja,
                      marketplace_id: pedidoAPI.data.data.lojaDoMarketplaceId ?? 999,
                      marketplace_nome: pedidoAPI.data.data.lojaDoMarketplaceNome ?? 'Site',
                    };
  
                     this.create(produto).then(res => {
                      console.log('Produto '+item.produtoDerivacaoCodigo+' foi cadastrado com sucesso. Response: '+res);
                    }).catch(error => {
                      console.log('Produto '+item.produtoDerivacaoCodigo+' não foi cadastrado, um erro ocorreu: '+error);
                    });
                  } else {
                    console.log('Produto '+item.produtoDerivacaoCodigo+' não teve seu estoque encontrado no Magazord. Ignorando produto');
                  };
          }};

          requisicoes = requisicoes + 1;
          indexPedido = indexPedido + 1;
        } else {
          requisicoes = 1001;
          finalizado = true;
        }
        
        if(requisicoes == 1001) {
          if(!finalizado) {
            pedidosTotais.splice(0, 1001);
          }
        }
      }

      console.log('Carga finalizada');

      if(finalizado) {
        console.log('Finalizando Requisições');
        clearInterval(intervalObject);
        console.log('Requisições finalizadas');
      } else {
        console.log('Proxima carga será enviada no próximo minuto');
      }
    }, 60000);
  }

  private async generatePDF() {
    console.log('entrou pdf');
    
    
    // HTML como string
    const htmlContent = `
    <!DOCTYPE html>
    <html>
    <head>
        <title>Exemplo de PDF</title>
        <style>
            body { font-family: Arial, sans-serif; }
            h1 { color: #333; }
            p { font-size: 14px; }
        </style>
    </head>
    <body>
        <h1>Exemplo de Geração de PDF</h1>
        <p>Este é um exemplo de como gerar um PDF a partir de um HTML utilizando node-html-pdf.</p>
    </body>
    </html>
    `;
    
    // Opções do PDF
    const options = { format: 'A4', type: 'pdf' };
    
    // Gerar PDF
    console.log('Vai gerar PDF');
    
    pdf.create(htmlContent, options).toBuffer((err, buffer) => {
        if (err) return console.log('ERRO AO GERAR: -------------------------', err);
    
        // Converte o buffer para base64
        const pdfBase64 = buffer.toString('base64');
        console.log(pdfBase64);
    });
  }

  private async saveFile(id: string, fileName: string) {
    let buffer = this.generatePDF();
    const params = {
      Bucket: "dalmir-orcamentos",
      //Key: fileName,
      // Body: pdf
    };
    await new S3Service().putObject(params);
  }

}
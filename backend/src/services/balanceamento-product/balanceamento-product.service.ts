import { BalanceamentoProduct, BalanceamentoProductFactory } from '@models/balanceamento-products/balanceamento-product.model';
import { CrudService } from '@libs/service.crud';
import { Payload } from '@libs/handler.decorators';
import {  Op } from 'sequelize';

export class BalanceamentoProductService extends CrudService<BalanceamentoProduct> {

  constructor(payload: Payload) { 
    super(payload, BalanceamentoProductFactory());
  }

  public async getAllByLoja(field:string) {
    let where = {};
    where[field] = {
      [Op.gt]: 0
    }

    let entities = await this.entity.findAll({
      where: where
    });
    entities = entities.map(data => data.dataValues);

    return entities.length > 0;
  }

}
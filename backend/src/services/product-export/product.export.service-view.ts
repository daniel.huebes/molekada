import { CrudService } from '@libs/service.crud';
import { Payload } from '@libs/handler.decorators';
import { ProductView, ProductViewFactory } from '@models/products/product-view.model';

var iconv = require("iconv-lite");

export class ProductExportViewService extends CrudService<ProductView> {
  constructor(payload: Payload) {
    super(payload, ProductViewFactory());
  }

  protected afterList(entities: ProductView[]): Promise<ProductView[]> {
    let csv =
      "Código; Produto; Preço médio; Percentual lucro; Markup; Custo; Itens vendidos\n";

    for (let i = 0; i < entities.length; i++) {
      const content = entities[i];
      csv += `${content?.codigo ?? ""};${content?.name ?? ""};${Number(content?.preco_media).toFixed(2).replace(".", ",") ?? ""};${Number(content?.percentual_lucro).toFixed(2)}%;${Number(content?.markup).toFixed(2) ?? ""};${String(content?.custo.toFixed(2)).replace(".", ",") ?? ""};${content.itens_vendidos}\n`;
    }

    var output = iconv.encode(csv, "ISO-8859-1");

    return output as any;       
  }
}
import { Product, ProductFactory } from '@models/products/product.model';
import { CrudService, iFilter } from '@libs/service.crud';
import { Payload } from '@libs/handler.decorators';

var iconv = require("iconv-lite");

export class ProductExportService extends CrudService<Product> {

  constructor(payload: Payload) { 
    super(payload, ProductFactory());
  }

  protected afterList(entities: Product[]): Promise<Product[]> {
    let csv = "Código; Produto; Preço médio; Percentual lucro; Markup; Custo; Categoria; Itens vendidos\n";

    for(let i= 0; i < entities.length; i++) {
      const content = entities[i];
      csv += `${content?.codigo ?? ''};${content?.name ?? ''};${String(content?.preco_media.toFixed(2)).replace('.', ',') ?? ''};${content?.percentual_lucro.toFixed(2)}%;${Number(content?.markup).toFixed(2) ?? ''};${String(content?.custo.toFixed(2)).replace('.', ',') ?? ''};${content.categoria_nome ?? ''};${content.itens_vendidos}\n`;
    }

    var output = iconv.encode(csv, "ISO-8859-1");

    return output as any;
  }
  
}
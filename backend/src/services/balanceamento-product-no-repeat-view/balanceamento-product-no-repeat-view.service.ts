import { Payload } from '@libs/handler.decorators';
import { CrudService } from '@libs/service.crud';
import { BalanceamentoProductNoRepeatView, BalanceamentoProductNoRepeatViewFactory } from '@models/balanceamento-products/balanceamento-product-no-repeat-view.model';

export class BalanceamentoProductNoRepeatViewService extends CrudService<BalanceamentoProductNoRepeatView> {

  constructor(payload: Payload) {
    super(payload, BalanceamentoProductNoRepeatViewFactory());
  }

  protected async afterList(entities: BalanceamentoProductNoRepeatView[]): Promise<BalanceamentoProductNoRepeatView[]> {
    for(let entity of entities) {
      entity.molekadaCheck = entity.percentual_molekada > 0;
      entity.amalookCheck = entity.percentual_amalook > 0;
      entity.miniMagiaCheck = entity.percentual_mini_magia > 0;
    }

    return entities;
  }

}
import { BalanceamentoLoja, BalanceamentoLojaFactory } from '@models/balanceamento-lojas/balanceamento-loja.model';
import { CrudService } from '@libs/service.crud';
import { Payload } from '@libs/handler.decorators';

export class BalanceamentoLojaService extends CrudService<BalanceamentoLoja> {

  constructor(payload: Payload) { 
    super(payload, BalanceamentoLojaFactory());
  }

}
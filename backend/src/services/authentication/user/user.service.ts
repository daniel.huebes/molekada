import * as admin from 'firebase-admin';
import axios from 'axios';

import { Payload } from '@libs/handler.decorators';
import { CrudService } from '@libs/service.crud';

import { NotFoundException } from '@exceptions/not-found.exception';

import { User, UserFactory } from '@models/authorization/user.model';

const serviceAccount = require('../../../../portal-molekada-41ad27748386');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

export class UserService extends CrudService<User> {

  constructor(payload: Payload) {
    super(payload, UserFactory());
  }

  protected async beforeCreate(entity: User): Promise<User> {
    const passAt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const passArray = Array.from({ length: 8 });
    entity.email = entity.email.trim();
    (entity as any).password = passArray.map(function () {
      return passAt.charAt(Math.random() * passAt.length);
    }).join('');
    await admin.auth().createUser({
      email: entity.email,
      password: (entity as any).password,
      emailVerified: true
    });
    entity.isAdmin = !!entity.isAdmin;
    return entity;
  }

  protected async afterCreate(entity: User, entitySaved: User): Promise<User> {
    await axios.post('https://qfqun7q0vi.execute-api.us-east-1.amazonaws.com/Prod/email', {

      email: entitySaved.email,
      message: `Olá!

Segue os dados de acesso ao Sistema Portal Molekada

Link: https://portal-molekada.web.app/
Usuário: ${entity.email}
Senha: ${(entity as any).password}

          `,
      subject: "Bem vindo - Portal Molekada"
    });
    return entitySaved;
  }

  protected async beforeDelete(id: string): Promise<void> {
    const user = await this.get(id);
    try {
      const userFinded = await admin.auth().getUserByEmail(user.email);
      if (userFinded) {
        await admin.auth().deleteUser(userFinded.uid);
      }
    } catch (err) {
      console.log(err);
    }
  }

  public async resetPassword(email: string) {
    const link = await admin.auth().generatePasswordResetLink(email);

    await axios.post('https://qfqun7q0vi.execute-api.us-east-1.amazonaws.com/Prod/email', {

      email: email,
      message: `Olá!

Clique no link abaixo para redefinir sua senha.

${link}

Caso tenha algum problema para acessar, fale com o responsável pelo sistema.
          `,
      subject: "Solicitação de troca de senha - Portal Molekada"
    });
  }

  public async getUserByEmail(email: string) {
    const users = await this.getAll("email", email);
    if (!users?.length) {
      throw new NotFoundException(`E-mail ${email} não encontrado!`);
    }
    return users[0];
  }

  public async getUsersAdmin() {
    return await UserFactory().findAll({
      where: {
        isAdmin: true
      }
    });
  }

}
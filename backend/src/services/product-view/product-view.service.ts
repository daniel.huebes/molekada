import { Payload } from '@libs/handler.decorators';
import { CrudService } from '@libs/service.crud';
import { ProductView, ProductViewFactory } from '@models/products/product-view.model';

export class ProductViewService extends CrudService<ProductView> {

  constructor(payload: Payload) {
    super(payload, ProductViewFactory());
  }

}
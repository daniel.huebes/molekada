import { BuildOptions, DataTypes, Model } from 'sequelize';

import { Database } from '@libs/database';

interface ProductUpdatedViewAttributes {
  id: number;
  createdAt: number;
  updatedAt: number;
}

export interface ProductUpdatedViewModel extends Model<ProductUpdatedViewAttributes>, ProductUpdatedViewAttributes { }

export class ProductUpdatedView extends Model<ProductUpdatedViewModel, ProductUpdatedViewAttributes> implements ProductUpdatedViewAttributes {
    id: number;
    createdAt: number;
    updatedAt: number;
}

export type ProductUpdatedViewStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): ProductUpdatedViewModel;
};

export function ProductUpdatedViewFactory(): ProductUpdatedViewStatic {
  return <ProductUpdatedViewStatic>Database.get('public').define('products_updated_view', {
    id: {
      field: 'id',
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    freezeTableName: true
  });
}

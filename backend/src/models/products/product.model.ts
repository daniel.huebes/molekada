import { BuildOptions, DataTypes, Model } from 'sequelize';
import { v4 } from 'uuid';

import { Database } from '@libs/database';

interface ProductAttributes {
  id?: string;
  name: string;
  codigo: string;
  preco_media: number;
  percentual_lucro: number;
  markup: number;
  custo: number;
  categoria_id: number;
  categoria_nome: string;
  itens_vendidos: string;
  createdAt?: Date;
  updatedAt?: string;
}

export interface ProductModel extends Model<ProductAttributes>, ProductAttributes { }

export class Product extends Model<ProductModel, ProductAttributes> implements ProductAttributes {
  id?: string;
  name: string;
  codigo: string;
  preco_media: number;
  percentual_lucro: number;
  markup: number;
  custo: number;
  categoria_id: number;
  categoria_nome: string;
  itens_vendidos: string;
  createdAt?: Date;
  updatedAt?: string;
}

export type ProductStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): ProductModel;
};

export function ProductFactory(): ProductStatic {
  return <ProductStatic>Database.get('public').define('products', {
    id: {
      type: DataTypes.UUIDV4,
      autoIncrement: true,
      primaryKey: true,
      defaultValue: v4()
    },
    name: {
      type: DataTypes.STRING(250),
      allowNull: false,
      validate: {
        len: {
          args: [1, 250],
          msg: 'Nome deve conter de 1 a 250 caracteres'
        },
        notNull: {
          msg: 'Nome não pode se nulo'
        },
        notEmpty: {
          msg: 'Nome não pode se nulo'
        }
      }
    },
    codigo: {
      type: DataTypes.STRING(250),
      allowNull: false,
      validate: {
        len: {
          args: [1, 250],
          msg: 'Código deve conter de 1 a 250 caracteres'
        },
        notNull: {
          msg: 'Código não pode ser nulo'
        },
        notEmpty: {
          msg: 'Código não pode ser nulo'
        }
      }
    },
    preco_media: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Preço médio não pode ser nulo'
        },
        notEmpty: {
          msg: 'Preço médio não pode ser nulo'
        }
      }
    },
    percentual_lucro: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Percentual de lucro não pode ser nulo'
        },
        notEmpty: {
          msg: 'Percentual de lucro não pode ser nulo'
        }
      }
    },
    markup: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
            notNull: {
            msg: 'Markup não pode ser nulo'
            },
            notEmpty: {
            msg: 'Markup não pode ser nulo'
            }
        }
    },
    custo: {
        type: DataTypes.FLOAT,
        allowNull: false,
        validate: {
            notNull: {
            msg: 'Custo não pode ser nulo'
            },
            notEmpty: {
            msg: 'Custo não pode ser nulo'
            }
        }
    },
    categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'ID da categoria não pode se nulo'
        },
        notEmpty: {
          msg: 'ID da categoria não pode se nulo'
        }
      }
    },
    categoria_nome: {
      type: DataTypes.STRING(250),
      allowNull: false,
      validate: {
        len: {
          args: [1, 250],
          msg: 'Categoria deve conter de 1 a 250 caracteres'
        },
        notNull: {
          msg: 'Categoria não pode se nulo'
        },
        notEmpty: {
          msg: 'Categoria não pode se nulo'
        }
      }
    },
    itens_vendidos: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    freezeTableName: true
  });
}

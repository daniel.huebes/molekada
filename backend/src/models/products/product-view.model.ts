import { BuildOptions, DataTypes, Model } from 'sequelize';

import { Database } from '@libs/database';

interface ProductViewAttributes {
  id: number;
  codigo: string;
  name: string;
  preco_media: string;
  percentual_lucro: string;
  markup: number;
  custo: number;
  itens_vendidos: number;
  createdAt: number;
  updatedAt: number;
}

export interface ProductViewModel extends Model<ProductViewAttributes>, ProductViewAttributes { }

export class ProductView extends Model<ProductViewModel, ProductViewAttributes> implements ProductViewAttributes {
    id: number;
    codigo: string;
    name: string;
    preco_media: string;
    percentual_lucro: string;
    markup: number;
    custo: number;
    itens_vendidos: number;
    createdAt: number;
    updatedAt: number;
}

export type ProductViewStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): ProductViewModel;
};

export function ProductViewFactory(): ProductViewStatic {
  return <ProductViewStatic>Database.get('public').define('products_view', {
    id: {
      field: 'id',
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    codigo: {
      field: 'codigo',
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      field: 'name',
      type: DataTypes.STRING,
      allowNull: false
    },
    preco_media: {
      field: 'preco_media',
      type: DataTypes.FLOAT,
      allowNull: false
    },
    percentual_lucro: {
      field: 'percentual_lucro',
      type: DataTypes.FLOAT,
      allowNull: false
    },
    markup: {
        field: 'markup',
        type: DataTypes.FLOAT,
        allowNull: false
    },
    custo: {
        field: 'custo',
        type: DataTypes.FLOAT,
        allowNull: false
    },
    itens_vendidos: {
        field: 'itens_vendidos',
        type: DataTypes.INTEGER,
        allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    freezeTableName: true
  });
}

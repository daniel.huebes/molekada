import { BuildOptions, DataTypes, Model } from 'sequelize';
import { v4 } from 'uuid';

import { Database } from '@libs/database';

interface ProductItemAttributes {
  id?: string;
  name: string;
  codigo: string;
  categoria_id: number;
  categoria_nome: string;
  quantidade: number;
  valor_item: number;
  id_loja: number;
  data_venda: Date;
  createdAt?: Date;
  updatedAt?: string;
  custo: number;
  marketplace_nome?: string;
  marketplace_id?: number;
}

export interface ProductItemModel extends Model<ProductItemAttributes>, ProductItemAttributes { }

export class ProductItem extends Model<ProductItemModel, ProductItemAttributes> implements ProductItemAttributes {
  id?: string;
  name: string;
  codigo: string;
  categoria_id: number;
  categoria_nome: string;
  quantidade: number;
  valor_item: number;
  id_loja: number;
  data_venda: Date;
  createdAt?: Date;
  updatedAt?: string;
  custo: number;
  marketplace_nome?: string;
  marketplace_id?: number;
}

export type ProductItemStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): ProductItemModel;
};

export function ProductItemFactory(): ProductItemStatic {
  return <ProductItemStatic>Database.get('public').define('products_vendas', {
    id: {
      type: DataTypes.UUIDV4,
      autoIncrement: true,
      primaryKey: true,
      defaultValue: v4()
    },
    name: {
      type: DataTypes.STRING(250),
      allowNull: false,
      validate: {
        len: {
          args: [1, 250],
          msg: 'Nome deve conter de 1 a 250 caracteres'
        },
        notNull: {
          msg: 'Nome não pode se nulo'
        },
        notEmpty: {
          msg: 'Nome não pode se nulo'
        }
      }
    },
    codigo: {
      type: DataTypes.STRING(250),
      allowNull: false,
      validate: {
        len: {
          args: [1, 250],
          msg: 'Código deve conter de 1 a 250 caracteres'
        },
        notNull: {
          msg: 'Código não pode ser nulo'
        },
        notEmpty: {
          msg: 'Código não pode ser nulo'
        }
      }
    },
    categoria_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'ID da categoria não pode se nulo'
        },
        notEmpty: {
          msg: 'ID da categoria não pode se nulo'
        }
      }
    },
    id_loja: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'ID da loja não pode se nulo'
        },
        notEmpty: {
          msg: 'ID da loja não pode se nulo'
        }
      }
    },
    categoria_nome: {
      type: DataTypes.STRING(250),
      allowNull: false,
      validate: {
        len: {
          args: [1, 250],
          msg: 'Categoria deve conter de 1 a 250 caracteres'
        },
        notNull: {
          msg: 'Categoria não pode se nulo'
        },
        notEmpty: {
          msg: 'Categoria não pode se nulo'
        }
      }
    },
    quantidade: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Quantidade não pode ser nulo'
        },
        notEmpty: {
          msg: 'Quantidade não pode ser nulo'
        }
      }
    },
    valor_item: {
      type: DataTypes.REAL,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Valor não pode ser nulo'
        },
        notEmpty: {
          msg: 'Valor não pode ser nulo'
        }
      }
    },
    custo: {
      type: DataTypes.REAL,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Custo não pode ser nulo'
        },
        notEmpty: {
          msg: 'Custo não pode ser nulo'
        }
      }
    },
    data_venda: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Data da venda não pode ser nulo'
        },
        notEmpty: {
          msg: 'Data da venda não pode ser nulo'
        }
      }
    },
    marketplace_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    marketplace_nome: {
      type: DataTypes.STRING(250),
      allowNull: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    freezeTableName: true
  });
}

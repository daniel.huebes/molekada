import { BuildOptions, DataTypes, Model } from 'sequelize';
import { v4 } from 'uuid';

import { Database } from '@libs/database';

interface BalanceamentoProductAttributes {
    id?: string;
    product_id: string;
    percentual_molekada: number;
    percentual_amalook: number;
    percentual_mini_magia:number;
    createdAt?: string;
    updatedAt?: string;
}

export interface BalanceamentoProductModel extends Model<BalanceamentoProductAttributes>, BalanceamentoProductAttributes { }

export class BalanceamentoProduct extends Model<BalanceamentoProductModel, BalanceamentoProductAttributes> implements BalanceamentoProductAttributes {
    id?: string;
    product_id: string;
    percentual_molekada: number;
    percentual_amalook: number;
    percentual_mini_magia:number;
    createdAt?: string;
    updatedAt?: string;
}

export type BalanceamentoProductStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): BalanceamentoProductModel;
};

export function BalanceamentoProductFactory(): BalanceamentoProductStatic {
  return <BalanceamentoProductStatic>Database.get('public').define('balanceamento_products', {
    id: {
      type: DataTypes.UUIDV4,
      autoIncrement: true,
      primaryKey: true,
      defaultValue: v4()
    },
    product_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Produto ID não pode se nulo'
        },
        notEmpty: {
          msg: 'Produto ID não pode ser nulo'
        }
      }
    },
    percentual_molekada: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    percentual_amalook: {
        type: DataTypes.FLOAT,
        allowNull: true
    },
    percentual_mini_magia: {
      type: DataTypes.FLOAT,
      allowNull: true
  },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    freezeTableName: true
  });
}

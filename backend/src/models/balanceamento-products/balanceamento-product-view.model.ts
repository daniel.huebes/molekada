import { BuildOptions, DataTypes, Model } from 'sequelize';
import { v4 } from 'uuid';

import { Database } from '@libs/database';

interface BalanceamentoProductViewAttributes {
  id: string;
  id_balanceamento: string;
  name: string;
  codigo: string;
  itens_vendidos: string;
  percentual_molekada: number;
  percentual_amalook: number;
  percentual_mini_magia: number;
  createdAt: number;
  updatedAt: number;
}

export interface BalanceamentoProductViewModel extends Model<BalanceamentoProductViewAttributes>, BalanceamentoProductViewAttributes { }

export class BalanceamentoProductView extends Model<BalanceamentoProductViewModel, BalanceamentoProductViewAttributes> implements BalanceamentoProductViewAttributes {
    id: string;
    name: string;
    id_balanceamento: string;
    codigo: string;
    itens_vendidos: string;
    percentual_molekada: number;
    percentual_amalook: number;
    percentual_mini_magia: number;
    createdAt: number;
    updatedAt: number;
}

export type BalanceamentoProductViewStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): BalanceamentoProductViewModel;
};

export function BalanceamentoProductViewFactory(): BalanceamentoProductViewStatic {
  return <BalanceamentoProductViewStatic>Database.get('public').define('balanceamento_product_view', {
    id: {
      type: DataTypes.UUIDV4,
      autoIncrement: true,
      primaryKey: true,
      defaultValue: v4()
    },
    id_balanceamento: {
      type: DataTypes.UUIDV4,
      allowNull: true,
      defaultValue: v4()
    },
    name: {
      field: 'name',
      type: DataTypes.STRING,
      allowNull: false
    },
    codigo: {
      field: 'codigo',
      type: DataTypes.STRING,
      allowNull: false
    },
    itens_vendidos: {
      field: 'itens_vendidos',
      type: DataTypes.INTEGER,
      allowNull: false
    },
    percentual_molekada: {
        field: 'percentual_molekada',
        type: DataTypes.FLOAT,
        allowNull: false
    },
    percentual_amalook: {
        field: 'percentual_amalook',
        type: DataTypes.FLOAT,
        allowNull: false
    },
    percentual_mini_magia: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    freezeTableName: true
  });
}

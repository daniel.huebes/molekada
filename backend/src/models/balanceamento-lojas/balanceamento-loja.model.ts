import { BuildOptions, DataTypes, Model } from 'sequelize';
import { v4 } from 'uuid';

import { Database } from '@libs/database';

interface BalanceamentoLojaAttributes {
    id?: string;
    name: string;
    percentual_balanceamento: number;
    createdAt?: string;
    updatedAt?: string;
}

export interface BalanceamentoLojaModel extends Model<BalanceamentoLojaAttributes>, BalanceamentoLojaAttributes { }

export class BalanceamentoLoja extends Model<BalanceamentoLojaModel, BalanceamentoLojaAttributes> implements BalanceamentoLojaAttributes {
    id?: string;
    name: string;
    percentual_balanceamento: number;
    createdAt?: string;
    updatedAt?: string;
}

export type BalanceamentoLojaStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): BalanceamentoLojaModel;
};

export function BalanceamentoLojaFactory(): BalanceamentoLojaStatic {
  return <BalanceamentoLojaStatic>Database.get('public').define('balanceamento_lojas', {
    id: {
      type: DataTypes.UUIDV4,
      autoIncrement: true,
      primaryKey: true,
      defaultValue: v4()
    },
    name: {
      type: DataTypes.STRING(250),
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Nome não pode se nulo'
        },
        notEmpty: {
          msg: 'Nome não pode ser nulo'
        }
      }
    },
    percentual_balanceamento: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Percentual de balanceamento não pode ser nulo'
        },
        notEmpty: {
          msg: 'Percentual de balanceamento não pode ser nulo'
        }
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    }
  }, {
    freezeTableName: true
  });
}

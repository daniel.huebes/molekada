import { BalanceamentoProductViewHandler } from '@handlers/balanceamento-product-view/balanceamento-product-view.handler';

export class Handlers {

    public static autoload() {
        new BalanceamentoProductViewHandler();
    }

}
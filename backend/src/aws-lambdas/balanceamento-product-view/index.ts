import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/balanceamento-product-view.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/balanceamento-product-view-api/{proxy+}',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    },
    {
      http: {
        method: 'any',
        path: '/balanceamento-product-view-api/',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    }
  ]
}
import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/balanceamento-loja.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/balanceamento-loja-api/{proxy+}',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    },
    {
      http: {
        method: 'any',
        path: '/balanceamento-loja-api/',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    }
  ]
}
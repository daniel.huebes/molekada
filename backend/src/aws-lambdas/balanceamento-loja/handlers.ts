import { BalanceamentoLojaHandler } from '@handlers/balanceamento-loja/balanceamento-loja.handler';

export class Handlers {

    public static autoload() {
        new BalanceamentoLojaHandler();
    }

}
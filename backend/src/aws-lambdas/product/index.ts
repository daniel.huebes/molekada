import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/product.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/product-api/{proxy+}',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    },
    {
      http: {
        method: 'any',
        path: '/product-api/',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    }
  ]
}
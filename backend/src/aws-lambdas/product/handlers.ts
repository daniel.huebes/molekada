import { ProductHandler } from '@handlers/product/product.handler';

export class Handlers {

    public static autoload() {
        new ProductHandler();
    }

}
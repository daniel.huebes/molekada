import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/balanceamento-product.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/balanceamento-product-api/{proxy+}',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    },
    {
      http: {
        method: 'any',
        path: '/balanceamento-product-api/',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    }
  ]
}
import { BalanceamentoProductHandler } from '@handlers/balanceamento-product/balanceamento-product.handler';

export class Handlers {

    public static autoload() {
        new BalanceamentoProductHandler();
    }

}
import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/balanceamento-product-no-repeat-view.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/balanceamento-product-no-repeat-view-api/{proxy+}',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    },
    {
      http: {
        method: 'any',
        path: '/balanceamento-product-no-repeat-view-api/',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    }
  ]
}
import { BalanceamentoProductNoRepeatViewHandler } from '@handlers/balanceamento-product-no-repeat-view/balanceamento-product-no-repeat-view.handler';

export class Handlers {

    public static autoload() {
        new BalanceamentoProductNoRepeatViewHandler();
    }

}
import { ProductUpdatedViewHandler } from '@handlers/product-updated-view/product-updated-view.handler';

export class Handlers {

    public static autoload() {
        new ProductUpdatedViewHandler();
    }

}
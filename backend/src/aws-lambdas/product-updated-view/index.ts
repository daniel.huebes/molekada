import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/product-updated-view.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/product-updated-view-api/{proxy+}',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    },
    {
      http: {
        method: 'any',
        path: '/product-updated-view-api/',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    }
  ]
}
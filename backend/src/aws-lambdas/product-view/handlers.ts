import { ProductViewHandler } from '@handlers/product-view/product-view.handler';

export class Handlers {

    public static autoload() {
        new ProductViewHandler();
    }

}
import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/product-view.main`,
  events: [
    {
      http: {
        method: 'any',
        path: '/product-view-api/{proxy+}',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    },
    {
      http: {
        method: 'any',
        path: '/product-view-api/',
        cors: {
          origin: '*',
          headers: ['*']
        }
      }
    }
  ]
}
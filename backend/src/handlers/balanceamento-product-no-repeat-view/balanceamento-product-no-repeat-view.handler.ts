import { API, Payload } from '@libs/handler.decorators';

import { APIEnum } from '@enuns/api.enum';
import { Transactional } from '@enuns/transactional.enum';
import { CrudHandler } from '@libs/handler.crud';
import { BalanceamentoProductNoRepeatViewService } from '@services/balanceamento-product-no-repeat-view/balanceamento-product-no-repeat-view.service';
import { Auth } from "@enuns/auth.enum";

export class BalanceamentoProductNoRepeatViewHandler extends CrudHandler {

  @API(`/balanceamento-product-no-repeat-view`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async list(payload: Payload) {
    return new BalanceamentoProductNoRepeatViewService(payload).list(payload.filter);
  }

}
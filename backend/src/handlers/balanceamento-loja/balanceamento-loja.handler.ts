import { API, Payload } from '@libs/handler.decorators';

import { APIEnum } from '@enuns/api.enum';
import { Transactional } from '@enuns/transactional.enum';
import { CrudHandler } from '@libs/handler.crud';
import { BalanceamentoLojaService } from '@services/balanceamento-loja/balanceamento-loja.service';
import { Auth } from "@enuns/auth.enum";

export class BalanceamentoLojaHandler extends CrudHandler {

  @API(`/balanceamento-loja/:id`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async get(payload: Payload) {
    return new BalanceamentoLojaService(payload).get(payload.id);
  }

  @API(`/balanceamento-loja`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async list(payload: Payload) {
    return new BalanceamentoLojaService(payload).list(payload.filter);
  }

  @API(`/balanceamento-loja`, APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED)
  public async create(payload: Payload) {
    return new BalanceamentoLojaService(payload).create(payload.body);
  }

  @API(`/balanceamento-loja/:id`, APIEnum.PUT, Transactional.TRANSACTION, Auth.REQUIRED)
  public async update(payload: Payload) {
    return new BalanceamentoLojaService(payload).update(payload.id, payload.body);
  }

  @API(`/balanceamento-loja/:id`, APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED)
  public async delete(payload: Payload) {
    return new BalanceamentoLojaService(payload).delete(payload.id);
  }

}
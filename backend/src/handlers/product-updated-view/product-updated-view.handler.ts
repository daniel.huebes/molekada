import { API, Payload } from '@libs/handler.decorators';

import { APIEnum } from '@enuns/api.enum';
import { Transactional } from '@enuns/transactional.enum';
import { CrudHandler } from '@libs/handler.crud';
import { ProductUpdatedViewService } from '@services/product-updated-view/product-updated-view.service';
import { Auth } from "@enuns/auth.enum";

export class ProductUpdatedViewHandler extends CrudHandler {

  @API(`/product-updated-view`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async list(payload: Payload) {
    return new ProductUpdatedViewService(payload).list(payload.filter);
  }

}
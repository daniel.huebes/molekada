import { API, Payload } from '@libs/handler.decorators';

import { APIEnum } from '@enuns/api.enum';
import { Transactional } from '@enuns/transactional.enum';
import { CrudHandler } from '@libs/handler.crud';
import { ProductService } from '@services/product/product.service';
import { Auth } from "@enuns/auth.enum";
import * as AWS from "aws-sdk";
import { ProductExportService } from '@services/product-export/product.export.service';
import { ProductExportViewService } from "@services/product-export/product.export.service-view";
import { ProductItemService } from '@services/product-item/product-item.service';

const moment = require("moment");

AWS.config.update({ region: "sa-east-1" });
const s3 = new AWS.S3({
  signatureVersion: "v4",
  accessKeyId: process.env.AMAZON_KEY,
  secretAccessKey: process.env.AMAZON_SECRET_KEY,
});


export class ProductHandler extends CrudHandler {

  @API(`/product-metrics`, APIEnum.POST, Transactional.TRANSACTION, Auth.NO_AUTH)
  public async productMetrics(payload: Payload) {
    new ProductItemService(payload).productMetrics(payload.body);
    return new ProductService(payload).productMetrics(payload.body);
  }

  @API(`/product-view`, APIEnum.GET, Transactional.TRANSACTION, Auth.REQUIRED)
  public async listCustomProduct(payload: Payload) {
    return new ProductItemService(payload).custom(payload.filter);
  }

  @API(`/product-cod`, APIEnum.GET, Transactional.TRANSACTION, Auth.REQUIRED)
  public async listCustom(payload: Payload) {
    return new ProductItemService(payload).customByCod(payload.filter);
  }

  @API(`/carga-de-dados`, APIEnum.GET, Transactional.TRANSACTION, Auth.REQUIRED)
  public async cargaDeDados(payload: Payload) {
    return new ProductService(payload).cargaDeDados(payload.filter);
  }

  @API(`/atualiza-custo`, APIEnum.GET, Transactional.TRANSACTION, Auth.NO_AUTH)
  public async atualizaCusto(payload: Payload) {
    return new ProductService(payload).atualizaCusto(payload.filter);
  }

  @API(`/product/:id`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async get(payload: Payload) {
    return new ProductService(payload).get(payload.id);
  }

  @API(`/product`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async list(payload: Payload) {
    return new ProductService(payload).list(payload.filter);
  }
  @API(`/product-export/:id`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async exportList(payload: Payload) {
    let response: any;
    if(payload.id === 'group')
      response = await new ProductExportViewService(payload).list(payload.filter, true);
    else
     response = await new ProductExportService(payload).list(payload.filter, true);

    const date = moment().format("DD-MM-yyyy");
    const file = `Exportacao-${date}.csv`;
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: file,
      Body: response.contents
    };
    await s3.putObject(params).promise();
    return {url: await s3.getSignedUrl('getObject', {
      Bucket: process.env.AWS_BUCKET,
      Key: file,
      Expires: 300
    })};
  }

  @API(`/product-export-new/:id`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async exportCustomList(payload: Payload) {
    let response: any;
    if(payload.id === 'cod')
      response = await new ProductItemService(payload).exportCustListByCod(payload.filter);
    else
     response = await new ProductItemService(payload).exportCustomList(payload.filter);

    const date = moment().format("DD-MM-yyyy");
    const file = `Exportacao-${date}.csv`;
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: file,
      Body: response
    };
    await s3.putObject(params).promise();
    return {url: await s3.getSignedUrl('getObject', {
      Bucket: process.env.AWS_BUCKET,
      Key: file,
      Expires: 300
    })};
  }

  @API(`/product-categories`, APIEnum.GET, Transactional.READ_ONLY, Auth.NO_AUTH)
  public async listCategories(payload: Payload) {
    return new ProductService(payload).listCategories();
  }

  @API(`/product-marketplaces`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async listMarketplaces(payload: Payload) {
    return new ProductItemService(payload).listMarketplaces();
  }

  @API(`/sales-category`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async getCategory(payload: Payload) {
    return new ProductItemService(payload).getCategory();
  }

  @API(`/sales`, APIEnum.POST, Transactional.READ_ONLY, Auth.REQUIRED)
  public async getSalesHisotry(payload: Payload) {
    return new ProductItemService(payload).getSalesHisotry(payload.body);
  }

  @API(`/sales-images`, APIEnum.POST, Transactional.READ_ONLY, Auth.REQUIRED)
  public async getImage(payload: Payload) {
    return new ProductItemService(payload).getImage(payload.body);
  }

  @API(`/collection`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async getCollection(payload: Payload) {
    return new ProductItemService(payload).getCollection();
  }

  @API(`/product`, APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED)
  public async create(payload: Payload) {
    return new ProductService(payload).create(payload.body);
  }

  @API(`/product/:id`, APIEnum.PUT, Transactional.TRANSACTION, Auth.REQUIRED)
  public async update(payload: Payload) {
    return new ProductService(payload).update(payload.id, payload.body);
  }

  @API(`/product/:id`, APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED)
  public async delete(payload: Payload) {
    return new ProductService(payload).delete(payload.id);
  }

}
import { API, Payload } from '@libs/handler.decorators';

import { APIEnum } from '@enuns/api.enum';
import { Transactional } from '@enuns/transactional.enum';
import { CrudHandler } from '@libs/handler.crud';
import { BalanceamentoProductService } from '@services/balanceamento-product/balanceamento-product.service';
import { Auth } from "@enuns/auth.enum";
import { ProductService } from '@services/product/product.service';
import { BalanceamentoProduct } from '@models/balanceamento-products/balanceamento-product.model';

export class BalanceamentoProductHandler extends CrudHandler {

  @API(`/balanceamento-product/:id`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async get(payload: Payload) {
    return new BalanceamentoProductService(payload).get(payload.id);
  }

  @API(`/balanceamento-product`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async list(payload: Payload) {
    return new BalanceamentoProductService(payload).list(payload.filter);
  }

  @API(`/balanceamento-product`, APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED)
  public async create(payload: Payload) {
    let produtosParaCriar = await new ProductService(payload).getAllLike(payload.body.codigo);
    let balanceamentosCriados = [];

    produtosParaCriar.forEach(produto => {
      let objBalanceamento = {
        product_id: produto.id,
        percentual_molekada: payload.body.percentual_molekada,
        percentual_amalook: payload.body.percentual_amalook,
        percentual_mini_magia: payload.body.percentual_mini_magia
      } as BalanceamentoProduct;

      let balanceamentoCriado = new BalanceamentoProductService(payload).create(objBalanceamento);
      balanceamentosCriados.push(balanceamentoCriado);
    });

    return balanceamentosCriados;
  }

  @API(`/balanceamento-product/:id`, APIEnum.PUT, Transactional.TRANSACTION, Auth.REQUIRED)
  public async update(payload: Payload) {
    let produtosParaUpdate = await new ProductService(payload).getAllLike(payload.body.codigo);
    let produtosIds = [];
    produtosParaUpdate.forEach(produto => {
      produtosIds.push(produto.id);
    });
    return new BalanceamentoProductService(payload).updatePersonalizated(payload.id, payload.body, produtosIds);
  }

  @API(`/balanceamento-product/:id`, APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED)
  public async delete(payload: Payload) {
    return new BalanceamentoProductService(payload).delete(payload.id);
  }

  @API(`/get-all-balanceamento-by-loja/:id`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async getAllByLoja(payload: Payload) {
    return new BalanceamentoProductService(payload).getAllByLoja(payload.id);
  }

}
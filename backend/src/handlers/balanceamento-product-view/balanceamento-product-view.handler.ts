import { API, Payload } from '@libs/handler.decorators';

import { APIEnum } from '@enuns/api.enum';
import { Transactional } from '@enuns/transactional.enum';
import { CrudHandler } from '@libs/handler.crud';
import { BalanceamentoProductViewService } from '@services/balanceamento-product-view/balanceamento-product-view.service';
import { Auth } from "@enuns/auth.enum";

export class BalanceamentoProductViewHandler extends CrudHandler {

  @API(`/balanceamento-product-view`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async list(payload: Payload) {
    return new BalanceamentoProductViewService(payload).list(payload.filter);
  }

  @API(`/balanceamento-product-view/active`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async listActive(payload: Payload) {
    return new BalanceamentoProductViewService(payload).getAllActive();
  }

}
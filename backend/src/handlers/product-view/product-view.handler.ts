import { API, Payload } from '@libs/handler.decorators';

import { APIEnum } from '@enuns/api.enum';
import { Transactional } from '@enuns/transactional.enum';
import { CrudHandler } from '@libs/handler.crud';
import { ProductViewService } from '@services/product-view/product-view.service';
import { Auth } from "@enuns/auth.enum";

export class ProductViewHandler extends CrudHandler {

  @API(`/product-view`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED)
  public async list(payload: Payload) {
    return new ProductViewService(payload).list(payload.filter);
  }

}
const Firebird = require('node-firebird');
const POOL_SIZE = 10;

class FirebirdPool {
  options;
  connection;

  constructor() {
    this.options = {
      host: process.env.FIREBIRD_HOST,
      port: process.env.FIREBIRD_PORT,
      database: process.env.FIREBIRD_DATABASE,
      user: process.env.FIREBIRD_USERNAME,
      password: process.env.FIREBIRD_PASSWORD,
      lowercase_keys: false,
      WireCrypt: 'Enabled'
    };
    this.connection = Firebird.pool(POOL_SIZE, this.options);
  }

  static getInstance() {
    return new FirebirdPool();
  }

  killPool() {
    return this.connection.destroy();
  }

  _isASelectQuery = (sql) => {
    const upperCaseQuery = sql.toUpperCase().trim();
    const splittedQuery = upperCaseQuery.split(' ');

    return splittedQuery[0] === 'SELECT';
  };

  _decodeString(buffer) {
    return String.fromCharCode.apply(null, new Uint16Array(buffer));
  }

  async _execute(sql, params = []) {
    return await new Promise((resolutionFunc, rejectionFunc) => {
      this.connection.get((err, db) => {
        if (err) return rejectionFunc(err.message);

        db.execute(sql, params, (error, result) => {
          try {
            if (error) return rejectionFunc(error.message);
            return resolutionFunc(result);
          } finally {
            db.detach();
          }
        });
      });
    });
  }

  async _query(sql, params = []) {
    return await new Promise((resolutionFunc, rejectionFunc) => {
      this.connection.get(async (err, db) => {
        if (err) return rejectionFunc(err);

        db.query(sql, params, (error, result) => {
          try {
            if (error) return rejectionFunc(error);

            for (const element of result) {
              for (const [i, value] of Object.entries(element)) {
                if (Buffer.isBuffer(value) || typeof value == 'string')
                  element[i] = this._decodeString(value);
                if (!value && value !== 0) element[i] = null;
              }
            }

            return resolutionFunc(result);
          } finally {
            db.detach();
          }
        });
      });
    });
  }

  async executeQuery(sql, params = []) {
    console.log(sql);
    if (this._isASelectQuery(sql)) return this._query(sql, params);
    return this._execute(sql, params);
  }
}

module.exports = {
  getInstance: FirebirdPool.getInstance,
};

const axios = require('axios');

const putDataToFirebase = async (route, data) => {
  const url = `${process.env.FIREBASE_BASE_URL}${route}`;
  return axios.put(url, data);
};

const patchDataToFirebase = async (route, data) => {
  const url = `${process.env.FIREBASE_BASE_URL}${route}`;
  return axios.patch(url, data);
};

const getDataToFirebase = async (route) => {
  const url = `${process.env.FIREBASE_BASE_URL}${route}`;
  return axios.get(url);
};

module.exports = {
  putDataToFirebase: putDataToFirebase,
  getDataToFirebase: getDataToFirebase,
  patchDataToFirebase: patchDataToFirebase
};

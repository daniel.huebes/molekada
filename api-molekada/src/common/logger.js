// TODO: use winston

const { writeFileSync } = require('fs');
const { format } = require('date-fns');


const saveLog = (script) => {
  let log =
    'Executado ' +
    format(new Date(), 'dd-MM-yyyy HH:mm') +
    '   -  ' +
    script +
    ' -  ' +
    '. \r\n';
  writeFileSync(
    'C:\\DG-Sistemas\\log.txt',
    log,
    { flag: 'a+' },
    (writeFileError) => {
      if (writeFileError) {
        throw writeFileError;
      }
    }
  );
};

const saveErrorLog = (error, script) => {
  let log =
    'Executado ' +
    format(new Date(), 'dd-MM-yyyy HH:mm') +
    '   -  ' +
    error +
    ' / ' +
    script +
    ' -  ' +
    '. \r\n';
  writeFileSync(
    'C:\\DG-Sistemas\\logError.txt',
    log,
    { flag: 'a+' },
    (writeFileError) => {
      if (writeFileError) {
        throw writeFileError;
      }
    }
  );
};

const saveLogMySQL = async (mySQLClient, data) => {
  let sql = `
  INSERT INTO execucao_script (script, data, result) values ('${data.script}', now(), '${data.result}')
    `;
  return await mySQLClient.executeQuery(sql);
};

module.exports = { saveLog: saveLog, saveErrorLog: saveErrorLog, saveLogMySQL: saveLogMySQL };
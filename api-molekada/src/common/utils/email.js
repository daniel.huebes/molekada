﻿const axios = require('axios');
const moment = require('moment');

const sendEmail = async (data, error) => {
  if (!error) {
    let header = `<style>
    .page-header{
      -webkit-print-color-adjust: exact;
      color: rgb(0, 0, 0) !important;
      font-size:8px !important;
      width: 100vw;
      text-align: right;
      margin-right: 30px;
    }
    .page-header-center{
        -webkit-print-color-adjust: exact;
        color: rgb(0, 0, 0) !important;
        font-size:12px !important;
        width: 100vw;
        margin-left: 30vw;
      }
    .page-header-left {
      -webkit-print-color-adjust: exact;
      color:    !important;
      font-size:8px !important;
      text-align: left;
      margin-left: 30px;
    }
  </style>
  <span style='-webkit-print-color-adjust: exact;color: rgb(0, 0, 0) !important; width: 100vw' class='page-header-left'>
    MOLEKADA - RELATORIO ESTOQUE
  </span>
  <span style='-webkit-print-color-adjust: exact;color:rgb(0, 0, 0) !important;' class='page-header'>
    <b> ${moment().format('DD/MM/yyy HH:MM:ss')} </b>
  </span>`

    let foot = `<style>
        .page-footer{
            -webkit-print-color-adjust: exact;
            color:rgb(0, 0, 0) !important;
            font-size:8px !important;
            width: 100vw;
            text-align: center
        }
        </style>
        <span style='-webkit-print-color-adjust: exact;color:rgb(0, 0, 0) !important;' class='page-footer'>
        <b> Desenvolvido por DG Sistemas </b>
        </span>`
    let body = {
      // bucketName: 'cicampo-report',
      name: `Pedido-Entrada`,
      html: Buffer.from(data),
      footerHTML: Buffer.from(foot),
      headerHTML: Buffer.from(header),
      from: null,
      emailDest: null,
      subject: null,
      bodyText: null,
    }

    // let email = 'gerente@molekada.com.br;felipe.pj.dev@gmail.com;thiago@molekada.com.br;dev@dgsys.com.br';
    let email = 'felipe.pj.dev@gmail.com';

    body.from = 'no.reply.dgsys@gmail.com';
    body.emailDest = email;
    body.subject = `Relatorio estoque`;
    body.bodyText = `Em Anexo esta o PDF com os produtos em estoque.`;
    console.log(body);

    axios.post(`https://punnekmvqtxqiscaem3nbev5s40jmhqg.lambda-url.us-east-1.on.aws/?isMobile=true`, body, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).catch((err) => {
      console.log(err);
    });
  } else {
    let body = {
      // bucketName: 'cicampo-report',
      name: `Erros-Balanço`,
      html: Buffer.from(data),
      footerHTML: Buffer.from(''),
      headerHTML: Buffer.from(''),
      from: null,
      emailDest: null,
      subject: null,
      bodyText: null,
    }

    //let email = 'gerente@molekada.com.br;felipe.santos.ofic@gmail.com;dev@dgsys.com.br';
    let email = 'felipe.pj.dev@gmail.com;dev@dgsys.com.br';

    body.from = 'no.reply.dgsys@gmail.com';
    body.emailDest = email;
    body.subject = `Erros do Balanço`;
    body.bodyText = ``;
    console.log(body);

    axios.post(`https://punnekmvqtxqiscaem3nbev5s40jmhqg.lambda-url.us-east-1.on.aws/?isMobile=true`, body, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).catch((err) => {
      console.log(err);
    });
  }
};

module.exports = {
  sendEmail
};

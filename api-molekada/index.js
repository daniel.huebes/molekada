/** scripts */
const getList = require('./getList');
const balancing = require('./balancing');
// const migration = require('./migration');

const getListScript = async () => {
  await getList.script()
  .then(
      () => {
      },
      (error) => {
      }
  );
}

// const migrationScript = async () => {
//   await migration.script()
//   .then(
//       () => {
//       },
//       (error) => {
//       }
//   );
// }

const balancingScript = async () => {
 await balancing.script()
 .then(
     () => {
     },
     (error) => {
     }
  );
}

module.exports = {
  getListScript: getListScript,
  balancingScript: balancingScript
};

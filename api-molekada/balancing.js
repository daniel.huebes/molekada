﻿const axios = require("axios");
const { parseInt, get } = require("lodash");
const { sendEmail } = require("./src/common/utils/email");
let exportCount = 0;
let erros = "";

const balancing = async () => {
  var amalook = "amalook";
  var molekada = "molekada";
  var miniMagia = "minimagia";

  var perctAmalook = 0.5;
  var perctMolekada = 0.4;
  var perctMiniMagia = 0.1;

  var tokenLogin = await getTokenLogin();
  var lojas = await getBalanceamentoGlobal(tokenLogin);
  var lojasAtivas = [];

  var produtosBalanceamento = await getBalanceamentoProdutos(tokenLogin);
  let arrayMolekada = [];
  let arrayAmalook = [];
  let arrayMiniMagia = [];
  let arrayBalancedMolekada = [];
  let arrayBalancedAmalook = [];
  let arrayBalancedMiniMagia = [];

  /* Verifica lojas que devem entrar no balanceamento e pega a porcentagem */
  for (let loja of lojas) {
    if (loja.name == "Molekada") {
      perctMolekada = loja.percentual_balanceamento / 100;
      if (loja.percentual_balanceamento && loja.percentual_balanceamento > 0) {
        arrayMolekada = await getEstoqueAtual(molekada);
        lojasAtivas.push('molekada');
      }

    } else if (loja.name == "Mini Magia") {
      perctMiniMagia = loja.percentual_balanceamento / 100;
      if (loja.percentual_balanceamento && loja.percentual_balanceamento > 0) {
        arrayMiniMagia = await getEstoqueAtual(miniMagia);
        lojasAtivas.push('minimagia');
      }

    } else {
      perctAmalook = loja.percentual_balanceamento / 100;
      if (loja.percentual_balanceamento && loja.percentual_balanceamento > 0) {
        arrayAmalook = await getEstoqueAtual(amalook);
        lojasAtivas.push('amalook');
      }
    }
  }

  arrayMolekada.forEach((prdLjOrigem) => {
    let resp = undefined;
    let respLoja3 = undefined;

    arrayAmalook.forEach((prdLjDestino) => {
      if (prdLjDestino.produto === prdLjOrigem.produto) {
        resp = { ...prdLjDestino };
      }
    });

    arrayMiniMagia.forEach((prdLjDestino) => {
      if (prdLjDestino.produto === prdLjOrigem.produto) {
        respLoja3 = { ...prdLjDestino };
      }
    });

    /* Atribui percentuais personalizados do produto  (apenas das lojas habilitadas)*/
    produtosBalanceamento.map((produto) => {
      if (produto.codigo == prdLjOrigem.produto) {
        if ((produto.percentual_amalook + produto.percentual_molekada + produto.percentual_mini_magia) == 100) {
          perctAmalook = lojasAtivas.includes(amalook) ? (produto.percentual_amalook / 100) : 0;
          perctMolekada = lojasAtivas.includes(molekada) ? (produto.percentual_molekada / 100) : 0;
          perctMiniMagia = lojasAtivas.includes(miniMagia) ? (produto.percentual_mini_magia / 100) : 0;
        }
      }
    });

    // console.log('data1', prdLjOrigem.produto);

    if (!resp && !lojasAtivas.includes(amalook)) {
      resp = JSON.parse(JSON.stringify(prdLjOrigem));
      resp.quantidadeDisponivelVenda = 0;
    }

    if (!respLoja3 && !lojasAtivas.includes(miniMagia)) {
      respLoja3 = JSON.parse(JSON.stringify(prdLjOrigem));
      respLoja3.quantidadeDisponivelVenda = 0;
    }

    /* Calcula quantidade total disponível para venda */
    if (respLoja3 && resp && prdLjOrigem) {

      let qtdTotal = resp.quantidadeDisponivelVenda + prdLjOrigem.quantidadeDisponivelVenda + respLoja3.quantidadeDisponivelVenda;
      let percAtualDestino = (resp?.quantidadeDisponivelVenda * 100) / qtdTotal;
      let percAtualOrigem = (prdLjOrigem?.quantidadeDisponivelVenda * 100) / qtdTotal;

      //console.log(`${resp.quantidadeDisponivelVenda} - ${prdLjOrigem.quantidadeDisponivelVenda} - ${qtdTotal} - ${percAtualDestino} - ${percAtualOrigem}`);
      // if (resp.produto == '11100-MAR-2') {
      //   console.log('data1', prdLjOrigem);
      // }
      if (
        qtdTotal > 1 && ((qtdTotal > 10 && (percAtualOrigem > 67 || percAtualOrigem < 55)) ||
          (qtdTotal < 5 && (percAtualOrigem > 75 || percAtualOrigem < 40)) ||
          (qtdTotal < 11 && qtdTotal > 4 && (percAtualOrigem > 72 || percAtualOrigem < 40)))
      ) {
        //if (Math.abs(resp.quantidadeDisponivelVenda - prdLjOrigem.quantidadeDisponivelVenda) > 1) {
        if (parseInt(resp.quantidadeDisponivelVenda) < 0) {
          resp.quantidadeDisponivelVenda = 0;
        }
        if (parseInt(respLoja3.quantidadeDisponivelVenda) < 0) {
          respLoja3.quantidadeDisponivelVenda = 0;
        }
        if (parseInt(prdLjOrigem.quantidadeDisponivelVenda) < 0) {
          prdLjOrigem.quantidadeDisponivelVenda = 0;
        }
        if (
          resp.quantidadeDisponivelVenda > 0 ||
          prdLjOrigem.quantidadeDisponivelVenda > 0 || respLoja3.quantidadeDisponivelVenda > 0
        ) {
          //console.log('resp.produto: ' + resp.produto + `(${resp.quantidadeDisponivelVenda}, ${prdLjOrigem.quantidadeDisponivelVenda})`);
          resp.quantidadeDisponivelVendaOrig = resp.quantidadeDisponivelVenda;
          prdLjOrigem.quantidadeDisponivelVendaOrig = prdLjOrigem.quantidadeDisponivelVenda;
          respLoja3.quantidadeDisponivelVendaOrig = respLoja3.quantidadeDisponivelVenda;

          /*
          let newQtd = qtdTotal / 2;
          if (parseInt(newQtd) != parseFloat(newQtd)) {
            prdLjOrigem.quantidadeDisponivelVenda = (newQtd + .5);
            resp.quantidadeDisponivelVenda = (newQtd - .5);
          } else {
            prdLjOrigem.quantidadeDisponivelVenda = newQtd;
            resp.quantidadeDisponivelVenda = newQtd;
          }
          */

          let percAtualDestino = (resp.quantidadeDisponivelVenda * 100) / qtdTotal;
          let percAtualOrigem = (prdLjOrigem.quantidadeDisponivelVenda * 100) / qtdTotal;
          //        console.log('resp.produto: ' + resp.produto + `. Total ${qtdTotal} (${resp.quantidadeDisponivelVenda}, ${prdLjOrigem.quantidadeDisponivelVenda}) - ${percAtualOrigem}`);

          /* Faz a distribuição do produto conforme a porcentagem */
          resp.quantidadeDisponivelVenda = Math.floor(qtdTotal * perctAmalook);
          respLoja3.quantidadeDisponivelVenda = Math.floor(qtdTotal * perctMiniMagia);
          prdLjOrigem.quantidadeDisponivelVenda = Math.ceil(qtdTotal * perctMolekada);

          percAtualDestino = (resp.quantidadeDisponivelVenda * 100) / qtdTotal;
          percAtualOrigem = (prdLjOrigem.quantidadeDisponivelVenda * 100) / qtdTotal;
          // console.log("resp.produto: " +resp.produto +`. Total ${qtdTotal} (${resp.quantidadeDisponivelVenda}, ${prdLjOrigem.quantidadeDisponivelVenda}, ${respLoja3.quantidadeDisponivelVenda}) - ${percAtualOrigem}`);
          // console.log('---------------------------------');
          // console.log('ok');
          // console.log('data1', prdLjOrigem);
          // console.log('data2', resp);
          // console.log('data3', respLoja3);
          // console.log('---------------------------------');

          /* Adiciona ao array para salvar dados */
          if (lojasAtivas.includes(amalook))
            arrayBalancedAmalook.push({ ...resp });
          if (lojasAtivas.includes(molekada))
            arrayBalancedMolekada.push({ ...prdLjOrigem });
          if (lojasAtivas.includes(miniMagia))
            arrayBalancedMiniMagia.push({ ...respLoja3 });
        }
      }
    } else {
      // console.log('---------------------------------');
      // console.log('SEM BALANÇO');
      console.log('cod: ', prdLjOrigem.produto);
      // console.log('data2', resp);
      // console.log('data3', respLoja3);
      // console.log('---------------------------------');
    }

  });
  /*
  arrayBalancedLjDestino = arrayBalancedLjDestino.filter((item) => item.produto.includes('MPJ000'));
  arrayBalancedLjOrigem = arrayBalancedLjOrigem.filter((item) => item.produto.includes('MPJ000'));
  */
  console.log(new Date());
  //arrayBalancedLjOrigem = [];

  for (let i = 0; i < arrayBalancedMolekada.length; i++) {
    const prdOrigem = arrayBalancedMolekada[i];
    let erro = false;
    let salvo = false;

    if (!prdOrigem.produto.includes("KIT")) {
      try {
        //SALVA AMALOOK
        const prdAmalook = arrayBalancedAmalook[i];
        if (prdAmalook) {
          var objPost = {
            produto: prdAmalook.produto,
            deposito: prdAmalook.deposito,
            quantidade: Math.abs(
              prdAmalook.quantidadeDisponivelVenda -
              prdAmalook.quantidadeDisponivelVendaOrig
            ),
            tipo: 1,
            tipoOperacao: getTipoOpe(
              prdAmalook.quantidadeDisponivelVenda -
              prdAmalook.quantidadeDisponivelVendaOrig
            ),
            origemMovimento: getOriMov(
              prdAmalook.quantidadeDisponivelVenda -
              prdAmalook.quantidadeDisponivelVendaOrig
            ),
            observacao: "Balanço Automático Loja 2.",
          };

          // console.log(objPost);
          try {
            await sleep(2200);
            await salvarLoja(amalook, objPost, 1);
            salvo = true;
          } catch (error) {
            erro = true;
          }
          // await salvarLoja(amalook, objPost, 1).catch((err) => {
          //   erro = true; 
          // });
          // console.log("SALVOU AMALOOK");
        }

        if (!erro) {
          //SALVA MOLEKADA
          let prdMolekada = arrayBalancedMolekada[i];
          if (prdMolekada) {
            let objPost2 = {
              produto: prdMolekada.produto,
              deposito: prdMolekada.deposito,
              quantidade: Math.abs(
                prdMolekada.quantidadeDisponivelVenda -
                prdMolekada.quantidadeDisponivelVendaOrig
              ),
              tipo: 1,
              tipoOperacao: getTipoOpe(
                prdMolekada.quantidadeDisponivelVenda -
                prdMolekada.quantidadeDisponivelVendaOrig
              ),
              origemMovimento: getOriMov(
                prdMolekada.quantidadeDisponivelVenda -
                prdMolekada.quantidadeDisponivelVendaOrig
              ),
              observacao: "Balanço Automático Loja 1.",
            };

            try {
              // console.log(objPost2);
              if (!erro) {
                await sleep(2200);
                await salvarLoja(molekada, objPost2, 1);
                salvo = true;
              }
              // console.log("SALVOU MOLEKADA");
            } catch (err) {
              sleep(120000);
              if (salvo) {
                erros +=
                  prdOrigem.produto +
                  ` - Erro salvar estoque loja 1 - Tentativa 1<br>`;
                erros += JSON.stringify(objPost2) + `<br>`;
                erros += err + "<br><br><br>";
                console.log(
                  prdOrigem.produto +
                  ` - Erro salvar de estoque loja 1 - ${err.response?.data}<br><br>`
                );
                erro = true;
                console.log(err);
              }
            }

          }
          //SALVA MINI MAGIA 
          let ObjMiniMagia = arrayBalancedMiniMagia[i];
          if (ObjMiniMagia) {
            let objMiniMagia = {
              produto: ObjMiniMagia.produto,
              deposito: ObjMiniMagia.deposito,
              quantidade: Math.abs(ObjMiniMagia.quantidadeDisponivelVenda - ObjMiniMagia.quantidadeDisponivelVendaOrig),
              tipo: 1,
              tipoOperacao: getTipoOpe(ObjMiniMagia.quantidadeDisponivelVenda - ObjMiniMagia.quantidadeDisponivelVendaOrig),
              origemMovimento: getOriMov(ObjMiniMagia.quantidadeDisponivelVenda - ObjMiniMagia.quantidadeDisponivelVendaOrig),
              observacao: "Balanço Automático Loja 3.",
            };

            try {
              // console.log(objMiniMagia);
              if (arrayBalancedMiniMagia.length > 0 && !erro) {
                await sleep(2200);
                await salvarLoja(miniMagia, objMiniMagia, 1);
                // console.log('SALVOU MINI MAGIA');
              }
            } catch (err) {
              sleep(120000);
              if (salvo) {
                erros += miniMagia.produto + ` - Erro salvar estoque loja 3 - Tentativa 1<br>`;
                erros += JSON.stringify(objPost2) + `<br>`;
                erros += err + "<br><br><br>";
                console.log(miniMagia.produto + ` - Erro salvar de estoque loja 3 - ${err.response?.data}<br><br>`);

              }
            }
          }

          //SALVA AMALOOK DE NOVO 
          if (prdAmalook) {
            let objPost3 = {
              produto: prdAmalook.produto,
              deposito: prdAmalook.deposito,
              quantidade: Math.abs(
                prdAmalook.quantidadeDisponivelVendaOrig -
                prdAmalook.quantidadeDisponivelVenda
              ),
              tipo: 1,
              tipoOperacao: getTipoOpe(
                prdAmalook.quantidadeDisponivelVendaOrig -
                prdAmalook.quantidadeDisponivelVenda
              ),
              origemMovimento: getOriMov(
                prdAmalook.quantidadeDisponivelVendaOrig -
                prdAmalook.quantidadeDisponivelVenda
              ),
              observacao: "Balanço Automático Loja 2",
            };

            try {
              console.log(objPost3);
              if (!erro) {
                await sleep(2200);
                await salvarLoja(amalook, objPost3, 2);
              }
              console.log("SALVOU AMALOOK");
            } catch (err) {
              sleep(120000);
              erros +=
                prdOrigem.produto +
                ` - Erro salvar estoque loja 2 - Tentativa 2<br>`;
              erros += JSON.stringify(objPost3) + `<br>`;
              erros += err + "<br><br><br>";

              console.log(
                prdOrigem.produto +
                ` - Erro volta de estoque loja 2 - ${err.response?.data?.mensagem}<br><br>`
              );
              console.log(err);
            }
          }
        }

      } catch (err) {
        sleep(120000);
        console.log(
          prdOrigem.produto +
          ` - Erro salvar estoque loja 2 - ${err.response?.data?.mensagem}<br><br>`
        );
        if (salvo) {
          console.log(err);
          erros +=
            prdOrigem.produto + ` - Erro salvar estoque loja 2 - Tentativa 1<br>`;
          erros += JSON.stringify(objPost) + `<br>`;
          erros += err + "<br><br><br>";
        }
      }
    }
  }
  console.log(new Date());
  if (erros !== "") {
    sendEmail(erros, true);
  }
};

async function salvarLoja(nomeLoja, data, tentativa) {
  // console.log(
  //   `${nomeLoja} - SALVANDO ${data.produto} - QTD ${data.quantidade} -  Tentativa ${tentativa}`
  // );

  let token = "";
  if (nomeLoja == "amalook") {
    token = "YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=";
  } else if (nomeLoja == 'molekada') {
    token = "YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=";
  } else {
    token = "OTRjZjczMmU1Yjc3Mzk2NjM5Yjk0NzIzNDBmZjQ4ZTA1MDZkZmY3OTJhMTIwNjdhNjE4ZmQ3MjMxOTViMjQ0YzpvNjJGRnIlaSFocDU=";
  }
  let count = 0;
  // if (data.produto.includes('MPJ0104-MAR')) {
  let balanceArr = [];
  if (data.produto.includes('KIT')) {
    // await axios.post(
    //   `https://${nomeLoja}.painel.magazord.com.br/api/v1/estoque`,
    //   data,
    //   {
    //     headers: {
    //       "Content-Type": "application/json",
    //       Authorization: `Basic ${token}`,
    //     },
    //   }
    // ); 
  } else if (true) {
    // console.log(data);
    balanceArr.push(data.produto);
    exportCount++;
    if (!data.produto.includes('KIT')) {
      // console.log(data);
      if (data.quantidade > 0) {
        await axios.post(
          `https://${nomeLoja}.painel.magazord.com.br/api/v1/estoque`,
          data,
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Basic ${token}`,
            },
          }
        );
      }
    }
  }
  console.log(balanceArr);
}

async function getTokenLogin() {
  let resultLogin = await axios.post(
    "http://y7m0yeljsg.execute-api.sa-east-1.amazonaws.com/dev/authentication-api/login",
    {
      email: "marcelojoras@hotmail.com",
      password: "DgSys06042023",
      returnSecureToken: true,
    }
  );

  return resultLogin.data.authorization;
}

async function getBalanceamentoGlobal(token) {
  let result = await axios.get(
    "http://y7m0yeljsg.execute-api.sa-east-1.amazonaws.com/dev/balanceamento-loja-api/balanceamento-loja",
    {
      headers: {
        "x-api-key": token,
      },
    }
  );
  console.log('balance',
    result.data.contents
  );

  return result.data.contents;
}

async function getBalanceamentoProdutos(token) {
  let results = await axios.get(
    "http://y7m0yeljsg.execute-api.sa-east-1.amazonaws.com/dev/balanceamento-product-view-api/balanceamento-product-view/active",
    {
      headers: {
        "x-api-key": token,
      },
    }
  );

  return results.data;
}

async function getEstoqueAtual(nomeLoja) {
  let token = "";
  if (nomeLoja == "amalook") {
    token =
      "YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=";
  } else if (nomeLoja == 'minimagia') {
    token = 'OTRjZjczMmU1Yjc3Mzk2NjM5Yjk0NzIzNDBmZjQ4ZTA1MDZkZmY3OTJhMTIwNjdhNjE4ZmQ3MjMxOTViMjQ0YzpvNjJGRnIlaSFocDU=';
  } else {
    token =
      "YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=";
  }

  let arrayResultLj = [];
  let filterLj = {
    offset: 0,
    limit: 100,
    allResults: false,
  };
  while (!filterLj.allResults) {
    let result = await axios.get(
      `https://${nomeLoja}.painel.magazord.com.br/api/v1/listEstoque?offset=${filterLj.offset}&limit=${filterLj.limit}&ativo=true`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Basic ${token}`,
        },
      }
    );
    if (result.data.data.length === 0) {
      filterLj.allResults = true;
    } else if (result.data.data.length > 0) {
      filterLj.offset = filterLj.offset + filterLj.limit;
    }
    arrayResultLj = arrayResultLj.concat(result.data.data);
  }

  return arrayResultLj;
}

function getTipoOpe(value) {
  return value < 0 ? 2 : 1;
}
function getOriMov(value) {
  return value < 0 ? 8 : 1;
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

balancing();

module.exports = {
  script: balancing,
};

﻿const axios = require('axios');
const { parseInt, get } = require('lodash');
const { sendEmail } = require('./src/common/utils/email');

let erros = '';
const balancing = async () => {
  var lojaOrigem = 'amalook';
  var lojaDestino = 'molekada';

  var percQtdOrigem = 0.7;
  var percQtdDestino = 0.3;

  let arrayResultLjDestino = await getEstoqueAtual(lojaDestino);
  let arrayResultLjOrigem = await getEstoqueAtual(lojaOrigem);
  let arrayBalancedLjDestino = [];
  let arrayBalancedLjOrigem = [];

  arrayResultLjOrigem.forEach((prdLjOrigem) => {
    let resp = undefined;
    arrayResultLjDestino.forEach((prdLjDestino) => {
      if (prdLjDestino.produto === prdLjOrigem.produto) {
        resp = prdLjDestino;
      }
    });

    if (!resp) {
      resp = JSON.parse(JSON.stringify(prdLjOrigem));
      resp.quantidadeDisponivelVenda = 0;
    }


    let qtdTotal = resp.quantidadeDisponivelVenda + prdLjOrigem.quantidadeDisponivelVenda;
    let percAtualDestino = (resp.quantidadeDisponivelVenda * 100) / qtdTotal;
    let percAtualOrigem = (prdLjOrigem.quantidadeDisponivelVenda * 100) / qtdTotal;

    //console.log(`${resp.quantidadeDisponivelVenda} - ${prdLjOrigem.quantidadeDisponivelVenda} - ${qtdTotal} - ${percAtualDestino} - ${percAtualOrigem}`);
    
    if (qtdTotal > 1 &&
        ((qtdTotal > 10 && (percAtualOrigem > 67 || percAtualOrigem < 55)) ||
        (qtdTotal < 5  && (percAtualOrigem > 75 || percAtualOrigem < 40)) ||
        (qtdTotal < 11 && qtdTotal > 4 && (percAtualOrigem > 72 || percAtualOrigem < 40)))) {
    //if (Math.abs(resp.quantidadeDisponivelVenda - prdLjOrigem.quantidadeDisponivelVenda) > 1) {
      if (parseInt(resp.quantidadeDisponivelVenda) < 0) {
        resp.quantidadeDisponivelVenda = 0;
      }
      if (parseInt(prdLjOrigem.quantidadeDisponivelVenda) < 0) {
        prdLjOrigem.quantidadeDisponivelVenda = 0;
      }
      if (resp.quantidadeDisponivelVenda > 0 || prdLjOrigem.quantidadeDisponivelVenda > 0) {
        //console.log('resp.produto: ' + resp.produto + `(${resp.quantidadeDisponivelVenda}, ${prdLjOrigem.quantidadeDisponivelVenda})`);
        resp.quantidadeDisponivelVendaOrig = resp.quantidadeDisponivelVenda;
        prdLjOrigem.quantidadeDisponivelVendaOrig = prdLjOrigem.quantidadeDisponivelVenda;

        /*
        let newQtd = qtdTotal / 2;
        if (parseInt(newQtd) != parseFloat(newQtd)) {
          prdLjOrigem.quantidadeDisponivelVenda = (newQtd + .5);
          resp.quantidadeDisponivelVenda = (newQtd - .5);
        } else {
          prdLjOrigem.quantidadeDisponivelVenda = newQtd;
          resp.quantidadeDisponivelVenda = newQtd;
        }
        */


        let percAtualDestino = (resp.quantidadeDisponivelVenda * 100) / qtdTotal;
        let percAtualOrigem = (prdLjOrigem.quantidadeDisponivelVenda * 100) / qtdTotal;
//        console.log('resp.produto: ' + resp.produto + `. Total ${qtdTotal} (${resp.quantidadeDisponivelVenda}, ${prdLjOrigem.quantidadeDisponivelVenda}) - ${percAtualOrigem}`);
        
        prdLjOrigem.quantidadeDisponivelVenda = Math.ceil(qtdTotal * percQtdOrigem);//(newQtd + .5);
        resp.quantidadeDisponivelVenda = Math.floor(qtdTotal * percQtdDestino); //(newQtd - .5);

         percAtualDestino = (resp.quantidadeDisponivelVenda * 100) / qtdTotal;
         percAtualOrigem = (prdLjOrigem.quantidadeDisponivelVenda * 100) / qtdTotal;
        console.log('resp.produto: ' + resp.produto + `. Total ${qtdTotal} (${resp.quantidadeDisponivelVenda}, ${prdLjOrigem.quantidadeDisponivelVenda}) - ${percAtualOrigem}`);

        arrayBalancedLjOrigem.push(prdLjOrigem);
        arrayBalancedLjDestino.push(resp);
      }
    }
  });

  /*
  arrayBalancedLjDestino = arrayBalancedLjDestino.filter((item) => item.produto.includes('MPJ000'));
  arrayBalancedLjOrigem = arrayBalancedLjOrigem.filter((item) => item.produto.includes('MPJ000'));
  */

  console.log(new Date());
  //arrayBalancedLjOrigem = [];
  for (let i = 0; i < arrayBalancedLjOrigem.length; i++) {
    const prdOrigem = arrayBalancedLjOrigem[i];

    if (!prdOrigem.produto.includes("KIT")) {
      try {
        var objPost = {
          produto: prdOrigem.produto,
          deposito: prdOrigem.deposito,
          quantidade: Math.abs(prdOrigem.quantidadeDisponivelVenda - prdOrigem.quantidadeDisponivelVendaOrig),
          tipo: 1,
          tipoOperacao: getTipoOpe(prdOrigem.quantidadeDisponivelVenda - prdOrigem.quantidadeDisponivelVendaOrig),
          origemMovimento: getOriMov(prdOrigem.quantidadeDisponivelVenda - prdOrigem.quantidadeDisponivelVendaOrig),
          observacao: 'Balanço Automático Loja 2.',
        }

        console.log(objPost);
        await salvarLoja(lojaOrigem, objPost, 1);
        console.log('SALVOU AMALOOK');
        let prdDestino = arrayBalancedLjDestino[i];

        let objPost2 = {
          produto: prdDestino.produto,
          deposito: prdDestino.deposito,
          quantidade: Math.abs(prdDestino.quantidadeDisponivelVenda - prdDestino.quantidadeDisponivelVendaOrig),
          tipo: 1,
          tipoOperacao: getTipoOpe(prdDestino.quantidadeDisponivelVenda - prdDestino.quantidadeDisponivelVendaOrig),
          origemMovimento: getOriMov(prdDestino.quantidadeDisponivelVenda - prdDestino.quantidadeDisponivelVendaOrig),
          observacao: 'Balanço Automático Loja 1.',
        }

        try {
          console.log(objPost2);
          await salvarLoja(lojaDestino, objPost2, 1);
          console.log('SALVOU MOLEKADA');
        } catch (err) {
          sleep(120000);
          erros += prdDestino.produto + ` - Erro salvar estoque loja 1 - Tentativa 1<br>`;
          erros += JSON.stringify(objPost2) + `<br>`;
          erros += err + '<br><br><br>';
          console.log(prdOrigem.produto + ` - Erro salvar de estoque loja 1 - ${err.response?.data}<br><br>`);
          console.log(err);

          let objPost3 = {
            produto: prdOrigem.produto,
            deposito: prdOrigem.deposito,
            quantidade: Math.abs(prdOrigem.quantidadeDisponivelVendaOrig - prdOrigem.quantidadeDisponivelVenda),
            tipo: 1,
            tipoOperacao: getTipoOpe(prdOrigem.quantidadeDisponivelVendaOrig - prdOrigem.quantidadeDisponivelVenda),
            origemMovimento: getOriMov(prdOrigem.quantidadeDisponivelVendaOrig - prdOrigem.quantidadeDisponivelVenda),
            observacao: 'Balanço Automático Loja 2',
          }
          try {
            console.log(objPost3);
            await salvarLoja(lojaOrigem, objPost3, 2);
            console.log('SALVOU AMALOOK');
          } catch (err) {
            sleep(120000);
            erros += prdOrigem.produto + ` - Erro salvar estoque loja 2 - Tentativa 2<br>`;
            erros += JSON.stringify(objPost3) + `<br>`;
            erros += err + '<br><br><br>';

            console.log(prdOrigem.produto + ` - Erro volta de estoque loja 2 - ${err.response?.data?.mensagem}<br><br>`);
            console.log(err);
          }
        }
      } catch (err) {
        sleep(120000);
        console.log(prdOrigem.produto + ` - Erro salvar estoque loja 2 - ${err.response?.data?.mensagem}<br><br>`);
        console.log(err);
        erros += prdOrigem.produto + ` - Erro salvar estoque loja 2 - Tentativa 1<br>`;
        erros += JSON.stringify(objPost) + `<br>`;
        erros += err + '<br><br><br>';
      }

    }

  }
  console.log(new Date());
  if (erros !== '') {
    sendEmail(erros, true);
  }
}

async function salvarLoja(nomeLoja, data, tentativa) {
  console.log(`${nomeLoja} - SALVANDO ${data.produto} - QTD ${data.quantidade} -  Tentativa ${tentativa}`);

  let token = '';
  if (nomeLoja == 'amalook') {
    token = 'YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=';
  } else {
    token = 'YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=';
  }
  
  await axios.post(`https://${nomeLoja}.painel.magazord.com.br/api/v1/estoque`, data, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Basic ${token}`
    }
  });
}

async function getEstoqueAtual(nomeLoja) {
  let token = '';
  if (nomeLoja == 'amalook') {
    token = 'YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=';
  } else {
    token = 'YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=';
  }
  
  let arrayResultLj = [];
  let filterLj = {
    offset: 0,
    limit: 100,
    allResults: false
  }
  while (!filterLj.allResults) {
    let result = await axios.get(`https://${nomeLoja}.painel.magazord.com.br/api/v1/listEstoque?offset=${filterLj.offset}&limit=${filterLj.limit}&ativo=true`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${token}`
      }
    });
    if (result.data.data.length === 0) {
      filterLj.allResults = true;
    } else if (result.data.data.length > 0) {
      filterLj.offset = filterLj.offset + filterLj.limit;
    }
    arrayResultLj = arrayResultLj.concat(result.data.data);
  }

  return arrayResultLj;
}

function getTipoOpe(value) {
  return value < 0 ? 2 : 1
}
function getOriMov(value) {
  return value < 0 ? 8 : 1
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

balancing();

module.exports = {
   script: balancing,
};
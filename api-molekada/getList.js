const moment = require('moment');
const axios = require('axios');
const { sendEmail } = require('./src/common/utils/email');
/** dotenv loading */
require('dotenv').config();

/** common */
const FirebirdPool = require('./src/common/database/firebirdPool');
const { isAfter } = require('date-fns');
const firebirdClient = FirebirdPool.getInstance();

const getList = async () => {
  let filter = {
    offset: 0,
    limit: 100,
    allResults: false
  }
  let filter2 = {
    offset: 0,
    limit: 100,
    allResults: false
  }
  let codigosInsert = {};
  let codigos = [];
  let codigosInsertProd = {};
  let codigosProd = [];
  let arrayResult = [];
  let colecoes = [];
  let produtosCol = [];
  let coresSisplan = [];
  let arrayResult2 = [];
  let finOp;
  let objIdx = {};
  let objIdxAux = {};
  let arrFinal = [];
  let listProd = [];

  try {
    sql = `SELECT DESCRICAO, CODIGO, DT_INICIO FROM COLECAO_001`;
    colecoes = await firebirdClient.executeQuery(sql);
  } catch (error) {
    console.log(error);
  }

  while (!filter.allResults) {
    let result = await axios.get(`https://molekada.painel.magazord.com.br/api/v1/listEstoque?offset=${filter.offset}&limit=${filter.limit}&ativo=true`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk='
      }
    });
    if (result.data.data.length === 0) {
      filter.allResults = true;
    } else if (result.data.data.length > 0) {
      filter.offset = filter.offset + filter.limit;
    }
    arrayResult = arrayResult.concat(result.data.data);
  }
  
  while (!filter2.allResults) {
    let result = await axios.get(`https://minimagia.painel.magazord.com.br/api/v1/listEstoque?offset=${filter2.offset}&limit=${filter2.limit}&ativo=true`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic OTRjZjczMmU1Yjc3Mzk2NjM5Yjk0NzIzNDBmZjQ4ZTA1MDZkZmY3OTJhMTIwNjdhNjE4ZmQ3MjMxOTViMjQ0YzpvNjJGRnIlaSFocDU='
      }
    });
    if (result.data.data.length === 0) {
      filter2.allResults = true;
    } else if (result.data.data.length > 0) {
      filter2.offset = filter2.offset + filter2.limit;
    }
    arrayResult2 = arrayResult2.concat(result.data.data);
  }

  // arrayResult.forEach((item) => {
  //   if (!item.produto.includes("KIT")) {
  //     let cod = item.produto.split('-');
  //     if (!codigosInsert[cod['0']]) {
  //       codigos.push(("'" + cod['0'] + "'"));
  //       codigosInsert[cod['0']] = 1;
  //     }
  //     if (!codigosInsertProd[cod['0']+'-'+cod['1']]) {
  //       codigosProd.push(("'" + cod['0']+'-'+cod['1'] + "'"));
  //       codigosInsertProd[cod['0']+'-'+cod['1']] = 1;
  //     }
  //   }
  // });

  // arrayResult2.forEach((item) => {
  //   if (!item.produto.includes("KIT")) {
  //     let cod = item.produto.split('-');
  //     if (!codigosInsert[cod['0']]) {
  //       codigos.push(("'" + cod['0'] + "'"));
  //       codigosInsert[cod['0']] = 1;
  //     }
  //     if (!codigosInsertProd[cod['0']+'-'+cod['1']]) {
  //       codigosProd.push(("'" + cod['0']+'-'+cod['1'] + "'"));
  //       codigosInsertProd[cod['0']+'-'+cod['1']] = 1;
  //     }
  //   }
  // });

  let auxArray2 = [];
  let auxArray3 = [];
  let count2 = 1;
  let idx2 = 0;
  let idx3 = 0;

  arrayResult.forEach((item) => {
    if (!item.produto.includes("KIT")) {
      let cod = item.produto.split('-');

      if (!codigosInsert[cod['0']]) {
        if (count2 > 500) {
          auxArray2[idx2] ? auxArray2[idx2].push("'" + cod['0'] + "'") : auxArray2[idx2] = ["'" + cod['0'] + "'"];
          count2 = 0;
          idx2++;
        } else {
          auxArray2[idx2] ? auxArray2[idx2].push("'" + cod['0'] + "'") : auxArray2[idx2] = ["'" + cod['0'] + "'"];
          count2++;
        }
        codigosInsert[cod['0']] = 1;
      }

      if (!codigosInsertProd[cod['0']+'-'+cod['1']]) {
        if (count2 > 500) {
          auxArray3[idx3] ? auxArray3[idx3].push("'" + cod['0']+'-'+cod['1'] + "'"
          ) : auxArray3[idx3] = ["'" + cod['0']+'-'+cod['1'] + "'"
        ];
          count2 = 0;
          idx3++;
        } else {
          auxArray3[idx3] ? auxArray3[idx3].push("'" + cod['0']+'-'+cod['1'] + "'"
          ) : auxArray3[idx3] = ["'" + cod['0']+'-'+cod['1'] + "'"
        ];
          count2++;
        }
        codigosInsertProd[cod['0']+'-'+cod['1']] = 1;
      }
    }
  });

  arrayResult2.forEach((item) => {
    if (!item.produto.includes("KIT")) {
      let cod = item.produto.split('-');

      if (!codigosInsert[cod['0']]) {
        if (count2 > 500) {
          auxArray2[idx2] ? auxArray2[idx2].push("'" + cod['0'] + "'") : auxArray2[idx2] = ["'" + cod['0'] + "'"];
          count2 = 0;
          idx2++;
        } else {
          auxArray2[idx2] ? auxArray2[idx2].push("'" + cod['0'] + "'") : auxArray2[idx2] = ["'" + cod['0'] + "'"];
          count2++;
        }
        codigosInsert[cod['0']] = 1;
      }

      if (!codigosInsertProd[cod['0']+'-'+cod['1']]) {
        if (count2 > 500) {
          auxArray3[idx3] ? auxArray3[idx3].push("'" + cod['0']+'-'+cod['1'] + "'"
          ) : auxArray3[idx3] = ["'" + cod['0']+'-'+cod['1'] + "'"
        ];
          count2 = 0;
          idx3++;
        } else {
          auxArray3[idx3] ? auxArray3[idx3].push("'" + cod['0']+'-'+cod['1'] + "'"
          ) : auxArray3[idx3] = ["'" + cod['0']+'-'+cod['1'] + "'"
        ];
          count2++;
        }
        codigosInsertProd[cod['0']+'-'+cod['1']] = 1;
      }
    }
  });

  //console.log(auxArray);
  let finOp3 = [];
  for (let i = 0; i < auxArray2.length; i++) {
    let rp = auxArray2[i].map(item => `${item}`);
    finOp3 ? finOp3 : finOp3 = [];
    finOp3[i] = rp.join(',');
  }

  for (let p = 0; p < finOp3.length; p++) {
    codigos.push(` CODIGO IN (${finOp3[p]}) `);
  }

  let finOp2 = [];
  for (let i = 0; i < auxArray3.length; i++) {
    let rp = auxArray3[i].map(item => `${item}`);
    finOp2 ? finOp2 : finOp2 = [];
    finOp2[i] = rp.join(',');
  }

  for (let p = 0; p < finOp2.length; p++) {
    codigosProd.push(`COMPOSICAO7 IN (${finOp2[p]}) `);
  }

  try {
    sql = `SELECT COLECAO, CODIGO FROM PRODUTO_001 WHERE ${codigos.join(' OR ')}`;
    produtosCol = await firebirdClient.executeQuery(sql);
    // console.log('---------------------------');
    // console.log(produtosCol);
  } catch (error) {
    console.log(error);
  }

  try {
    sql = `SELECT COR, CODIGO, TAM, COMPOSICAO7 FROM PA_ITEN_001 WHERE ${codigosProd.join(' OR ')}`;
    coresSisplan = await firebirdClient.executeQuery(sql);
  } catch (error) {
    console.log(error);
  }

  try {
    sql = `SELECT DISTINCT NUMERO FROM FACCAO_001 t1a WHERE OP = '90' AND QUANT <> 0 AND DT_MOD_APP > '2022-01-01 10:00:00.000'`;
    let auxArray = [];
    let resp = await firebirdClient.executeQuery(sql);

    let count = 0;
    let idx = 0;
    resp.forEach((it) => {
      if (count > 500) {
        auxArray[idx] ? auxArray[idx].push(it.NUMERO) : auxArray[idx] = [it.NUMERO];
        count = 0;
        idx++;
      } else {
        auxArray[idx] ? auxArray[idx].push(it.NUMERO) : auxArray[idx] = [it.NUMERO];
        count++;
      }
    });

    console.log(auxArray);
    for (let i = 0; i < auxArray.length; i++) {
      let rp = auxArray[i].map(item => `'${item}'`);
      finOp ? finOp : finOp = [];
      finOp[i] = rp.join(',');
    }

    // finOp = ['2246009','2248005']

    try {
      let inSql = '';
      for (let p = 0; p < finOp.length; p++) {
        inSql += `AND NUMERO NOT IN (${finOp[p]}) `
      }

      sql = `SELECT CODIGO, COR, TAM, QT_ORIG, OP, NUMERO
      FROM FACCAO_001 F1
      WHERE DT_MOD_APP > '2022-01-01 10:00:00.000' ${inSql} AND NUMERO <> '2239016'
      AND OP = (SELECT MAX(OP) FROM FACCAO_001 F2 WHERE F1.NUMERO = F2.NUMERO AND PARTE = '01') AND PARTE = '01'`;
      listProd = await firebirdClient.executeQuery(sql);
      // listProd = listProd.concat(result);

    } catch (error) {
        console.log(error);
    }
  } catch (error) {
      console.log(error);
  }

  arrayResult.forEach((prod) => {
    if (!prod.produto.includes("KIT")) {
      let cod = prod.produto.split('-');
      let newCod = cod['0'] + cod['1'];
      let codigo = cod['0'].split(' ').join('');
      let corProd = coresSisplan.filter((clPrd) => clPrd.COMPOSICAO7 === (cod['0'] + '-' + cod['1']));
      let producao = undefined;
      if (corProd['0']) {
        // console.log(cod['0']);
        producao = listProd.filter((prdQtd) => prdQtd.CODIGO === codigo && prdQtd.COR === corProd['0'].COR && prdQtd.TAM === cod['2']);
        producao.totalCod = 0;
        if (producao.length > 0) {
          producao.forEach((pd) => {
            if (pd.CODIGO == '10582') {
              // console.log(pd);

            }
            producao.totalCod += pd.QT_ORIG;
          })
        }
      }
      prod.color = cod['1'];
      prod.size = cod['2'];
      prod.produto = cod['0'] + '';
      cod['0'] = cod['0'] + '';

      // console.log(newCod);
      if (objIdx[codigo] !== undefined) {
        if (objIdxAux[newCod] !== undefined) {
          arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'])] !== undefined ? arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'])] += parseInt(prod.quantidadeDisponivelVenda) : arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'])] = parseInt(prod.quantidadeDisponivelVenda);
          arrFinal[objIdx[codigo]].quantidadeDisponivelVendaTotal += parseInt(prod.quantidadeDisponivelVenda);
          if (producao && producao['0']) {
            arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'] + 'prod')] !== undefined ? arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'] + 'prod')] += parseInt(producao.totalCod) : arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'] + 'prod')] = parseInt(producao.totalCod);
            arrFinal[objIdx[codigo]].quantidadeProdTotal += parseInt(producao.totalCod);  
          } else {
            arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'] + 'prod')] = 0;
            arrFinal[objIdx[codigo]].quantidadeProdTotal ? '' : arrFinal[objIdx[codigo]].quantidadeProdTotal = 0;
          }
        } else {  
          prod[('size' + cod['2'])] = parseInt(prod.quantidadeDisponivelVenda);
          if (producao && producao['0']) {
            prod[('size' + cod['2'] + 'prod')] = parseInt(producao.totalCod);
          } else {
            prod[('size' + cod['2'] + 'prod')] = 0;
          }
          objIdxAux[newCod] = arrFinal[objIdx[codigo]].data.length;
          arrFinal[objIdx[codigo]].data.push(prod);
        }
      } else {
        objIdx[codigo] = arrFinal.length;
        objIdxAux[newCod] = 0;
        prod[('size' + cod['2'])] = parseInt(prod.quantidadeDisponivelVenda);
        prod[('size' + cod['2'] + 'prod')] = (producao && producao['0']) ? parseInt(producao.totalCod) : 0;
        arrFinal.push({ data: [prod], quantidadeDisponivelVendaTotal: parseInt(prod.quantidadeDisponivelVenda), quantidadeProdTotal: ((producao && producao['0']) ? parseInt(producao.totalCod) : 0) });
      }
    }
  });

  arrayResult2.forEach((prod) => {
    if (!prod.produto.includes("KIT")) {
      let cod = prod.produto.split('-');
      prod.color = cod['1'];
      prod.size = cod['2'];
      prod.produto = cod['0'] + '';
      let newCod = cod['0'] + cod['1'];
      cod['0'] = cod['0'] + '';

      let codigo = cod['0'].split(' ').join('');
      // console.log(newCod);
      if (objIdx[codigo] !== undefined) {
        if (objIdxAux[newCod] !== undefined) {
          arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'])] !== undefined ? arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'])] += parseInt(prod.quantidadeDisponivelVenda) : arrFinal[objIdx[codigo]].data[objIdxAux[newCod]][('size' + cod['2'])] = parseInt(prod.quantidadeDisponivelVenda);
          arrFinal[objIdx[codigo]].quantidadeDisponivelVendaTotal += parseInt(prod.quantidadeDisponivelVenda);
        } else {
          prod[('size' + cod['2'])] = parseInt(prod.quantidadeDisponivelVenda);
          objIdxAux[newCod] = arrFinal[objIdx[codigo]].data.length;
          arrFinal[objIdx[codigo]].data.push(prod);
        }
      } else {
        objIdx[codigo] = arrFinal.length;
        objIdxAux[newCod] = 0;
        prod[('size' + cod['2'])] = parseInt(prod.quantidadeDisponivelVenda);
        arrFinal.push({ data: [prod], quantidadeDisponivelVendaTotal: parseInt(prod.quantidadeDisponivelVenda) });
      }
    }
  });

  // console.log('produtosCol-----------');
  // console.log( produtosCol);

  let objCol = []
  let pushCol = [];

  produtosCol.forEach((itm) => {
    if (!objCol[itm.COLECAO]) {
      objCol[itm.COLECAO] = 1;
      let initDt = colecoes.filter((tt) => tt.CODIGO === itm.COLECAO)
      pushCol.push({COL: itm.COLECAO, DT: initDt['0'].DT_INICIO})
    }
  })
  // console.log(pushCol);
  pushCol.sort((a,b) => {
    if (moment(a.DT,'YYYY-MM-DD').isAfter(moment(b.DT,'YYYY-MM-DD'))) {
      return -1;
    } else if (moment(a.DT,'YYYY-MM-DD').isBefore(moment(b.DT,'YYYY-MM-DD'))) {
      return 1;
    } else {
      return 0;
    }
  })
  // arrFinal.sort((a,b) => {
  //   console.log();
  //   if (a.produto > b.produto) {
  //     return 1;
  //   }
  //   if (a.produto < b.produto) {
  //     return -1;
  //   }
  //   // a must be equal to b
  //   return 0;
  // })  

  let auxFinal = [];

  for (let i = 0; i < pushCol.length; i++) {
    const cl = pushCol[i];
    
    let resp = produtosCol.filter((prCl) => prCl.COLECAO == cl.COL);
    
    // console.log(resp);
  // console.log('produtosCol-----------');
  // console.log( produtosCol);
    let allProdCol = []; 
    for (let i = 0; i < resp.length; i++) {
      const itm = resp[i];
      
      let prod = arrFinal.filter((prd) => prd.data['0'].produto === itm.CODIGO);
      // console.log(prod);
      allProdCol.push(prod['0']);

    }
    console.log('antes');
    // console.log(prod);
    allProdCol = allProdCol.sort((a,b) => {
              // console.log('-----------------');
      if (a.data['0'].produto > b.data['0'].produto) {
        return 1;
      }
      if (a.data['0'].produto < b.data['0'].produto) {
        return -1;
      }
      // a deve ser igual a b
      return 0;
    });
    console.log('depois');

    allProdCol.forEach((pd) => {
      auxFinal.push(pd);
    });
  }


  let finalResp = await htmlReturn(auxFinal, produtosCol, colecoes);

  console.log('Enviando email...');
  await sendEmail(finalResp,false);
  console.log('----------------Final----------------');
}

const htmlReturn = async (list, produtosCol, colecoes) => {
  let printContents;
  let colecoesInsert = {};

  printContents = `<div class="table-responsive rounded-top">`
  printContents = printContents + `<table class="table table-bordered">`
  printContents = printContents + `<thead class='thead-dark'>`
  printContents = printContents + `<col>`
  printContents = printContents + `<col>`
  printContents = printContents + `<col>`
  printContents = printContents + `<colgroup span="9"></colgroup>`
  printContents = printContents + `<col>`

  printContents = printContents + `<tr>`
  printContents = printContents + `<th style='font-size: 10px' rowspan="2">Cod.</th>`
  printContents = printContents + `<th style='font-size: 10px' rowspan="2">Desc.</th>`
  printContents = printContents + `<th style='font-size: 10px' rowspan="2">*</th>`
  printContents = printContents + `<th style='font-size: 10px' rowspan="2">Cor</th>`
  printContents = printContents + `<th style='font-size: 10px' colspan="12" style='text-align: center'>Tamanho</th>`
  printContents = printContents + `<th style='font-size: 10px' rowspan="2">Tot.</th>`
  printContents = printContents + `</tr>`

  printContents = printContents + `<tr>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">1</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">2</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">3</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">4</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">6</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">8</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">10</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">12</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">14</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">P</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">M</th>`
  printContents = printContents + `<th style='font-size: 10px' scope="col">G</th>`
  printContents = printContents + `</tr>`
  printContents = printContents + `</thead>`
  printContents = printContents + `<tbody>`;

  await list.forEach(async (prodList) => {

    let totalSize1 = 0;
    let totalSize2 = 0;
    let totalSize3 = 0;
    let totalSize4 = 0;
    let totalSize6 = 0;
    let totalSize8 = 0;
    let totalSize10 = 0;
    let totalSize12 = 0;
    let totalSize14 = 0;
    let totalSizeP = 0;
    let totalSizeM = 0;
    let totalSizeG = 0;
    let totalSize1prod = 0;
    let totalSize2prod = 0;
    let totalSize3prod = 0;
    let totalSize4prod = 0;
    let totalSize6prod = 0;
    let totalSize8prod = 0;
    let totalSize10prod = 0;
    let totalSize12prod = 0;
    let totalSize14prod = 0;
    let totalSizePprod = 0;
    let totalSizeMprod = 0;
    let totalSizeGprod = 0;
    let cnt = 1;

    await prodList.data.forEach((prod) => {
      totalSize1 += (prod.size1 !== undefined ? prod.size1 : 0);
      totalSize2 += (prod.size2 !== undefined ? prod.size2 : 0);
      totalSize3 += (prod.size3 !== undefined ? prod.size3 : 0);
      totalSize4 += (prod.size4 !== undefined ? prod.size4 : 0);
      totalSize6 += (prod.size6 !== undefined ? prod.size6 : 0);
      totalSize8 += (prod.size8 !== undefined ? prod.size8 : 0);
      totalSize10 += (prod.size10 !== undefined ? prod.size10 : 0);
      totalSize12 += (prod.size12 !== undefined ? prod.size12 : 0);
      totalSize14 += (prod.size14 !== undefined ? prod.size14 : 0);
      totalSizeP += (prod.sizeP !== undefined ? prod.sizeP : 0);
      totalSizeM += (prod.sizeM !== undefined ? prod.sizeM : 0);
      totalSizeG += (prod.sizeG !== undefined ? prod.sizeG : 0);
      totalSize1prod += (prod.size1prod !== undefined ? prod.size1prod : 0);
      totalSize2prod += (prod.size2prod !== undefined ? prod.size2prod : 0);
      totalSize3prod += (prod.size3prod !== undefined ? prod.size3prod : 0);
      totalSize4prod += (prod.size4prod !== undefined ? prod.size4prod : 0);
      totalSize6prod += (prod.size6prod !== undefined ? prod.size6prod : 0);
      totalSize8prod += (prod.size8prod !== undefined ? prod.size8prod : 0);
      totalSize10prod += (prod.size10prod !== undefined ? prod.size10prod : 0);
      totalSize12prod += (prod.size12prod !== undefined ? prod.size12prod : 0);
      totalSize14prod += (prod.size14prod !== undefined ? prod.size14prod : 0);
      totalSizePprod += (prod.sizePprod !== undefined ? prod.sizePprod : 0);
      totalSizeMprod += (prod.sizeMprod !== undefined ? prod.sizeMprod : 0);
      totalSizeGprod += (prod.sizeGprod !== undefined ? prod.sizeGprod : 0);


      let totalColor = (prod.size1 !== undefined ? prod.size1 : 0) + (prod.size2 !== undefined ? prod.size2 : 0) + (prod.size3 !== undefined ? prod.size3 : 0) + 
                       (prod.size4 !== undefined ? prod.size4 : 0) + (prod.size6 !== undefined ? prod.size6 : 0) + (prod.size8 !== undefined ? prod.size8 : 0) + 
                       (prod.size10 !== undefined ? prod.size10 : 0) + (prod.size12 !== undefined ? prod.size12 : 0) + (prod.size14 !== undefined ? prod.size14 : 0) +
                       (prod.sizeP !== undefined ? prod.sizeP : 0) + (prod.sizeM !== undefined ? prod.sizeM : 0) + (prod.sizeG !== undefined ? prod.sizeG : 0);
                       
      let totalColorProd = (prod.size1prod !== undefined ? prod.size1prod : 0) + (prod.size2prod !== undefined ? prod.size2prod : 0) + (prod.size3prod !== undefined ? prod.size3prod : 0) + 
                           (prod.size4prod !== undefined ? prod.size4prod : 0) + (prod.size6prod !== undefined ? prod.size6prod : 0) + (prod.size8prod !== undefined ? prod.size8prod : 0) + 
                           (prod.size10prod !== undefined ? prod.size10prod : 0) + (prod.size12prod !== undefined ? prod.size12prod : 0) + (prod.size14prod !== undefined ? prod.size14prod : 0) + 
                           (prod.sizePprod !== undefined ? prod.sizePprod : 0) + (prod.sizeMprod !== undefined ? prod.sizeMprod : 0) + (prod.sizeGprod !== undefined ? prod.sizeGprod : 0);

      // let lgt = 1 - prod.descricaoProduto.length;
      // console.log(prod.descricaoProduto.split('-',2));

      let prdColc = produtosCol.filter((prCl) => prCl.CODIGO === (prod.produto + ''));
      let colc = colecoes.filter((cl) => parseInt(cl.CODIGO) === parseInt(prdColc['0'].COLECAO));

      // if (prod.produto === '10602') {
      //   console.log('prdColcCOLECAO-----------');
      //   console.log( prdColc['0'].COLECAO);
    
      //   console.log('colcDESCRICAO-----------');
      //   console.log(colc['0'].DESCRICAO);
      // }

      if (!colecoesInsert[prdColc['0'].COLECAO]) {
        colecoesInsert[prdColc['0'].COLECAO] = '#'+(0x1000000+Math.random()*0xffffff).toString(16).substr(1,6);

        let contentLineTitulo = "<tr>";
        contentLineTitulo += "<td colspan='1'></td>";
        contentLineTitulo += `<td colspan='17' style='text-align: center'>${colc['0'].DESCRICAO}</td>`;
        contentLineTitulo += `<td style='color: ${colecoesInsert[prdColc['0'].COLECAO]}'>`+ '|' + `</td>`;
        contentLineTitulo += "</tr>";
        printContents = printContents + contentLineTitulo;
      }


      let descProd = prod.descricaoProduto.split('-', 2)
      let contentLine = "<tr>";
      contentLine += "<td style='font-size: 13px'>" + prod.produto + "</td>";
      contentLine += "<td style='font-size: 13px'>" + descProd['0'] + "</td>";
      contentLine += "<td style='font-size: 10px'>" + prod.deposito + "</td>";
      contentLine += "<td style='font-size: 10px'>" + prod.color + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size1 > 150 && prod.size1 <= 300 ? 'rgb(164, 167, 13)' : (prod.size1 > 0 && prod.size1 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size1 !== undefined ? prod.size1 : 0) + '/' + (prod.size1prod !== undefined ? prod.size1prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size2 > 150 && prod.size2 <= 300 ? 'rgb(164, 167, 13)' : (prod.size2 > 0 && prod.size2 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size2 !== undefined ? prod.size2 : 0) + '/' + (prod.size2prod !== undefined ? prod.size2prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size3 > 150 && prod.size3 <= 300 ? 'rgb(164, 167, 13)' : (prod.size3 > 0 && prod.size3 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size3 !== undefined ? prod.size3 : 0) + '/' + (prod.size3prod !== undefined ? prod.size3prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size4 > 150 && prod.size4 <= 300 ? 'rgb(164, 167, 13)' : (prod.size4 > 0 && prod.size4 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size4 !== undefined ? prod.size4 : 0) + '/' + (prod.size4prod !== undefined ? prod.size4prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size6 > 150 && prod.size6 <= 300 ? 'rgb(164, 167, 13)' : (prod.size6 > 0 && prod.size6 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size6 !== undefined ? prod.size6 : 0) + '/' + (prod.size6prod !== undefined ? prod.size6prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size8 > 150 && prod.size8 <= 300 ? 'rgb(164, 167, 13)' : (prod.size8 > 0 && prod.size8 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size8 !== undefined ? prod.size8 : 0) + '/' + (prod.size8prod !== undefined ? prod.size8prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size10 > 150 && prod.size10 <= 300 ? 'rgb(164, 167, 13)' : (prod.size10 > 0 && prod.size10 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size10 !== undefined ? prod.size10 : 0) + '/' + (prod.size10prod !== undefined ? prod.size10prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size12 > 150 && prod.size12 <= 300 ? 'rgb(164, 167, 13)' : (prod.size12 > 0 && prod.size12 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size12 !== undefined ? prod.size12 : 0) + '/' + (prod.size12prod !== undefined ? prod.size12prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${prod.size14 > 150 && prod.size14 <= 300 ? 'rgb(164, 167, 13)' : (prod.size14 > 0 && prod.size14 <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + (prod.size14 !== undefined ? prod.size14 : 0) + '/' + (prod.size14prod !== undefined ? prod.size14prod : 0) + "</td>";
      // contentLine += `<td style='font-size: 10px; color: ${totalColor > 150 && totalColor <= 300 ? 'rgb(164, 167, 13)' : (totalColor <= 150 ? 'rgb(221, 24, 24)' : '')}''>` + totalColor + '/' + totalColorProd + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size1 !== undefined ? prod.size1 : 0) + '/' + (prod.size1prod !== undefined ? prod.size1prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size2 !== undefined ? prod.size2 : 0) + '/' + (prod.size2prod !== undefined ? prod.size2prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size3 !== undefined ? prod.size3 : 0) + '/' + (prod.size3prod !== undefined ? prod.size3prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size4 !== undefined ? prod.size4 : 0) + '/' + (prod.size4prod !== undefined ? prod.size4prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size6 !== undefined ? prod.size6 : 0) + '/' + (prod.size6prod !== undefined ? prod.size6prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size8 !== undefined ? prod.size8 : 0) + '/' + (prod.size8prod !== undefined ? prod.size8prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size10 !== undefined ? prod.size10 : 0) + '/' + (prod.size10prod !== undefined ? prod.size10prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size12 !== undefined ? prod.size12 : 0) + '/' + (prod.size12prod !== undefined ? prod.size12prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.size14 !== undefined ? prod.size14 : 0) + '/' + (prod.size14prod !== undefined ? prod.size14prod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.sizeP !== undefined ? prod.sizeP : 0) + '/' + (prod.sizePprod !== undefined ? prod.sizePprod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.sizeM !== undefined ? prod.sizeM : 0) + '/' + (prod.sizeMprod !== undefined ? prod.sizeMprod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + (prod.sizeG !== undefined ? prod.sizeG : 0) + '/' + (prod.sizeGprod !== undefined ? prod.sizeGprod : 0) + "</td>";
      contentLine += `<td style='font-size: 10px'>` + totalColor + '/' + totalColorProd + "</td>";

      contentLine += `<td style='color: ${colecoesInsert[prdColc['0'].COLECAO]}'>`+ '|' + `</td>`;

      contentLine += "</tr>";
      printContents = printContents + contentLine;
      if (prodList.data.length === cnt) {
        let total = (totalSize1 ? totalSize1 : 0) + (totalSize2 ? totalSize2 : 0) + (totalSize3 ? totalSize3 : 0) + 
                    (totalSize4 ? totalSize4 : 0) + (totalSize6 ? totalSize6 : 0) + (totalSize8 ? totalSize8 : 0) + 
                    (totalSize10 ? totalSize10 : 0) + (totalSize12 ? totalSize12 : 0) + (totalSize14 ? totalSize14 : 0) +
                    (totalSizeP ? totalSizeP : 0) + (totalSizeM ? totalSizeM : 0) + (totalSizeG ? totalSizeG : 0);
        let totalProd = (totalSize1prod ? totalSize1prod : 0) + (totalSize2prod ? totalSize2prod : 0) + (totalSize3prod ? totalSize3prod : 0) + 
                        (totalSize4prod ? totalSize4prod : 0) + (totalSize6prod ? totalSize6prod : 0) + (totalSize8prod ? totalSize8prod : 0) + 
                        (totalSize10prod ? totalSize10prod : 0) + (totalSize12prod ? totalSize12prod : 0) + (totalSize14prod ? totalSize14prod : 0) +
                        (totalSizePprod ? totalSizePprod : 0) + (totalSizeMprod ? totalSizeMprod : 0) + (totalSizeGprod ? totalSizeGprod : 0);

        let contentLine = "<tr>";
        contentLine += "<td colspan='4' style='text-align: center'>Total</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize1 > 150 && totalSize1 <= 300 ? 'rgb(164, 167, 13)' : (totalSize1 > 0 && totalSize1 <= 150 ? 'rgb(221, 24, 24)' : '')}' >` + totalSize1 + '/' + totalSize1prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize2 > 150 && totalSize2 <= 300 ? 'rgb(164, 167, 13)' : (totalSize2 > 0 && totalSize2 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize2 + '/' + totalSize2prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize3 > 150 && totalSize3 <= 300 ? 'rgb(164, 167, 13)' : (totalSize3 > 0 && totalSize3 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize3 + '/' + totalSize3prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize4 > 150 && totalSize4 <= 300 ? 'rgb(164, 167, 13)' : (totalSize4 > 0 && totalSize4 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize4 + '/' + totalSize4prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize6 > 150 && totalSize6 <= 300 ? 'rgb(164, 167, 13)' : (totalSize6 > 0 && totalSize6 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize6 + '/' + totalSize6prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize8 > 150 && totalSize8 <= 300 ? 'rgb(164, 167, 13)' : (totalSize8 > 0 && totalSize8 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize8 + '/' + totalSize8prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize10 > 150 && totalSize10 <= 300 ? 'rgb(164, 167, 13)' : (totalSize10 > 0 && totalSize10 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize10 + '/' + totalSize10prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize12 > 150 && totalSize12 <= 300 ? 'rgb(164, 167, 13)' : (totalSize12 > 0 && totalSize12 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize12 + '/' + totalSize12prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${totalSize14 > 150 && totalSize14 <= 300 ? 'rgb(164, 167, 13)' : (totalSize14 > 0 && totalSize14 <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + totalSize14 + '/' + totalSize14prod + "</td>";
        // contentLine += `<td style='font-size: 10px; color: ${total > 150 && total <= 300 ? 'rgb(164, 167, 13)' : (total <= 150 ? 'rgb(221, 24, 24)' : '')}'>` + total + '/' + totalProd + "</td>";

        contentLine += `<td style='font-size: 10px' >` + totalSize1 + '/' + totalSize1prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize2 + '/' + totalSize2prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize3 + '/' + totalSize3prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize4 + '/' + totalSize4prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize6 + '/' + totalSize6prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize8 + '/' + totalSize8prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize10 + '/' + totalSize10prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize12 + '/' + totalSize12prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSize14 + '/' + totalSize14prod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSizeP + '/' + totalSizePprod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSizeM + '/' + totalSizeMprod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + totalSizeG + '/' + totalSizeGprod + "</td>";
        contentLine += `<td style='font-size: 10px'>` + total + '/' + totalProd + "</td>";


        contentLine += `<td style='color: ${colecoesInsert[prdColc['0'].COLECAO]}'>`+ '|' + `</td>`;
        contentLine += "</tr>";
        printContents = printContents + contentLine;
        let contentLineSpace = "<tr>";
        contentLineSpace += "<td colspan='14'></td>";
        contentLineSpace += "</tr>";
        printContents = printContents + contentLineSpace;
      }
      cnt++;
    });


  });

  printContents = printContents + `</tbody>`
  printContents = printContents + `</table>`
  printContents = printContents + `</div>`

  let finalHTML = `<html>
      <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Estoque</title>
        <style>
          //........Customized style.......
        </style>
      </head>
      <body onload="window.print();">
        ${printContents}
      </body>
    </html>`
  // console.log(finalHTML);
  return finalHTML;
}

// getQtdProd = async (cod, color, size) => {
//   let resp = listProd.filter((prod) => prod.produto.includes(cod) && prod.produto.includes(color) && prod.produto.includes(size));
//   return resp['0'];
// }

getList();

// module.exports = {
//   script: getList,
// };
const moment = require('moment');
const axios = require('axios');

const run = async () => {
    let filter = {
        offset: 0,
        limit: 100,
        allResults: false
    }

    let arrayResult = [];
    while (!filter.allResults) {
        let result = await axios.get(`https://molekada.painel.magazord.com.br/api/v1/listMovimentacaoEstoque?offset=${filter.offset}&limit=${filter.limit}&ativo=true&dataHoraMovimentacaoInicial=2023-09-05T15:00:00-03&dataHoraMovimentacaoFinal=2023-09-05T19:45:00-03`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk='
            }
        });

        if (result.data.data.length === 0) {
            filter.allResults = true;
        } else if (result.data.data.length > 0) {
            filter.offset = filter.offset + filter.limit;
        }
        arrayResult = arrayResult.concat(result.data.data);
    }


    let arrayResultCanceled = [];
    let arrayRepeteadCanceled = [];
    arrayResult.forEach((item) => {
        if (item.observacao && item.observacao.indexOf('Cancelamento da movimentação') > -1) {
            let index = arrayResultCanceled.findIndex((canceled) => canceled.produto == item.produto);
            if (index == -1)  {
                arrayResultCanceled.push(item);
            } else {
                arrayRepeteadCanceled.push(item);
                console.log(item.produto);
            }
        }
    });

    console.log(arrayResult.length);
    console.log(arrayResultCanceled.length);
    console.log(arrayRepeteadCanceled.length);
}

run();
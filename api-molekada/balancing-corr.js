﻿const axios = require("axios");
const { parseInt, get } = require("lodash");
const { sendEmail } = require("./src/common/utils/email");

let erros = "";
const balancing = async () => {
  var amalook = "amalook";
  var molekada = "molekada";
  var miniMagia = "minimagia";


  let arrayCor = await getEstoqueAtual(molekada);

};

async function salvarLoja(nomeLoja, data, tentativa) {
  // console.log(
  //   `${nomeLoja} - SALVANDO ${data.produto} - QTD ${data.quantidade} -  Tentativa ${tentativa}`
  // );

  let token = "";
  if (nomeLoja == "amalook") {
    token = "YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=";
  } else if(nomeLoja == 'molekada'){
    token = "YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=";
  } else {
    token = "OTRjZjczMmU1Yjc3Mzk2NjM5Yjk0NzIzNDBmZjQ4ZTA1MDZkZmY3OTJhMTIwNjdhNjE4ZmQ3MjMxOTViMjQ0YzpvNjJGRnIlaSFocDU=";
  }
  // if (data.produto.includes('MPJ0104-MAR')) {
  if (data.produto.includes('KIT')) {
    console.log('KIT');
  } else {
    console.log(data);
    await axios.post(
      `https://${nomeLoja}.painel.magazord.com.br/api/v1/estoque`,
      data,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Basic ${token}`,
        },
      }
    ); 
  }
  

}

async function getTokenLogin() {
  let resultLogin = await axios.post(
    "http://y7m0yeljsg.execute-api.sa-east-1.amazonaws.com/dev/authentication-api/login",
    {
      email: "marcelojoras@hotmail.com",
      password: "DgSys06042023",
      returnSecureToken: true,
    }
  );

  return resultLogin.data.authorization;
}

async function getBalanceamentoGlobal(token) {
  let result = await axios.get(
    "http://y7m0yeljsg.execute-api.sa-east-1.amazonaws.com/dev/balanceamento-loja-api/balanceamento-loja",
    {
      headers: {
        "x-api-key": token,
      },
    }
  );
  console.log('balance',
  result.data.contents
  );

  return result.data.contents;
}

async function getBalanceamentoProdutos(token) {
  let results = await axios.get(
    "http://y7m0yeljsg.execute-api.sa-east-1.amazonaws.com/dev/balanceamento-product-view-api/balanceamento-product-view/active",
    {
      headers: {
        "x-api-key": token,
      },
    }
  );

  return results.data;
}

async function getEstoqueAtual(nomeLoja) {
  let token = "";
  if (nomeLoja == "amalook") {
    token =
      "YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY=";
  } else if(nomeLoja == 'minimagia') {
    token = 'OTRjZjczMmU1Yjc3Mzk2NjM5Yjk0NzIzNDBmZjQ4ZTA1MDZkZmY3OTJhMTIwNjdhNjE4ZmQ3MjMxOTViMjQ0YzpvNjJGRnIlaSFocDU=';
  } else {
    token =
      "YzM3YzYwZDhhZjcxOGFkZmFiNGI4NWNhYWZlNDdlZGI1NjMxMGM5YmQyOTRiYzFiMDYwOTUyZmVlZmVkM2MzNTpYJEg0N3olUXMkNmk=";
  }

  let arrayResultLj = [];
  let filterLj = {
    offset: 0,
    limit: 100,
    allResults: false,
  };
  while (!filterLj.allResults) {
    let result = await axios.get(
      `https://${nomeLoja}.painel.magazord.com.br/api/v1/listMovimentacaoEstoque?offset=${filterLj.offset}&limit=${filterLj.limit}&dataHoraMovimentacaoInicial='2023-09-04T00:00:00Z'&dataHoraMovimentacaoFinal='2023-09-04T23:00:00Z'&produto=''&ignoreMovimentacaoWebService=true`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Basic ${token}`,
        },
      }
    );
    if (result.data.data.length === 0) {
      filterLj.allResults = true;
    } else if (result.data.data.length > 0) {
      filterLj.offset = filterLj.offset + filterLj.limit;
    }
    arrayResultLj = arrayResultLj.concat(result.data.data);
  }

  return arrayResultLj;
}

function getTipoOpe(value) {
  return value < 0 ? 2 : 1;
}
function getOriMov(value) {
  return value < 0 ? 8 : 1;
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

balancing();

// module.exports = {
//   script: balancing,
// };

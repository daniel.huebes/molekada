const axios = require('axios');

const emptyV2 = async () => {
    let arrayResultLj2 = [];
    let filterLj2 = {
        offset: 0,
        limit: 100,
        allResults: false
    }
    while (!filterLj2.allResults) {
        let result = await axios.get(`https://amalook.painel.magazord.com.br/api/v1/listEstoque?offset=${filterLj2.offset}&limit=${filterLj2.limit}&ativo=true`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY='
            }
        });
        if (result.data.data.length === 0) {
            filterLj2.allResults = true;
        } else if (result.data.data.length > 0) {
            filterLj2.offset = filterLj2.offset + filterLj2.limit + 1;
            filterLj2.limit = filterLj2.limit;
        }
        arrayResultLj2 = arrayResultLj2.concat(result.data.data);
    }

    arrayResultLj2.forEach(element => {
        if (element.quantidadeDisponivelVenda > 0) {
            let objPost = {
                produto: element.produto,
                deposito: element.deposito,
                quantidade: element.quantidadeDisponivelVenda,
                tipo: 1,
                tipoOperacao: 2,
                origemMovimento: 8,
                observacao: 'TestesBalançoEstoque',
            }
            console.log(objPost);
            axios.post(`https://amalook.painel.magazord.com.br/api/v1/estoque`,objPost,{
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic YTgwYTQzZjU3MGY1YTI4ZTgwZWE1MTk1ODVkNTI0ZGMxOGQzMTYxMzg5Mzk4YWRlOTk3MWE3YzcyZWFkMGYzMDp0czcmVThQNUtGMjY='
                }
            });
        }
    });
}

emptyV2();

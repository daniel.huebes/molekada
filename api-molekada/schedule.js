const cron = require('node-cron');
const index = require('./index');

cron.schedule('* * * * *', () => {    
  index.getListScript().then();
});

cron.schedule('25 10 * * *', () => {    
  index.balancingScript().then();
});
//index.getListScript().then();

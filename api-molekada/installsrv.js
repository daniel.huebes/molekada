const { resolve } = require('path');
const Service = require('node-windows').Service;

const scriptPath = resolve(__dirname, 'schedule.js');

// Create a new service object
var svc = new Service({
  name: 'DG Sys Scripts 1.2.3',
  description: 'DG Sys load to cloud 1.2.3',
  script: scriptPath,
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install', () => {
  svc.start();
});

svc.install();
